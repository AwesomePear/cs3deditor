using CSUtils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using static CSUtils.MSGUtils;

namespace Server_ {

    /// <summary>
    /// Серверный класс клиента, действия над клиентом
    /// </summary>
    public class Client {
        TcpClient tcp;
        public bool connected;
        public string nickname { get; set; }
        public UserActions UserActions = new UserActions();

        public Client(TcpClient t) {
            tcp = t;
            connected = true;
        }

        /// <summary>
        /// Сервер читает сообщение от клиента
        /// </summary>
        public string ReadMessage() {
            try {
                return GetMessage();
            } catch (Exception e) {
                Debug.WriteLine(e);
                return DISCONNECTED;
            }
        }
        public bool HaveMessage => connected && (message_part.Length > 0 || tcp.Available > 0);

        private string GetRawMessage() {
            var buffer = new byte[SYS.ByteLimit];
            int bytes = tcp.GetStream().Read(buffer, 0, buffer.Length);
            if (bytes == 0) return null;
            return Encoding.Unicode.GetString(buffer);
        }
        private string message_part = "";
        private string GetMessage() {
            int delimiter = 0;
            var message = "";
            if (message_part.Length > 0 && message_part.First() != '[') message_part = "";
            if (message_part.Length > 0) { //если не дочитали
                delimiter = message_part.IndexOf(SYS.MessageDelimiter);
                if (delimiter == -1) {  // если последнее недочитанное, возможен разрыв
                    message = message_part;
                    message_part = "";
                } else { //если не последнее недочитанное
                    var res = message_part.Substring(0, delimiter);
                    message_part = message_part.Substring(delimiter + 1);
                    return res;
                }
            }
            if (tcp.Available > 0) {//если есть что читать из буфера
                var raw = GetRawMessage().Trim();
                if (message.Length != 0) { //если уже достали кусок от прошлого
                    if (raw != null) {//если что-то было вообще
                        delimiter = raw.IndexOf(SYS.MessageDelimiter);
                        message += raw.Substring(0, delimiter);
                        message_part = raw.Substring(delimiter + 1);
                    }
                    return message;
                } else { //если недочитанных огрызков нет
                    delimiter = raw.IndexOf(SYS.MessageDelimiter);
                    if (delimiter > 0) {
                        message = raw.Substring(0, delimiter);
                        message_part = raw.Substring(delimiter + 1);
                    } else {
                        message = raw;
                        message_part = "";
                    }

                    return message;
                }
            }
            return message;
        }

        public void SendMessage(string tag, string message) => SendMessage(tag + message + Separator);
        public void SendMessage(string message) {
            try {
                message += SYS.MessageDelimiter;
                byte[] Buffer = Encoding.Unicode.GetBytes(message);
                tcp.GetStream().Write(Buffer, 0, Buffer.Length);
            } catch { }
        }
        public void Disconnect() {
            connected = false;
            tcp.Close();
        }

        public override string ToString() {
            return nickname;
        }
    }

   
}
