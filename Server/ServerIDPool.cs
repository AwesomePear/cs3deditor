using CSUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server_ {

    public class ServerIDPool {
        private static ServerIDPool o = new ServerIDPool();
        public static ServerIDPool Get => o;
        private ServerIDPool() {}
        decimal VertexFrom = 100;
        //readonly decimal VertexTo = 10000000;
        decimal EdgeFrom = 100;
        //readonly decimal EdgeTo = 10000000;
        decimal FaceFrom = 100;
        //readonly decimal FaceTo = 10000000;
        decimal ObjectFrom = 100;
        //readonly decimal ObjectTo = 10000000;

        public enum Type : byte {
            Vertex, Edge, Face, Object
        }
        public (decimal from,decimal to) GetNext(Type t, decimal count) {
            switch (t) {
                case Type.Vertex:
                    var res = (VertexFrom, VertexFrom + count);
                    VertexFrom += count + 1;
                    return res;
                case Type.Edge:
                    res = (EdgeFrom, EdgeFrom + count);
                    EdgeFrom += count + 1;
                    return res;
                case Type.Face:
                    res = (FaceFrom, FaceFrom + count);
                    FaceFrom += count + 1;
                    return res;
                case Type.Object:
                    res = (ObjectFrom, ObjectFrom + count);
                    ObjectFrom += count + 1;
                    return res;
            }
            throw new ArgumentException("че ваще положили сюда? " + t);
        }

        public void ParseAndSend(Client c, string msg) {
            //[SYS]iv_100_&|
            msg = msg.Substring(6);
            //v_100_&|
            var required = decimal.Parse(msg.Split('_')[1]);
            Send(c,msg.First(), required);
        }
        public void Send(Client c, char first, decimal required) {
            switch (first) {
                case 'v':
                    var pair = GetNext(Type.Vertex, required);
                    c.SendMessage(MSGUtils.SYS_tag, "iv_" + pair.from + '-' + pair.to + "_&");
                    break;
                case 'e':
                    pair = GetNext(Type.Edge, required);
                    c.SendMessage(MSGUtils.SYS_tag, "ie_" + pair.from + '-' + pair.to + "_&");
                    break;
                case 'f':
                    pair = GetNext(Type.Face, required);
                    c.SendMessage(MSGUtils.SYS_tag, "if_" + pair.from + '-' + pair.to + "_&");
                    break;
                case 'o':
                    pair = GetNext(Type.Object, required);
                    c.SendMessage(MSGUtils.SYS_tag, "io_" + pair.from + '-' + pair.to + "_&");
                    break;
                case 'a':
                    var pairv = GetNext(Type.Vertex, required);
                    var paire = GetNext(Type.Edge, required);
                    var pairf = GetNext(Type.Face, required);
                    var pairo = GetNext(Type.Object, required);
                    c.SendMessage(MSGUtils.SYS_tag, "ia_" +
                        'v' + '_' + pairv.from + '-' + pairv.to + '_' + 
                        'e' + '_' + paire.from + '-' + paire.to + '_' +
                        'f' + '_' + pairf.from + '-' + pairf.to + '_' +
                        'o' + '_' + pairo.from + '-' + pairo.to + "_&");
                    break;
            }
        }

    }
}
