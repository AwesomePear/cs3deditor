using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace Server_ {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>

    public partial class MainWindow : Window {
        static MainWindow o;
        public static MainWindow Get => o;
        public static void FromGUIThread(Action action) {
            o.Dispatcher.Invoke(DispatcherPriority.Normal, action);
        }


        public string Property {
            get { return (string)GetValue(ChatTextProperty); }
            set { SetValue(ChatTextProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ChatTextProperty.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ChatTextProperty =
            DependencyProperty.Register("ChatTextProperty", typeof(string), typeof(MainWindow), new PropertyMetadata(""));


        public bool ServerEnabled {
            get => _serverEnabled;
            set {
                if (value) EnabledColor = GREEN;
                else EnabledColor = RED;
                _serverEnabled = value;
            }
        }
        public string EnabledColor {
            get { return (string)GetValue(EnabledColorProperty); }
            set { SetValue(EnabledColorProperty, value); }
        }

        // Using a DependencyProperty as the backing store for EnabledColor.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty EnabledColorProperty =
            DependencyProperty.Register("EnabledColor", typeof(string), typeof(MainWindow), new PropertyMetadata(RED));

        public const string RED = "#FFDD5656";
        public const string GREEN = "#FF56DD56";


        Server Server;
        private bool _serverEnabled;
        private Client _selected;

        public MainWindow() {
            o = this;
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            InitializeComponent();
            Server = new Server();
            Chat.Window = this;
            Chat.Server = Server;
            DataContext = this;
            SceneCounters.DataContext = Server.Counter;
        }

        protected override void OnClosing(CancelEventArgs e) {
            base.OnClosing(e);
            Server.Stop();
        }

        private void Settings_Click(object sender, RoutedEventArgs e) {
            new SettingsWindow(Server).ShowDialog();
        }
        private void Start_Click(object sender, RoutedEventArgs e) {
            Server.Start();
            ServerEnabled = Server.Started;
        }
        private void Stop_Click(object sender, RoutedEventArgs e) {
            Server.Stop();
            ServerEnabled = Server.Started;
        }
        private void KickClient_Click(object sender, RoutedEventArgs e) {
            if (Selected != null) {
                Selected.SendMessage(CSUtils.MSGUtils.NET_tag, "kicked");
                Selected.Disconnect();
                ClientsList.Items.Remove(Selected);
                Selected = null;
            }
        }
        private Client Selected {
            get => _selected; set {
                if (value != null)
                    SelectedUsername.Text = value.nickname;
                else SelectedUsername.Text = "";
                _selected = value;
            }
        }
        private void ClientsList_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            Selected = (sender as ListBox).SelectedItem as Client;
        }
    }



}
