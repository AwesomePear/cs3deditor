using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server_ {
    public static class Extensions {
        public static string Prepare(this DateTime d) => "[" + d.Hour + ":" + d.Minute + ":" + d.Second + "]";

        public static List<T> Remove<T>(this List<T> l, Predicate<T> pred) {
            var to_remove = l.FindAll(pred);
            foreach (var x in to_remove) l.Remove(x);
            return l;
        }
        public static Dictionary<T1, T2> AddRange<T1, T2>(this Dictionary<T1, T2> l, Dictionary<T1,T2> d) {
            foreach (var x in d) l.Add(x.Key, x.Value);
            return l;
        }
        public static Dictionary<T1,T2> Remove<T1,T2>(this Dictionary<T1, T2> l, Predicate<T2> pred) {
            var to_remove = l.FindAll(pred);
            foreach (var x in to_remove) l.Remove(x.Key);
            return l;
        }

        public static KeyValuePair<T1,T2>? Find<T1,T2>(this Dictionary<T1, T2> l, Predicate<T2> pred) {
            foreach (var x in l)
                if (pred(x.Value)) return x;
            return null;
        }
        public static Dictionary<T1, T2> FindAll<T1, T2>(this Dictionary<T1, T2> l, Predicate<T2> pred) {
            var res = new Dictionary<T1, T2>();
            foreach (var x in l)
                if (pred(x.Value)) res.Add(x.Key, x.Value);
            return res;
        }
    }
}
