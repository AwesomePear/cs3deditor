using CSUtils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using static CSUtils.MSGUtils;

namespace Server_ {

    public class Server:DependencyObject {
        private IPAddress IP;
        private TcpListener tcpListener;
        private List<Client> clients = new List<Client>();
        public ServerScene Scene = new ServerScene();
        public string pass = "12547";
        public bool fromfile = false;
        public string ServerIp {
            get { return (string)GetValue(ServerIpProperty); }
            set {
                SetValue(ServerIpProperty, value);
            }
        }
        public static readonly DependencyProperty ServerIpProperty =
            DependencyProperty.Register("ServerIp", typeof(string), typeof(Server), new PropertyMetadata("127.0.0.1"));
        public int ServerPort {
            get { return (int)GetValue(ServerPortProperty); }
            set {
                SetValue(ServerPortProperty, value);
            }
        }
        public static readonly DependencyProperty ServerPortProperty =
            DependencyProperty.Register("ServerPort", typeof(int), typeof(Server), new PropertyMetadata(14500));
        public string Password {
            get { return (string)GetValue(PasswordProperty); }
            set {
                pass = value;
                SetValue(PasswordProperty, value);
            }
        }
        public static readonly DependencyProperty PasswordProperty =
            DependencyProperty.Register("Password", typeof(string), typeof(Server), new PropertyMetadata("12547"));
        public string SceneFilePath {
            get { return (string)GetValue(SceneFilePathProperty); }
            set {
                SetValue(SceneFilePathProperty, value);
            }
        }
        public static readonly DependencyProperty SceneFilePathProperty =
            DependencyProperty.Register("SceneFilePath", typeof(string), typeof(Server), new PropertyMetadata(""));
        public bool SceneFromFile {
            get { return (bool)GetValue(SceneFromFileProperty); }
            set {
                fromfile = value;
                SetValue(SceneFromFileProperty, value);
            }
        }
        public static readonly DependencyProperty SceneFromFileProperty =
            DependencyProperty.Register("SceneFromFile", typeof(bool), typeof(Server), new PropertyMetadata(false));


        public class SceneCounter :DependencyObject{
            public decimal Objects {
                get { return (decimal)GetValue(ObjectsProperty); }
                set { SetValue(ObjectsProperty, value); }
            }
            public static readonly DependencyProperty ObjectsProperty =
                DependencyProperty.Register("Objects", typeof(decimal), typeof(SceneCounter), new PropertyMetadata(0M));
            public decimal Vertices {
                get { return (decimal)GetValue(VerticesProperty); }
                set { SetValue(VerticesProperty, value); }
            }
            public static readonly DependencyProperty VerticesProperty =
                DependencyProperty.Register("Vertices", typeof(decimal), typeof(SceneCounter), new PropertyMetadata(0M));
            public decimal Edges {
                get { return (decimal)GetValue(EdgesProperty); }
                set { SetValue(EdgesProperty, value); }
            }
            public static readonly DependencyProperty EdgesProperty =
                DependencyProperty.Register("Edges", typeof(decimal), typeof(SceneCounter), new PropertyMetadata(0M));
            public decimal Faces {
                get { return (decimal)GetValue(FacesProperty); }
                set { SetValue(FacesProperty, value); }
            }
            public static readonly DependencyProperty FacesProperty =
                DependencyProperty.Register("Faces", typeof(decimal), typeof(SceneCounter), new PropertyMetadata(0M));


            public void Update(ServerScene s) {
                Objects = s.Objects.Count;
                Vertices = 0M;
                foreach (var x in s.Objects) Vertices += x.Value.Vertices.Count;
                Edges = 0M;
                foreach (var x in s.Objects) Edges += x.Value.Edges.Count;
                Faces = 0M;
                foreach (var x in s.Objects) Faces += x.Value.Faces.Count;
            }
        }

        public readonly SceneCounter Counter = new SceneCounter();

        public Server() {
            UserAction.Context = Scene;
            
            IPAddress[] ipList = Dns.GetHostEntry(Dns.GetHostName()).AddressList; //all host`s ip-s
            
        }

        public void SendForAll(string tag, string message, Client except = null) {
            foreach(var x in clients) {
                if (x.connected && (except==null || x != except)) {
                    x.SendMessage(tag, message);
                }
            }
        }
        public void SendForAll(string message, Client except = null) {
            foreach (var x in clients) {
                if (x.connected && (except == null || x != except)) {
                    x.SendMessage(message);
                }
            }
        }

        /// <summary>
        /// Сервер парсит сообщение от клиента на составные части
        /// </summary>
        public static (string tag, string[] parts) ParseToParts(string msg) {
            var tag = msg.Substring(0, 5);
            var parts = msg.Substring(5).Split(Separator);
            return (tag, parts);
        }





        public void ReactOnMessage(Client c) {
            try {
                var msg = c.ReadMessage();
                
                if (msg == null || msg.Length == 0 || msg == ERROR_EMPTY_BUFFER || msg.First()!='[') return;
                //new Task(() => MessageBox.Show("Сервер получил: "+msg)).Start();

                if (msg == DISCONNECTED) {
                    c.connected = false;
                    Chat.AddMessage("SERVER", c.nickname + " " + DISCONNECTED.ToLower());
                    return;
                }
                if (msg.IsSYS()) {
                    if (msg[5] == 'i')
                        ServerIDPool.Get.ParseAndSend(c, msg);
                    else {
                        var acts = Parse.FullMessage(ref UserActions.UserActionCounter, msg);
                        var s = "";
                        foreach (var x in acts) 
                            c.UserActions.Do(x);
                        #region ааааа
                        var act = acts.FindAll(o => o is CreateObject);
                        if (act.Count > 0) s += " created " + act.Count + " object" + (act.Count == 1 ? "" : "s") + "; "; ;
                        act = acts.FindAll(o => o is CreateVertex);
                        if (act.Count > 1) {
                            s += " created " + act.Count + " vertices";
                            foreach (var x in act) {
                                var v = Scene.GetVertexByID(((CreateVertex)x).VertexID).Item2;
                                s += "[" + v.ID + "](" + v.X + ", " + v.Y + ", " + v.Z + ") ";
                            }
                            s += ";";
                        } else if (act.Count == 1) {
                            var v = Scene.GetVertexByID(((CreateVertex)act[0]).VertexID).Item2;
                            s += " created 1 vertex [" + v.ID + "]((" + v.X + ", " + v.Y + ", " + v.Z + ");";
                        }

                        act = acts.FindAll(o => o is CreateEdge);
                        if (act.Count > 1) {
                            s += " created " + act.Count + " edges";
                            foreach (var x in act) {
                                var v = Scene.GetEdgeByID(((CreateEdge)x).EdgeID).Item2;
                                s += "[" + v.ID + "](" + v.First.ID + "," + v.Second.ID + ") ";
                            }
                            s += ";";
                        } else if (act.Count == 1) {
                            var v = Scene.GetEdgeByID(((CreateEdge)act[0]).EdgeID).Item2;
                            s += " created 1 edge [" + v.ID + "](" + v.First.ID + "," + v.Second.ID + ");";
                        }

                        act = acts.FindAll(o => o is CreateFace);
                        if (act.Count > 1) {
                            s += " created "+act.Count+" faces";
                            foreach (var x in act) {
                                var v = Scene.GetFaceByID(((CreateFace)x).FaceID).Item2;
                                s += "[" + v.ID + "](" + v.First.ID + "," + v.Second.ID + "," + v.Third.ID + ") ";
                            }
                            s += ";";
                        } else if (act.Count == 1) {
                            var v = Scene.GetFaceByID(((CreateFace)act[0]).FaceID).Item2;
                            s += " created 1 face [" + v.ID + "](" + v.First.ID + "," + v.Second.ID + "," + v.Third.ID + ");";
                        }

                        if (msg[msg.Length - 2] == 'l') {
                            act = acts.FindAll(o => o is ChangeVertex);
                            if (act.Count > 1) {
                                s += " transformed " + act.Count + " vertices";
                                foreach (var x in act) {
                                    var v = Scene.GetVertexByID(((ChangeVertex)x).VertexID).Item2;
                                    s += "[" + v.ID + "](" + v.X + ", " + v.Y + ", " + v.Z + ") ";
                                }
                                s += ";";
                            } else if (act.Count == 1) {
                                var v = Scene.GetVertexByID(((ChangeVertex)act[0]).VertexID).Item2;
                                s += " transformed 1 vertex [" + v.ID + "]((" + v.X + ", " + v.Y + ", " + v.Z + ");";
                            } else {
                                act = acts.FindAll(o => o is ChangeModelMatrix);
                                if (act.Count > 1) {
                                    s += " transformed " + act.Count + " objects";
                                    foreach (var x in act) {
                                        var v = Scene.GetObjectByID(((ChangeModelMatrix)x).ObjectID);
                                        s += "[" + v.ID + "](" + Pack.Matrix4(v.ModelMatrix) + ") ";
                                    }
                                    s += ";";
                                } else if (act.Count == 1) {
                                    var v = Scene.GetObjectByID(((ChangeModelMatrix)act[0]).ObjectID);
                                    s += " transformed 1 object [" + v.ID + "]((" + Pack.Matrix4(v.ModelMatrix) + ");";
                                }
                            }
                        }

                        if (s.Length > 0 )MainWindow.FromGUIThread(() => Chat.Window.HistoryList.Items.Add(DateTime.Now.Prepare() + c + s));
                        #endregion
                    }
                    Debug.WriteLine(msg);
                    
                    SendForAll(msg, c);
                    MainWindow.FromGUIThread(() => Counter.Update(Scene));

                } else if (msg.IsMSG()) {

                    var m = ParseToParts(msg);
                    Chat.AddMessage(c.nickname, m.parts[0]);

                } else if (msg.IsNET()) {
                    var m = ParseToParts(msg);
                    Debug.WriteLine("Server received: " + msg);
                    Debug.WriteLine("m.parts[0] = \"" + m.parts[0]+"\"");
                    if (m.parts[0] == "needscene") {//если запрос сцены
                        var mess = Pack.Scene(Scene);
                        foreach (var x in mess)
                            c.SendMessage(SYS_tag, x);
                    } else if (m.parts[0]==DISCONNECT) {
                        c.connected = false;
                        MainWindow.FromGUIThread(() => Chat.Window.ClientsList.Items.Remove(c));
                        Chat.AddMessage(("SERVER", c.nickname + " " + DISCONNECTED.ToLower()));

                    } else {//если подключение
                        c.nickname = m.parts[0];
                        var pass = m.parts[1];
                        if (pass != this.pass) {
                            c.SendMessage(NET_tag, "wrong password");
                            c.Disconnect();
                            return;
                        }
                        MainWindow.FromGUIThread(() => Chat.Window.ClientsList.Items.Add(c));
                        var newmsg = CONNECTED;
                        if (!fromfile) {
                            newmsg += Separator + "Forward";
                            MainWindow.FromGUIThread(() => SceneFromFile = true);


                        }
                        c.SendMessage(NET_tag, newmsg);
                        Chat.AddMessage(("SERVER", c.nickname + " " + CONNECTED.ToLower()));
                        c.connected = true;
                        ServerIDPool.Get.Send(c, 'a', 20000);
                    }

                } else {
                    throw new Exception("пришел мусор: " + msg);
                }

            } catch (Exception e) {
                new Task(() => Debug.WriteLine(e+Environment.NewLine+ e.StackTrace)).Start();
            }
        }


        private void Listening() {
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            try {
                while (true) {
                    if (tcpListener.Pending()) //есть ли запрос на подключение
                        clients.Add(new Client(tcpListener.AcceptTcpClient()));//взяли клиента, который к нам попросился на ручки
                    foreach (var x in clients) 
                        if (x.HaveMessage) ReactOnMessage(x);
                    clients.Remove(x => !x.connected);
                    MainWindow.FromGUIThread(() => Counter.Update(Scene));
                    Thread.Sleep(5);
                }
            } catch (ThreadAbortException) {
                Debug.WriteLine("Поток слушателя остановлен");
                foreach (var x in clients) {
                    x.SendMessage(NET_tag, DISCONNECT);
                    x.Disconnect();
                }
                clients.Clear();
            } catch (Exception e) {
                Debug.WriteLine(e+Environment.NewLine+ e.StackTrace);
            } 
        }

        Thread Listener;
        public bool Started = false;
        public void Start() {
            IP = IPAddress.Parse(ServerIp);
            tcpListener = new TcpListener(IP, ServerPort);
            tcpListener.Start();
            if (fromfile = SceneFromFile) 
                Parse.File(SceneFilePath);
            Debug.WriteLine(ServerIp + ":" + ServerPort);
            pass = Password;


            if (!Started) {
                Listener = new Thread(new ThreadStart(Listening));
                Listener.Start();
                Started = true;
            }
        }
        public void Stop() {
            if (Started) {
                Listener.Abort();
                Listener = null;
                Started = false;
                tcpListener.Stop();
            } 
        }

    }
}
