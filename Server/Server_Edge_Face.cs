using CSUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server_ {
    public class Edge : IEdge {
        public decimal ID { get; }
        public IVertex First { get; }
        public IVertex Second { get; }
        public Edge(decimal id, IVertex f, IVertex s) {
            ID = id; First = f; Second = s;
        }
        private bool Contains(IVertex v) => Contains(v.ID);
        private bool Contains(decimal v_id) => First.ID == v_id || Second.ID == v_id;

        public bool Contains(params IVertex[] v) {
            foreach (var x in v)
                if (!Contains(x.ID)) return false;
            return true;
        }
        public bool Contains(params decimal[] v) {
            foreach (var x in v)
                if (!Contains(x)) return false;
            return true;
        }
        public override string ToString() => "Edge[" + ID + "," + First.ID + "," + Second.ID + "]";
    
}


    public class Face : IFace {
        public decimal ID { get; }
        public IVertex First { get; }
        public IVertex Second { get; }
        public IVertex Third  { get; }
        public Face(decimal id, IVertex f, IVertex s, IVertex t) {
            ID = id; First = f; Second = s; Third = t;
        }
        private bool Contains(IVertex v) => Contains(v.ID);
        private bool Contains(decimal v_id) => First.ID == v_id || Second.ID == v_id || Third.ID == v_id;

        public bool Contains(params IVertex[] v) {
            foreach (var x in v)
                if (!Contains(x.ID)) return false;
            return true;
        }
        public bool Contains(params decimal[] v) {
            foreach (var x in v)
                if (!Contains(x)) return false;
            return true;
        }
        public override string ToString() => "Face[" + ID + "," + First.ID + "," + Second.ID + "," + Third.ID + "]";

    }
}
