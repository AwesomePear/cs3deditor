using System;
using System.Windows;
using OpenTK.Graphics.OpenGL4;
using OpenTK.Graphics;
using OpenTK;
using System.Diagnostics;
using System.Collections.Generic;
using System.Windows.Input;
using System.Windows.Controls;
using CSUtils;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace Server_ {
    public partial class SettingsWindow : Window {



        public SettingsWindow(object datacontext) {
            InitializeComponent();
            Grid.DataContext = datacontext;

        }

        void OpenFile_Click(object sender, EventArgs e) {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.FileName = "Scene.byak"; // Default file name
            dlg.DefaultExt = ".byak"; // Default file extension
            dlg.Filter = "Byak scene format (.byak)|*.byak"; // Filter files by extension

            bool? open = dlg.ShowDialog();

            if (open == true) {
                ((Server)Grid.DataContext).SceneFilePath = dlg.FileName;

            }
        }
        void SaveFile_Click(object sender, EventArgs e) {
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = "Scene.byak"; // Default file name
            dlg.DefaultExt = ".byak"; // Default file extension
            dlg.Filter = "Byak scene format (.byak)|*.byak"; // Filter files by extension

            bool? open = dlg.ShowDialog();

            if (open == true) {
                FileWork.SaveTo(dlg.FileName);

            }
        }
    }
}