using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSUtils;

namespace Server_ {

    class TestMain {

        static private string GetRawMessage() {
            return "lalalalalalalala|papapapapa";
        }
        static int reads = 0;
        static private string message_part = "";
        static private string GetMessage(bool a) {
            int delimiter = 0;
            var message = "";
            var dbg = "";

            if (message_part.Length > 0) { //если не дочитали
                dbg += "[" + reads++ + "]" + "не дочитали(" + message_part.Length + ")" + Environment.NewLine;
                delimiter = message_part.IndexOf(SYS.MessageDelimiter);
                if (delimiter == -1) {  // если последнее недочитанное, возможен разрыв
                    dbg += "и оно было последним" + Environment.NewLine;
                    message = message_part;
                    message_part = "";
                } else { //если не последнее недочитанное
                    var res = message_part.Substring(0, delimiter);
                    message_part = message_part.Substring(delimiter + 1);
                    dbg += "и оно не было последним(ост. часть: " + message_part + ")" + Environment.NewLine;
                    Console.WriteLine(dbg);
                    return res;
                }
            }
            if (a) {//если есть что читать из буфера
                var raw = GetRawMessage();
                dbg += "прочитали из буфера" + Environment.NewLine;
                if (message.Length != 0) { //если уже достали кусок от прошлого
                    dbg += "а у нас уже был кусок от прошлого (" + message + ")" + Environment.NewLine;
                    if (raw != null) {//если что-то было вообще
                        delimiter = raw.IndexOf(SYS.MessageDelimiter);
                        message += raw.Substring(0, delimiter);
                        message_part = raw.Substring(delimiter + 1);
                    }
                    Console.WriteLine(dbg);
                    return message;
                } else { //если недочитанных огрызков нет
                    dbg += "и недочитанного не было" + Environment.NewLine;
                    delimiter = raw.IndexOf(SYS.MessageDelimiter);
                    message = raw.Substring(0, delimiter);
                    message_part = raw.Substring(delimiter + 1);
                    dbg += "теперь мы не дочитали(" + message_part + ")" + Environment.NewLine;
                    Console.WriteLine(dbg);
                    return message;
                }
            }
            Console.WriteLine(dbg);
            return message;

        }
        public static void Main() {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            Console.WriteLine(GetMessage(true));
            Console.WriteLine(GetMessage(false));




            Console.ReadKey(true);
        }
    }
}
