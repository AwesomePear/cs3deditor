using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSUtils;
using OpenTK;

namespace Server_ {

    class SceneObject : IObject {
        public Dictionary<decimal, IVertex> Vertices { get; }
        public Dictionary<decimal, IEdge> Edges { get; }
        public Dictionary<decimal, IFace> Faces { get; }

        public Matrix4 ModelMatrix { get; set; } = Matrix4.Identity;

        public void AcceptModelMatrix() {
            foreach (var v in Vertices) {
                var vec = new Vector3(v.Value.X, v.Value.Y, v.Value.Z);
                vec = ( new Vector4(vec, 1f) * ModelMatrix).Xyz;
                v.Value.X = vec.X;
                v.Value.Y = vec.Y;
                v.Value.Z = vec.Z;
            }
            ModelMatrix = Matrix4.Identity;
        }
        public decimal ID { get; set; }

        public void CreateVertex(VertexInfo info) => Vertices.Add(info.ID, info);
        public IVertex GetVertexByID(decimal ID) => Vertices[ID];
        public void DeleteVertex(decimal ID) {
            Vertices.Remove(ID);
        }
        public SceneObject(decimal ID) {
            this.ID = ID;
            Vertices = new Dictionary<decimal, IVertex>();
            Edges = new Dictionary<decimal, IEdge>();
            Faces = new Dictionary<decimal, IFace>();
        }

        public override string ToString() {
            var s = "Object " + ID + Environment.NewLine;
            foreach(var x in Vertices) 
                s += "    " + x +Environment.NewLine;
            foreach (var x in Edges)
                s += "    " + x + Environment.NewLine;
            foreach (var x in Faces)
                s += "    " + x + Environment.NewLine;
            return s;
        }


        public void CreateEdge(decimal ID, IVertex f, IVertex s) {
            //добавить проверку на повтор
            Edges.Add(ID,new Edge(ID, f, s));
        }
        public void CreateEdge(decimal ID, decimal f_id, decimal s_id) => CreateEdge(ID, Vertices[f_id], Vertices[s_id]);
        public void DeleteEdge(decimal ID) => Edges.Remove(ID);
        public void DeleteEdge(decimal f_id, decimal s_id) => Edges.Remove(x => x.Contains(f_id, s_id));
        public void DeleteEdge(IVertex f, IVertex s) => DeleteEdge(f.ID, s.ID);
        public IEdge GetEdge(decimal ID) => Edges[ID];
        public IEdge GetEdge(IVertex f, IVertex s) => GetEdge(f.ID, s.ID);
        public IEdge GetEdge(decimal f, decimal s) => ((KeyValuePair<decimal, IEdge>)Edges.Find(x => x.Contains(f, s))).Value;
        public void CreateFace(decimal ID, IVertex f, IVertex s, IVertex t) {
            //добавить проверку на повтор
            Faces.Add(ID, new Face(ID, f, s, t));
        }
        public void CreateFace(decimal ID, decimal f_id, decimal s_id, decimal t_id) => CreateFace(ID, Vertices[f_id], Vertices[s_id], Vertices[t_id]);
        public void DeleteFace(decimal ID) => Faces.Remove(ID);
        public void DeleteFace(IVertex f, IVertex s, IVertex t) => DeleteFace(f, s, t);
        public void DeleteFace(decimal f, decimal s, decimal t) => Faces.Remove(x => x.Contains(f,s,t));
        public IFace GetFace(decimal ID) => Faces[ID];
        public IFace GetFace(IVertex f, IVertex s, IVertex t) => GetFace(f, s, t);
        public IFace GetFace(decimal f, decimal s, decimal t) => ((KeyValuePair<decimal, IFace>)Faces.Find(o => o.Contains(f, s, t))).Value;

        public void Join(IObject other) {
            Vertices.AddRange(other.Vertices);
            Edges.AddRange(other.Edges);
            Faces.AddRange(other.Faces);
        }
    }

    public class ServerScene: IScene {
        public Dictionary<decimal, IObject> Objects { get; private set; }
        public ServerScene() {
            Objects = new Dictionary<decimal, IObject>();
        }
        public (IObject, IVertex) GetVertexByID(decimal id) {
            foreach(var x in Objects) {
                var succ = x.Value.Vertices.TryGetValue(id, out IVertex v);
                if (succ) return (x.Value, v);
            }
            return (null, null);
        }

        public (IObject, IEdge) GetEdgeByID(decimal id) {
            foreach (var x in Objects) {
                var succ = x.Value.Edges.TryGetValue(id, out IEdge e);
                if (succ) return (x.Value, e);
            }
            return (null, null);
        }

        public (IObject, IFace) GetFaceByID(decimal id) {
            foreach (var x in Objects) {
                var succ = x.Value.Faces.TryGetValue(id, out IFace f);
                if (succ) return (x.Value, f);
            }
            return (null, null);
        }

        public IObject GetObjectByID(decimal id) {
            var succ = Objects.TryGetValue(id, out IObject o);
            return o;
        }

        public void DeleteVertex(IObject o, IVertex v) {
            o.DeleteVertex(v.ID);
        }

        public void CreateVertex(IObject o, VertexInfo v) {
            o.CreateVertex(v);
        }

        public override string ToString() {
            var s = "Scene " + Environment.NewLine;
            foreach (var x in Objects)
                s += x + Environment.NewLine;
            return s;
        }

        public void CreateObject(decimal ID) {
            Objects.Add(ID, new SceneObject(ID));
        }

        public void DeleteObject(decimal ID) {
            Objects.Remove(ID);
        }

        public void JoinObjects(decimal main, params decimal[] other) {
            Objects[main].AcceptModelMatrix();
            foreach(var x in other) {
                Objects[x].AcceptModelMatrix();
                Objects[main].Join(Objects[x]);
                Objects.Remove(x);
            }
        }
    }
}
