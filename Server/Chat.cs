using CSUtils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using static CSUtils.MSGUtils;

namespace Server_ {
    public static class Chat {

        public static List<MSG> MSGs = new List<MSG>();

        public static MainWindow Window;
        public static Server Server;
        public static void AddMessage((string nickname, string message) pair) {
            var msg = new MSG(DateTime.Now, pair.nickname, pair.message);
            Window.ChatBlock.Dispatcher.Invoke(DispatcherPriority.Normal, new System.Action(delegate () {
                Window.Property += Environment.NewLine + msg;
            }));
            Server.SendForAll(msg.ToFullString());
        }
        public static void AddMessage(string nickname, string message) => AddMessage((nickname, message));
        //public static string GetNewMessage() => Window.ChatNewMessage.Text;

    }

    public struct MSG {
        public DateTime time;
        public string nickname;
        public string message;
        public override string ToString() {
            string _time = "[" + time.Hour + ":" + time.Minute + ":" + time.Second + "] ";
            return MSG_tag + _time + nickname + ": " + message.Trim(' ');
        }
        public string ToFullString() {
            return MSG_tag + time + Separator + nickname + Separator + message.Trim(' ') + Separator;
        }
        public MSG(DateTime t, string n, string m) {
            time = t; nickname = n; message = m;
        }
    }
}
