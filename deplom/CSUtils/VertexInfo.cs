using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSUtils {

    public class VertexInfo : IVertex {
        public class ColorInfo : IColor {
            public float R { get; set; }
            public float G { get; set; }
            public float B { get; set; }
            public float A { get; set; }

            public ColorInfo(float R, float G, float B, float A) {
                this.R = R; this.G = G; this.B = B; this.A = A;
            }
            public static IColor operator -(ColorInfo a) => a == null ? null : new ColorInfo(-a.R, -a.G, -a.B, -a.A);
            public IColor Copy() => new ColorInfo(R, G, B, A);
            public IColor Inverted() => new ColorInfo(-R, -G, -B, -A);
            public override string ToString() => "Color[" + R + "," + G + "," + B + "," + A + "]";
            public static ColorInfo Zero => new ColorInfo(0f, 0f, 0f, 1f);
        }
        public class TextureInfo : ITextureCoordinates {
            public float X { get; set; }
            public float Y { get; set; }

            public TextureInfo(float x, float y) {
                X = x; Y = y;
            }
            public static ITextureCoordinates operator -(TextureInfo a) => a == null ? null : new TextureInfo(-a.X, -a.Y);
            public ITextureCoordinates Copy() => new TextureInfo(X, Y);
            public ITextureCoordinates Inverted() => new TextureInfo(-X, -Y);
            public override string ToString() => "TC[" + X + "," + Y + "]";
            public static TextureInfo Zero => new TextureInfo(0f, 0f);
        }
        public decimal ID { get; }

        public float X { get; set; }
        public float Y { get; set; }
        public float Z { get; set; }
        public override string ToString() => "Vertex[" + ID + "," + X + "," + Y + "," + Z + " " + Color + TextureCoordinates + "]";

        public IColor Color { get; set; }
        public ITextureCoordinates TextureCoordinates { get; set; }
        public VertexInfo(decimal id, float x, float y, float z, IColor c = null, ITextureCoordinates t = null) {
            ID = id; X = x; Y = y; Z = z; Color = c; TextureCoordinates = t;
        }
        public VertexInfo(IVertex v) {
            ID = v.ID; X = v.X; Y = v.Y; Z = v.Z; Color = v.Color; TextureCoordinates = v.TextureCoordinates;
        }
        public static VertexInfo operator -(VertexInfo a) => new VertexInfo(a.ID, -a.X, -a.Y, -a.Z, a.Color == null ? null : a.Color.Inverted(), a.TextureCoordinates == null ? null : a.TextureCoordinates.Inverted());

        public VertexInfo Copy() => new VertexInfo(ID, X, Y, Z, Color == null ? null : Color.Copy(), TextureCoordinates == null ? null : TextureCoordinates.Copy());

        public VertexInfo GetCurrentState() => Copy();

        public void AddVertexInfo(VertexInfo info) {
            X += info.X;
            Y += info.Y;
            Z += info.Z;
            if (info.Color != null) {
                if (Color == null) Color = ColorInfo.Zero;
                Color.R += info.Color.R;
                Color.G += info.Color.G;
                Color.B += info.Color.B;
                Color.A += info.Color.A;
            }
            if (info.TextureCoordinates != null) {
                if (TextureCoordinates == null) TextureCoordinates = TextureInfo.Zero;
                TextureCoordinates.X += info.TextureCoordinates.X;
                TextureCoordinates.Y += info.TextureCoordinates.Y;
            }
        }

        public void SetVertexInfo(VertexInfo info) {
            X = info.X;
            Y = info.Y;
            Z = info.Z;
            Color = info.Color;
            TextureCoordinates = info.TextureCoordinates;
        }
    }
}
