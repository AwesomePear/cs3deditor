using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSUtils {
    public abstract class EdgeAction : UserAction {
        public decimal FirstID { get; protected set; }
        public decimal SecondID { get; protected set; }
        public decimal EdgeID { get; protected set; }
        public EdgeAction(decimal id, decimal e_id) : base(id) {
            EdgeID = e_id;
        }
    }


    public class CreateEdge : EdgeAction {
        public CreateEdge(decimal id, decimal e_id, decimal f_id, decimal s_id) : base(id, e_id) {
            FirstID = f_id;
            SecondID = s_id;
        }
        public override void Accept() {
            base.Accept();
            var Object = Context.GetVertexByID(FirstID).Item1;
            Object.CreateEdge(EdgeID, /*Object.Vertices[*/FirstID/*]*/, /*Object.Vertices[*/SecondID/*]*/);
        }
        public override void Cancel() {
            base.Cancel();
            new DeleteEdge(0, EdgeID).Accept();
        }
    }
    public class DeleteEdge : EdgeAction {
        public DeleteEdge(decimal id, decimal edge_id) : base(id, edge_id) { }
        public override void Accept() {
            base.Accept();
            (IObject O, IEdge E) = Context.GetEdgeByID(EdgeID);
            FirstID = E.First.ID;
            SecondID = E.Second.ID;
            O.DeleteEdge(EdgeID);
        }
        public override void Cancel() {
            base.Cancel();
            new CreateEdge(0, EdgeID, FirstID, SecondID).Accept();
        }
    }
}
