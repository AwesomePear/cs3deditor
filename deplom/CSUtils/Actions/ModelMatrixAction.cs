using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSUtils {
    public class ModelMatrixAction : UserAction {
        public ModelMatrixAction(decimal id) : base(id) {
        }
    }

    public class ChangeModelMatrix : ModelMatrixAction {
        public Matrix4 delta;
        public decimal ObjectID;
        public ChangeModelMatrix(decimal id, decimal obj_id, Matrix4 matrix) : base(id) {
            delta = matrix;
            ObjectID = obj_id;
        }

        public override void Accept() {
            base.Accept();
            var O = Context.GetObjectByID(ObjectID);
            O.ModelMatrix += delta; 
        }
        public override void Cancel() {
            base.Cancel();
            var O = Context.GetObjectByID(ObjectID);
            O.ModelMatrix += delta;

        }
    }
}
