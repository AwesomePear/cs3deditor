using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSUtils {

    public class CancelNotAcceptedActionException : Exception {
        public CancelNotAcceptedActionException(string message) : base(message) { }
    }
    public class ObjectNotFoundException : Exception {
        public ObjectNotFoundException(string message) : base(message) { }
    }
    public class ObjectAlreadyExistsException : Exception {
        public ObjectAlreadyExistsException(string message) : base(message) { }
    }
    public abstract class UserAction {
        public readonly decimal ID;
        private bool Accepted = false;
        public static IScene Context;
        public virtual void Accept() {
            if (Context == null) throw new ArgumentNullException("Context был пуст");
            Accepted = true; }
        public virtual void Cancel() {
            if (Context == null) throw new ArgumentNullException("Context был пуст");
            if (!Accepted) throw new CancelNotAcceptedActionException("Попытка отмены действия, которое не было совершено");
            else Accepted = false;
        }

        public UserAction(decimal id) {
            ID = id;
        }
    }


    
}
