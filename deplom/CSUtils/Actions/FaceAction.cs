using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSUtils {
    public abstract class FaceAction : UserAction {
        public decimal FirstID { get; protected set; }
        public decimal SecondID{ get; protected set; }
        public decimal ThirdID { get; protected set; } 
        public decimal FaceID { get; protected set; }
        public FaceAction(decimal id, decimal face_id) : base(id) {
            FaceID = face_id;
        }

    }


    public class CreateFace : FaceAction {
        public CreateFace(decimal id, decimal face_id, decimal first, decimal second, decimal third) : base(id, face_id) {
            FirstID = first; SecondID = second; ThirdID = third;
        }
        public override void Accept() {
            base.Accept();
            var Object = Context.GetVertexByID(FirstID).Item1;
            Object.CreateFace(FaceID, FirstID, SecondID, ThirdID);
        }
        public override void Cancel() {
            base.Cancel();
            new DeleteFace(0, FaceID).Accept();
        }
    }

    public class DeleteFace : FaceAction {
        public DeleteFace(decimal id, decimal face_id) : base(id, face_id) {
        }
        public override void Accept() {
            base.Accept();
            (IObject O, IFace F) = Context.GetFaceByID(FaceID);
            FirstID = F.First.ID;
            SecondID = F.Second.ID;
            ThirdID = F.Third.ID;
            O.DeleteFace(FaceID);
        }
        public override void Cancel() {
            base.Cancel();
            new CreateFace(0, FaceID, FirstID, SecondID, ThirdID).Accept();
        }
    }
    public class FlipFace : FaceAction {
        public FlipFace(decimal id, decimal face_id) : base(id, face_id) {}
        public override void Accept() {
            base.Accept();
            (IObject O, IFace F) = Context.GetFaceByID(FaceID);
            FirstID = F.First.ID;
            SecondID = F.Second.ID;
            ThirdID = F.Third.ID;
            O.DeleteFace(FaceID);
            O.CreateFace(FaceID, FirstID, ThirdID, SecondID);
            /*new DeleteFace(ID, FaceID, ObjectID).Accept();
            new CreateFace(ID, FaceID, ObjectID, First, Third, Second).Accept();*/
        }
        public override void Cancel() {
            base.Cancel();
            (IObject O, IFace F) = Context.GetFaceByID(FaceID);
            FirstID = F.First.ID;
            SecondID = F.Second.ID;
            ThirdID = F.Third.ID;
            O.DeleteFace(FaceID);
            O.CreateFace(FaceID, FirstID, SecondID, ThirdID);
            /*new DeleteFace(ID, FaceID, ObjectID).Accept();
            new CreateFace(ID, FaceID, ObjectID, First, Third, Second).Accept();*/
        }
    }
}
