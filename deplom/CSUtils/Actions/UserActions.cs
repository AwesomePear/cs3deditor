using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSUtils {
    
    public class UserActions {
        public static decimal UserActionCounter = 0; 
        Stack<UserAction> ToUndo = new Stack<UserAction>();
        Stack<UserAction> ToRedo = new Stack<UserAction>();


        public void Undo() {
            var a = ToUndo.Pop();
            a.Cancel();
            ToRedo.Push(a);
        }
        public void Redo() {
            var a = ToRedo.Pop();
            a.Accept();
            ToUndo.Push(a);
        }

        public void Do(UserAction a) {
            ToUndo.Push(a);
            a.Accept();
            ToRedo.Clear();
        }
    }
}
