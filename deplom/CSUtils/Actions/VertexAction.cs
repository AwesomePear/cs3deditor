using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSUtils {
    public abstract class VertexAction : UserAction {
        public IVertex Vertex;
        public decimal VertexID;
        public IObject Object;
        public decimal ObjectID;
        public VertexInfo Info;
        public VertexAction(decimal id, decimal v_id) : base(id) {
            VertexID = v_id;
        }
        public VertexAction(decimal id, decimal v_id, decimal o_id) : base(id) {
            VertexID = v_id; ObjectID = o_id;
        }
        public VertexAction(decimal id, IObject obj, VertexInfo v) : base(id) {
            Object = obj;
            ObjectID = obj.ID;
            Info = v;
            VertexID = v.ID;
        }
        public override void Accept() {
            (Object, Vertex) = Context.GetVertexByID(VertexID);
            base.Accept();
        }
        public override void Cancel() {
            (Object, Vertex) = Context.GetVertexByID(VertexID);
            base.Cancel();
        }
    }

    public class DeleteVertex : VertexAction {
        public DeleteVertex(decimal id, decimal vertexID) : base(id, vertexID) {
            VertexID = vertexID;
        }
        public DeleteVertex(decimal id, IObject obj, VertexInfo v) : base(id, obj, v) {
            VertexID = v.ID;
        }

        public override void Accept() {
            base.Accept();
            if (Vertex == null) throw new ObjectNotFoundException("Vertex[" + VertexID + "] не найден в текущем контексте");
            Info = Vertex.GetCurrentState();
            Context.DeleteVertex(Object, Vertex);

        }
        public override void Cancel() {
            base.Cancel();
            new CreateVertex(0, Object, Info).Accept();


        }
        public override string ToString() => "DeleteVertex[ID=" + VertexID + "]";
        
    }

    public class CreateVertex : VertexAction {
        
        public CreateVertex(decimal id, VertexInfo info, decimal o_id) : base(id, info.ID, o_id) {
            Info = info;
        }
        public CreateVertex(decimal id, IVertex info, decimal o_id) : base(id, info.ID, o_id) {
            Info = new VertexInfo(info);
        }
        public CreateVertex(decimal id, IObject obj, VertexInfo v) : base(id, obj, v) { }

        public override void Accept() {
            base.Accept();
            if (Vertex != null) throw new ObjectAlreadyExistsException("Vertex[" + Info.ID + "] уже существует в текущем контексте");
            Object = Context.GetObjectByID(ObjectID);
            Context.CreateVertex(Object, Info);

        }
        public override void Cancel() {
            base.Cancel();
            new DeleteVertex(0, Object, Info).Accept();

        }
        public override string ToString() => "CreateVertex[Info="+Info+"]";
    }

    public class ChangeVertex : VertexAction {
        public ChangeVertex(decimal id, IVertex info) : base(id, info.ID) {
            Info = new VertexInfo(info);
        }
        public ChangeVertex(decimal id, IObject obj, VertexInfo v) : base(id, obj, v) { }

        public override void Accept() {
            base.Accept();
            if (Vertex == null) throw new ObjectNotFoundException("Vertex[" + Info.ID + "] не найден в текущем контексте");
            Vertex.AddVertexInfo(Info);
        }
        public override void Cancel() {
            base.Cancel();
            if (Vertex == null) throw new ObjectNotFoundException("Vertex[" + Info.ID + "] не найден в текущем контексте");
            Vertex.AddVertexInfo(-Info);
        }
        public override string ToString() => "ChangeVertex[Info=" + Info + "]";
    }

}
