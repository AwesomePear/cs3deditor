using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSUtils {
    public class ObjectAction :UserAction{
        public IObject Object;
        public decimal ObjectID;
        public Matrix4 ModelMatrix;

        public ObjectAction(decimal id, decimal o_id):base(id) {
            ObjectID = o_id;
            Object = Context.GetObjectByID(o_id);
        }

    }

    public class DeleteObject : ObjectAction {

        public DeleteObject(decimal id, decimal o_id) : base(id, o_id) {
        }

        public override void Accept() {
            if (Object == null) throw new ObjectNotFoundException("Object["+ObjectID + "] не найден в текущем контексте");
            base.Accept();
            ModelMatrix = Context.Objects[ObjectID].ModelMatrix;
            Context.DeleteObject(ObjectID);
        }
        public override void Cancel() {
            base.Cancel();
            new CreateObject(0, ObjectID, ModelMatrix).Accept();


        }
    }

    public class CreateObject : ObjectAction {
        public CreateObject(decimal id, decimal o_id, Matrix4 modelmatrix) : base(id, o_id) {
            ModelMatrix = modelmatrix;
        }

        public override void Accept() {
            if(Object != null) throw new ObjectAlreadyExistsException("Object[" + ObjectID + "] уже существует в текущем контексте");
            base.Accept();
            Context.CreateObject(ObjectID);
            Context.Objects[ObjectID].ModelMatrix = ModelMatrix;

        }
        public override void Cancel() {
            base.Cancel();
            new DeleteObject(0, ObjectID).Accept();

        }
    }

    public class JoinObject : ObjectAction {
        public decimal[] others;
        public JoinObject(decimal id, decimal o_id, params decimal[] other_o_ids) : base(id, o_id) {
            others = other_o_ids;
        }

        public override void Accept() {
            if (Object == null) throw new ObjectNotFoundException("Object[" + ObjectID + "] не найден в текущем контексте");
            base.Accept();
            Context.JoinObjects(ObjectID, others);

        }
        public override void Cancel() {
            base.Cancel();


        }
    }
    /*public class DuplicateObject : ObjectAction {
        public decimal new_o;
        public DuplicateObject(decimal id, decimal o_id, decimal new_o_id) : base(id, o_id) {
            new_o = new_o_id;
        }

        public override void Accept() {
            if (Object == null) throw new ObjectNotFoundException("Object[" + ObjectID + "] не найден в текущем контексте");
            base.Accept();
            Context.JoinObjects(ObjectID, others);

        }
        public override void Cancel() {
            base.Cancel();


        }
    }*/
}
