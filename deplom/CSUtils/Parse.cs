using OpenTK;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSUtils {
    public static class Parse {
        public static void File(string path) {
            foreach (var obj in FileWork.ReadFrom(path).Split('|')) {
                if (obj.Length == 0) continue;
                var delim = new string[] { Environment.NewLine };
                var object_ = obj.Split(delim, StringSplitOptions.RemoveEmptyEntries);
                var obj_id = decimal.Parse(object_[0]);
                new CreateObject(0, obj_id, OpenTK.Matrix4.Identity).Accept();
                for (var i = 1; i < object_.Length; i++) {
                    var str = object_[i].Split(',');
                    switch (str[0].First()) {
                        case 'v': {
                                var id_v = decimal.Parse(str[0].Substring(1));
                                var x = float.Parse(str[1]);//coords
                                var y = float.Parse(str[2]);
                                var z = float.Parse(str[3]);
                                switch (str.Length) {
                                    case 4: {
                                            new CreateVertex(0, new VertexInfo(id_v, x, y, z, null,null), obj_id).Accept();
                                            break;
                                        }
                                    case 8: {
                                            var c = new VertexInfo.ColorInfo(float.Parse(str[4]), float.Parse(str[5]), float.Parse(str[6]), float.Parse(str[7]));
                                            new CreateVertex(0, new VertexInfo(id_v, x, y, z, c), obj_id).Accept();
                                            break; }
                                    case 6: {
                                            var t = new VertexInfo.TextureInfo(float.Parse(str[8]), float.Parse(str[9]));
                                            new CreateVertex(0, new VertexInfo(id_v, x, y, z, null, t), obj_id).Accept();
                                            break; }
                                    case 10: {
                                            var c = new VertexInfo.ColorInfo(float.Parse(str[4]), float.Parse(str[5]), float.Parse(str[6]), float.Parse(str[7]));
                                            var t = new VertexInfo.TextureInfo(float.Parse(str[8]), float.Parse(str[9]));
                                            new CreateVertex(0, new VertexInfo(id_v, x, y, z, c, t), obj_id).Accept();
                                            break; }
                                }
                                break;
                        }
                        case 'e': {
                                var id_e = decimal.Parse(str[0].Substring(1));
                                var first = decimal.Parse(str[1]);
                                var second = decimal.Parse(str[2]);
                                new CreateEdge(0, id_e, first, second).Accept();
                                break;
                        }
                        case 'f': {
                                var id_f = decimal.Parse(str[0].Substring(1));
                                var first = decimal.Parse(str[1]);
                                var second = decimal.Parse(str[2]);
                                var third = decimal.Parse(str[3]);
                                new CreateFace(0, id_f, first, second, third).Accept();
                                break;
                        }

                    }

                }
            }

        }  

        
        public static List<UserAction> FullMessage(ref decimal IDcounter, string acts) {
            try {
                if (acts.IsSYS()) acts = acts.Substring(5);
                var res = new List<UserAction>();
                var mess = acts.Split(MSGUtils.Separator);
                foreach (var x in mess) {
                    if (x.Length == 0) continue;
                    var trg = x.First();
                    UserAction action = null;
                    switch (trg) {
                        case 'v': action = VertexAction(++IDcounter, x); break;
                        case 'e': action = EdgeAction(++IDcounter, x); break;
                        case 'f': action = FaceAction(++IDcounter, x); break;
                        case 'o': action = ObjectAction(++IDcounter, x); break;
                        case 'm': action = ModelMatrixAction(++IDcounter, x); break;
                    }
                    if (action != null) {
                        res.Add(action);
                    }

                }
                return res;
            } catch (Exception e) {
                Debug.WriteLine("PARSE " + Environment.NewLine + e + Environment.NewLine + e.StackTrace + Environment.NewLine+acts);
                return null;
            }
        }

        public static VertexAction VertexAction(decimal give_id, string act) {
            var parts = act.Split('_');
            var trg_id = decimal.Parse(parts[0].Substring(1));
            try {
                switch (parts[1]) {
                    case "crt":
                        var obj_id = decimal.Parse(parts[2]);
                        var x = float.Parse(parts[3]);
                        var y = float.Parse(parts[4]);
                        var z = float.Parse(parts[5]);
                        VertexInfo.ColorInfo c = null;
                        VertexInfo.TextureInfo t = null;
                        if (parts.Length == 10)
                            c = new VertexInfo.ColorInfo(float.Parse(parts[6]), float.Parse(parts[7]), float.Parse(parts[8]), float.Parse(parts[9]));

                        if (parts.Length == 8)
                            t = new VertexInfo.TextureInfo(float.Parse(parts[6]), float.Parse(parts[7]));
                        if (parts.Length == 12)
                            t = new VertexInfo.TextureInfo(float.Parse(parts[10]), float.Parse(parts[11]));

                        return new CreateVertex(give_id, new VertexInfo(trg_id, x, y, z, c, t), obj_id);
                    //v2553 crt 33 x y z  //6
                    //v2553 crt 33 x y z r g b a // 10
                    //v2553 crt 33 x y z r g b a p s  //12
                    //v2553 crt 33 x y z p s // 8
                    case "dlt":
                        x = float.Parse(parts[2]);
                        y = float.Parse(parts[3]);
                        z = float.Parse(parts[4]);
                        c = null;
                        t = null;
                        if (parts.Length >= 9)
                            c = new VertexInfo.ColorInfo(float.Parse(parts[5]), float.Parse(parts[6]), float.Parse(parts[7]), float.Parse(parts[8]));

                        if (parts.Length > 9 || parts.Length == 7)
                            t = new VertexInfo.TextureInfo(float.Parse(parts[9]), float.Parse(parts[10]));

                        return new ChangeVertex(give_id, new VertexInfo(trg_id, x, y, z, c, t));
                    //v2553 dlt x y z  //5
                    //v2553 dlt x y z r g b a // 9
                    //v2553 dlt x y z r g b a p s  //11
                    //v2553 dlt x y z p s // 7
                    case "del":
                        return new DeleteVertex(give_id, trg_id);
                }
                throw new ArgumentException("Строка " + act + " не распознана как действие ");
            }catch (Exception e) {
                Debug.WriteLine("VERTEXACTION: " + Environment.NewLine + act + e + Environment.NewLine + e.StackTrace);
                return null;
            }
        }


        public static ObjectAction ObjectAction(decimal give_id, string act) {
            var parts = act.Split('_');
            var trg_id = decimal.Parse(parts[0].Substring(1));
            switch (parts[1]) {
                case "crt": 
                    return new CreateObject(give_id, trg_id, Matrix4(parts[2]));

                case "del":
                    return new DeleteObject(give_id, trg_id);
                case "jno":
                    var others = new List<decimal>();
                    for (int i = 2; i < parts.Length; i++)
                        others.Add(decimal.Parse(parts[i]));
                    return new JoinObject(give_id, trg_id,others.ToArray());
            }
            throw new ArgumentException("Строка " + act + " не распознана как действие ");
        }

        public static FaceAction FaceAction(decimal give_id, string act) {
            var parts = act.Split('_');
            var trg_id = decimal.Parse(parts[0].Substring(1));
            switch (parts[1]) {
                case "crt": {
                        var first = decimal.Parse(parts[2]);
                        var second = decimal.Parse(parts[3]);
                        var third = decimal.Parse(parts[4]);
                        return new CreateFace(give_id, trg_id, first, second, third);
                    }
                case "del":
                    return new DeleteFace(give_id, trg_id);
                case "flp":
                    return new FlipFace(give_id, trg_id);
            }
            throw new ArgumentException("Строка " + act + " не распознана как действие ");
        }

        public static EdgeAction EdgeAction(decimal give_id, string act) {
            var parts = act.Split('_');
            var trg_id = decimal.Parse(parts[0].Substring(1));
            switch (parts[1]) {
                case "crt": {
                        var first = decimal.Parse(parts[2]);
                        var second = decimal.Parse(parts[3]);
                        return new CreateEdge(give_id, trg_id, first, second);
                    }
                case "del":
                    return new DeleteEdge(give_id, trg_id);
            }
            throw new ArgumentException("Строка " + act + " не распознана как действие ");
        }

        public static ModelMatrixAction ModelMatrixAction(decimal give_id, string act) {
            if (act.IsSYS()) act = act.Substring(5);
            var parts = act.Split('_');
            var trg_id = decimal.Parse(parts[2]);
            //mm_dlt_25_0.1:0.2 ...
            switch (parts[1]) {
                case "dlt": {
                        return new ChangeModelMatrix(give_id, trg_id, Matrix4(parts[3]));
                    }
            }
            throw new ArgumentException("Строка " + act + " не распознана как действие ");
        }

        public static Matrix4 Matrix4(string m) {
            var parts = m.Split(':');
            Vector4 r1 = new Vector4(float.Parse(parts[0]), float.Parse(parts[1]), float.Parse(parts[2]), float.Parse(parts[3]));
            Vector4 r2 = new Vector4(float.Parse(parts[4]), float.Parse(parts[5]), float.Parse(parts[6]), float.Parse(parts[7]));
            Vector4 r3 = new Vector4(float.Parse(parts[8]), float.Parse(parts[9]), float.Parse(parts[10]), float.Parse(parts[11]));
            Vector4 r4 = new Vector4(float.Parse(parts[12]), float.Parse(parts[13]), float.Parse(parts[14]), float.Parse(parts[15]));
            return new Matrix4(r1, r2, r3, r4);
        }
    }

}
