using OpenTK;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSUtils {
    public static class Pack {

        public static string VertexAction(VertexAction action) {
            var s = "v" + action.Info.ID;
            if (action is CreateVertex) {
                s += string.Format("_crt_{0}_{1}_{2}_{3}", action.ObjectID, action.Info.X, action.Info.Y, action.Info.Z);
                if (action.Info.Color != null)
                    s += string.Format("_{0}_{1}_{2}_{3}", action.Info.Color.R, action.Info.Color.G, action.Info.Color.B, action.Info.Color.A);
                if (action.Info.TextureCoordinates != null)
                    s += string.Format("_{0}_{1}", action.Info.TextureCoordinates.X, action.Info.TextureCoordinates.Y);
                return s;
            }
            if (action is ChangeVertex) {
                s += string.Format("_dlt_{0}_{1}_{2}", action.Info.X, action.Info.Y, action.Info.Z);
                if (action.Info.Color != null)
                    s += string.Format("_{0}_{1}_{2}_{3}", action.Info.Color.R, action.Info.Color.G, action.Info.Color.B, action.Info.Color.A);
                if (action.Info.TextureCoordinates != null)
                    s += string.Format("_{0}_{1}", action.Info.TextureCoordinates.X, action.Info.TextureCoordinates.Y);
                return s;
            }
            if (action is DeleteVertex) {
                s += "_del";
                return s;
            }
            throw new ArgumentException("аргумент не является Create/Change/Delete действием");
        }
        public static string EdgeAction(EdgeAction action) {
            var s = "e" + action.EdgeID;
            if (action is CreateEdge) 
                return s + string.Format("_crt_{0}_{1}", action.FirstID, action.SecondID);
            
            if (action is DeleteEdge) 
                return s + "_del";
           
            throw new ArgumentException("аргумент не является Create/Delete действием");
        }

        public static string FaceAction(FaceAction action) {
            var s = "f" + action.FaceID;
            if (action is CreateFace) 
                return s + string.Format("_crt_{0}_{1}_{2}", action.FirstID, action.SecondID, action.ThirdID);
            if (action is FlipFace)
                return s + "_flp";
            if (action is DeleteFace)
                return s + "_del";

            throw new ArgumentException("аргумент не является Create/Flip/Delete действием");
        }

        public static string ObjectAction(ObjectAction action) {
            var s = "o" + action.ObjectID;
            if (action is CreateObject) {
                s += "_crt_" + Matrix4(action.ModelMatrix);
                return s;
            }
            if (action is DeleteObject) {
                s += "_del";
                return s;
            }
            if (action is JoinObject) {
                var a = action as JoinObject;
                s += "_jno";
                foreach (var x in a.others) s += "_" + x;
                return s;
            }
            throw new ArgumentException("аргумент не является Create/Delete действием");
        }

        public static List<string> Scene(IScene s) {
            var res = new MessagePacker();
            foreach(var o in s.Objects) {
                res += ObjectAction(new CreateObject(0, o.Key, o.Value.ModelMatrix)) + MSGUtils.Separator;
                foreach(var v in o.Value.Vertices) 
                    res += VertexAction(new CreateVertex(0M, v.Value, o.Key)) + MSGUtils.Separator;
                foreach (var e in o.Value.Edges)
                    res += EdgeAction(new CreateEdge(0M, e.Key, e.Value.First.ID, e.Value.Second.ID)) + MSGUtils.Separator;
                foreach (var f in o.Value.Faces)
                    res += FaceAction(new CreateFace(0M, f.Key, f.Value.First.ID, f.Value.Second.ID, f.Value.Third.ID)) + MSGUtils.Separator;
            }
            return res.Result;

        }
        public static List<string> FullObject(IObject s) {
            var res = new MessagePacker();
            res += ObjectAction(new CreateObject(0, s.ID, s.ModelMatrix)) + MSGUtils.Separator;
            foreach (var v in s.Vertices)
                res += VertexAction(new CreateVertex(0M, v.Value, s.ID)) + MSGUtils.Separator;
            foreach (var e in s.Edges)
                res += EdgeAction(new CreateEdge(0M, e.Key, e.Value.First.ID, e.Value.Second.ID)) + MSGUtils.Separator;
            foreach (var f in s.Faces)
                res += FaceAction(new CreateFace(0M, f.Key, f.Value.First.ID, f.Value.Second.ID, f.Value.Third.ID)) + MSGUtils.Separator;
            
            return res.Result;
        }


        public static string ModelMatrixAction(ModelMatrixAction c) {
            var res = "mm_dlt_";
            if (c is ChangeModelMatrix)
                res += ChangeModelMatrix((ChangeModelMatrix) c);
            return res;
        }

        public static string ChangeModelMatrix(ChangeModelMatrix c) {
            var res = c.ObjectID+"_";
            res += Matrix4(c.delta) + "_";
            return res;
        }

        public static string Matrix4(Matrix4 m) {
            var res = "";
            res += m.M11 + ":" + m.M12 + ":" + m.M13 + ":" + m.M14 + ":";
            res += m.M21 + ":" + m.M22 + ":" + m.M23 + ":" + m.M24 + ":";
            res += m.M31 + ":" + m.M32 + ":" + m.M33 + ":" + m.M34 + ":";
            res += m.M41 + ":" + m.M42 + ":" + m.M43 + ":" + m.M44;
            return res;
        }

        class MessagePacker {
            List<string> messages = new List<string>();
            string current = "";
            public MessagePacker() {}

            public void Add(string act) {
                current += act;
                if (current.Length*2+350>SYS.ByteLimit) {
                    messages.Add(current);
                    current = "";
                }
            }

            public static MessagePacker operator+(MessagePacker m, string s) {
                m.Add(s);
                return m;
            }

            public List<string> Result { get {
                    messages.Add(current);
                    var res = messages;
                    current = "";
                    messages = new List<string>();
                    return res;
                }
            }
        }
    }
}
