using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSUtils{
    public static class MSGUtils {
        public const string MSG_tag = "[MSG]";
        public const string SYS_tag = "[SYS]";
        public const string NET_tag = "[NET]";
        public const char Separator = '&';
        public const string ERROR_EMPTY_BUFFER = "Buffer was empty";
        public const string ERROR_CANTCONNECT = "Cant connect";
        public const string DISCONNECT = "Disconnect";
        public const string CONNECTED = "Connected successfully";
        public const string DISCONNECTED = "Disconnected";

        public enum State : byte {
            Disconnected, Connected, TryingReconnect, Connecting, Disconnecting, None, ConnectionTimeout, CantConnect
        }

        public static bool IsMSG(this string message) => string.CompareOrdinal(message, 0, MSG_tag, 0, 5) == 0 ? true : false;
        public static bool IsSYS(this string message) => string.CompareOrdinal(message, 0, SYS_tag, 0, 5) == 0 ? true : false;
        public static bool IsNET(this string message) => string.CompareOrdinal(message, 0, NET_tag, 0, 5) == 0 ? true : false;



    }
    public static class SYS {
        public const string NeedScene = "needscene";
        public const string ForwardScene = "forward"; // -? реально заменить на needscene
        public const string VertexIDPool = "iv";//запрос наборов айдишников для клиента
        public const string EdgeIDPool = "ie";  //типа [SYS]iv_25& - дай мне 25 айдишников
        public const string FaceIDPool = "if";  //
        public const string ObjectIDPool = "io";//
        public const string AllFirstIDPool = "iaf";//в первое подключение все пулы
        public const int ByteLimit = 1024;//лимит байт на одно сообщение
        public const string PartMark = "=>";//метка о частичности сообщения, "смотри следующее"
        public const char MessageDelimiter = '|';//в конце каждого сообщения, чтоб разделять их между собой

    }
}
