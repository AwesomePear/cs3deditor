using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSUtils {
    public static class Extensions {
        public static string PackToStr(this IVertex vert) {
            var res = "";
            res += vert.ID + "," + vert.X + "," + vert.Y + "," + vert.Z;
            if (vert.Color != null) res += ","+ vert.Color.R + "," + vert.Color.G + "," + vert.Color.B + "," + vert.Color.A;
            if (vert.TextureCoordinates != null) res += ","+ vert.TextureCoordinates.X + "," + vert.TextureCoordinates.Y;
            return res += Environment.NewLine;
        }
        public static string PackToStr(this IEdge e) =>
            e.ID + "," + e.First.ID + "," + e.Second.ID + Environment.NewLine;

        public static string PackToStr(this IFace f) => 
            f.ID + "," + f.First.ID + "," + f.Second.ID + "," + f.Third.ID + Environment.NewLine;


        public static string PackToStr(this IObject obj) {
            obj.AcceptModelMatrix();
            var res = "";
            res += obj.ID + Environment.NewLine;
            foreach (var v in obj.Vertices) 
                res += "v"+ v.Value.PackToStr();
            foreach (var e in obj.Edges) 
                res += "e" + e.Value.PackToStr();
            foreach (var f in obj.Faces) 
                res += "f" + f.Value.PackToStr();
            return res += "|";
        }

    }
}
