using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSUtils {
    public static class FileWork {
        public static void SaveTo(string path) {
            FileInfo fileInf = new FileInfo(path);
            using (StreamWriter sw = File.CreateText(path)) { //запись
                foreach (var o in UserAction.Context.Objects) {
                    sw.Write(o.Value.PackToStr());
                }
            }
        }

        public static string ReadFrom(string path) {
            if (!File.Exists(path)) throw new FileNotFoundException("Файл "+path+ " не существует!");
            string res = "";
            using (StreamReader sr = File.OpenText(path)) {
                res += File.ReadAllText(path);
            }
            return res;
        }
    }

    
}
