using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSUtils {
    public interface IScene {
        Dictionary<decimal, IObject> Objects { get; }
        (IObject, IVertex) GetVertexByID(decimal id);
        (IObject, IEdge) GetEdgeByID(decimal id);
        (IObject, IFace) GetFaceByID(decimal id);
        IObject GetObjectByID(decimal id);
        void DeleteVertex(IObject o, IVertex v);
        void CreateVertex(IObject o, VertexInfo v);
        void CreateObject(decimal ID);
        void DeleteObject(decimal ID);
        void JoinObjects(decimal main, params decimal[] other);
        //void DuplicateObject(decimal main, decimal newo);
    }
    public interface IColor {
        float R { get; set; }
        float G { get; set; }
        float B { get; set; }
        float A { get; set; }
        IColor Copy();
        IColor Inverted();
    }
    public interface ITextureCoordinates {
        float X { get; set; }
        float Y { get; set; }
        ITextureCoordinates Copy();
        ITextureCoordinates Inverted();
    }
    public interface IVertex {
        decimal ID { get; }
        float X { get; set; }
        float Y { get; set; }
        float Z { get; set; }
        IColor Color { get; set; }
        ITextureCoordinates TextureCoordinates { get; set; }
        VertexInfo GetCurrentState();
        void AddVertexInfo(VertexInfo info);
        void SetVertexInfo(VertexInfo info);
    }
    public interface IEdge {
        decimal ID { get; }
        IVertex First { get; }
        IVertex Second { get; }
        bool Contains(params IVertex[] v);
        bool Contains(params decimal[] v);
    }
    public interface IFace {
        decimal ID { get; }
        IVertex First { get; }
        IVertex Second { get; }
        IVertex Third { get; }
        bool Contains(params IVertex[] v);
        bool Contains(params decimal[] v);

    }
    public interface IObject {
        Dictionary<decimal, IVertex> Vertices { get; }
        Dictionary<decimal, IFace> Faces { get; }
        Dictionary<decimal, IEdge> Edges { get; }

        Matrix4 ModelMatrix { get; set; }
        void AcceptModelMatrix();
        decimal ID { get; }

        //IObject Copy(decimal newoid);

        void Join(IObject other);
        void CreateVertex(VertexInfo info);
        void DeleteVertex(decimal ID);
        IVertex GetVertexByID(decimal ID);

        void CreateEdge(decimal ID, IVertex f, IVertex s);
        void CreateEdge(decimal ID, decimal f_id, decimal s_id);
        void DeleteEdge(decimal ID);
        void DeleteEdge(decimal f_id, decimal s_id);
        void DeleteEdge(IVertex f, IVertex s);
        IEdge GetEdge(decimal ID);
        IEdge GetEdge(IVertex f, IVertex s);
        IEdge GetEdge(decimal f, decimal s);
        void CreateFace(decimal ID, IVertex f, IVertex s, IVertex t);
        void CreateFace(decimal ID, decimal f_id, decimal s_id, decimal t_id);
        void DeleteFace(decimal ID);
        void DeleteFace(IVertex f, IVertex s, IVertex t);
        void DeleteFace(decimal f, decimal s, decimal t);
        IFace GetFace(decimal ID);
        IFace GetFace(IVertex f, IVertex s, IVertex t);
        IFace GetFace(decimal f, decimal s, decimal t);

    }

}
