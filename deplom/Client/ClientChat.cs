using Editor.Graphics;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace Editor {
    public class ClientChat {
        private MainWindow Window;
        public ClientChat(MainWindow window) {
            Window = window;
        }

        public  void AddMessage(DateTime time, string nickname, string message) {
            string _time = "[" + time.Hour + ":" + time.Minute + ":" + time.Second + "] ";
            string res = _time + nickname + ": " + message.Trim(' ');

            MainWindow.FromGUIThread(Window.ChatMessages, () => Window.ChatText += Environment.NewLine + res);

        }
        public  void AddMessage((DateTime time, string nickname, string message) x) => AddMessage(x.time, x.nickname, x.message);
        public  string GetNewMessage() {
            var text = Window.ChatNewMessage.Text;
            Window.ChatNewMessage.Text = "";
            return text;
        }


    }
}
