using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using CSUtils;
using Editor.Graphic.RenderObject.Base;
using Editor.Graphic.Window;
using Editor.Graphics;
using static CSUtils.MSGUtils;

namespace Editor {
    public class Client :DependencyObject {
        public ClientIDPool IDPool;
        private ClientChat Chat;
        private TcpClient TcpClient;
        private MainWindow Window;
        private bool connected = false;
        private State currentState;
        private int Timeout = 300;//ms
        private static Client o;
        public static Client Get => o;
        public bool ForwardScene = true;


        public string ServerIp {
            get { return (string)GetValue(ServerIpProperty); }
            set { SetValue(ServerIpProperty, value); }
        }
        public static readonly DependencyProperty ServerIpProperty =
            DependencyProperty.Register("ServerIp", typeof(string), typeof(Client), new PropertyMetadata("127.0.0.1"));

        public int ServerPort {
            get { return (int)GetValue(ServerPortProperty); }
            set { SetValue(ServerPortProperty, value); }
        }
        public static readonly DependencyProperty ServerPortProperty =
            DependencyProperty.Register("ServerPort", typeof(int), typeof(Client), new PropertyMetadata(14500));


        public string Password {
            get { return (string)GetValue(PasswordProperty); }
            set { SetValue(PasswordProperty, value); }
        }
        public static readonly DependencyProperty PasswordProperty =
            DependencyProperty.Register("Password", typeof(string), typeof(Client), new PropertyMetadata("12547"));
        public string Nickname {
            get { return (string)GetValue(NicknameProperty); }
            set { SetValue(NicknameProperty, value); }
        }
        public static readonly DependencyProperty NicknameProperty =
            DependencyProperty.Register("Nickname", typeof(string), typeof(Client), new PropertyMetadata("unnamed"));



        public const string RED = "#FFDD5656";
        public const string GREEN = "#FF56DD56";
        public bool Connected {
            get => connected; set {
                try {
                    connected = value;
                    if (connected) Window.ConnectionStatusColor = GREEN; else Window.ConnectionStatusColor = RED;
                } catch {
                    MainWindow.FromGUIThread(Window.ConnectionStatus, () => { if (connected = value) Window.ConnectionStatusColor = GREEN; else Window.ConnectionStatusColor = RED; });
                }
            }
        }

        public State CurrentState {
            get => currentState; private set {
                currentState = value;
            }
        }

        public Client(MainWindow window) {
            o = this;
            Chat = new ClientChat(window);
            Window = window;
            CurrentState = State.None;
            IDPool = ClientIDPool.Get;
        }

        public void Connect() {
            try {
                var IP = IPAddress.Parse(ServerIp);
                CurrentState = State.Connecting;
                TcpClient = new TcpClient();
                TcpClient.Connect(IP, ServerPort);
                SendMessage(NET_tag, Nickname+Separator+Password);
                Thread.Sleep(300);
                StartListening();
            } catch  {
                CurrentState = State.CantConnect;
            }
        }

        public void Disconnect() {
            SendMessage(NET_tag, DISCONNECT);
            TcpClient.Close();
            StopListening();
            Connected = false;
        }

        /// <summary>
        /// Отправка сообщения на сервер
        /// </summary>
        public void SendMessage(string tag, string message) {
            string final = "";
            if (!(message.Trim().Length > 0)) return;
            if (tag == MSG_tag) final = MSG_tag + message + Separator;
            else if (tag == NET_tag) {
                if (!(message.Trim().Length > 0)) throw new FieldNotSetException("Nickname is empty! :(");
                final = NET_tag + message + Separator;
            } else if (tag == SYS_tag) {
                if (message.Length == 0) return;
                final = tag + message + Separator;
            }
            Send(final);
        }
        public void SendMessage(string tag, List<string> message) {
            //message.ForEach(x => Debug.WriteLine(x));
            foreach (var x in message) SendMessage(tag, x);
        }

        public void SendMSG() => SendMessage(MSG_tag, Chat.GetNewMessage());

        private void Send(string message) {
            message += SYS.MessageDelimiter;
            Debug.WriteLine("Sent to server: "+message);
            byte[] Buffer = Encoding.Unicode.GetBytes(message);
            TcpClient.GetStream().Write(Buffer, 0, Buffer.Length);
        }

        private static (DateTime time, string nickname, string message) UnpackMSGFromServer(string s) {
            try {
                s = s.Substring(5);//снимаем тег
                var parts = s.Split(Separator);
                return (DateTime.Parse(parts[0]), parts[1], parts[2]);
            } catch (Exception e) {
                Debug.WriteLine("UnpackMSGFromServer");
                Debug.WriteLine(e);
                Debug.WriteLine(e.StackTrace);
                return (DateTime.MinValue, "shit", "happened");
            }
        }


        public void SendVertexActions(IEnumerable<VertexAction> a, bool last = false) {
            var s = "";
            foreach (var x in a)
                s += Pack.VertexAction(x) + Separator;
            SendMessage(SYS_tag,s +(last? "_l":""));
        }
        public void SendModelMatrixActions(IEnumerable<ModelMatrixAction> a, bool last = false) {
            var s = "";
            foreach (var x in a)
                s += Pack.ModelMatrixAction(x) + Separator;
            SendMessage(SYS_tag, s + (last ? "_l" : ""));
        }
        public void SendScene() {
            //Debug.WriteLine(Scene.Get);
            var mess = Pack.Scene(Scene.Get);
            foreach (var x in mess) 
                SendMessage(SYS_tag,x);
            
        }

        static object ReadLocker = new object();
        private void ReactOnMessage(string message) {
            try {
                if (message == null || message.Length == 0) return;

                if (message == ERROR_CANTCONNECT) {
                    CurrentState = State.CantConnect;
                    return;
                }
                if (message == ERROR_EMPTY_BUFFER) {
                    if (CurrentState == State.Connecting)
                        CurrentState = State.ConnectionTimeout;
                    
                    return;
                }
                if (message.IsSYS()) {
                    try {
                        if (message[5] == 'i') {
                            IDPool.Parse(message);
                            return;
                        }
                        decimal temp_id_counter = 0;
                        foreach (var x in Parse.FullMessage(ref temp_id_counter, message)) {
                            if (x is CreateEdge) Debug.WriteLine("x now - ["+((CreateEdge)x).FirstID+", "+((CreateEdge)x).SecondID +"]");
                            x.Accept();
                        }
                         
                    } catch (Exception e) {
                        Debug.WriteLine("ReactOnMessage -> SYS" + Environment.NewLine + e + Environment.NewLine + e.StackTrace);
                        MessageBox.Show(e.Message + Environment.NewLine + e.StackTrace);
                    }
                    return;
                } else if (message.IsMSG()) {

                    Chat.AddMessage(UnpackMSGFromServer(message));
                } else if (message.IsNET()) {
                    var mes = message.Substring(5);
                    var parts = mes.Split(Separator);
                    if (parts[0] == CONNECTED) {
                        CurrentState = State.Connected;
                        Connected = true;
                        if (string.Equals(parts[1],"forward", StringComparison.OrdinalIgnoreCase)) {
                            SendScene();
                        } else {
                            Scene.Get.Clear();
                            SendMessage(NET_tag, "needscene");
                        }
                    } else if (parts[0] == DISCONNECT || parts[0]=="kicked") {
                        CurrentState = State.Disconnected;
                        Connected = false;
                    }

                } else {
                    throw new WTF("мусор пришел: " + message);
                }
            }
            catch (Exception e) {
                Debug.WriteLine("ReactOnMessage");
                Debug.WriteLine(e);
                Debug.WriteLine(e.StackTrace);
            }
        }

        
        /*private string ReadMessage() {
            
            try {
                var Avail = TcpClient.Available;
                byte[] buffer = new byte[Avail];
                if (Avail == 0 && TcpClient.Connected) return ERROR_EMPTY_BUFFER;
                int bytes = TcpClient.GetStream().Read(buffer, 0, buffer.Length);
                if (bytes == 0) return ERROR_EMPTY_BUFFER;
                return Encoding.Unicode.GetString(buffer);
            } catch {
                return ERROR_CANTCONNECT;
            }
        }*/

        public void CheckMessages() {
            if (CurrentState == State.Connecting) Thread.Sleep(Timeout);
            try {
                lock (ReadLocker) 
                    if (HaveMessage) ReactOnMessage(ReadMessage());
            } catch (Exception e) {
                MessageBox.Show(e.Message+Environment.NewLine+e.StackTrace);
            }
        }

        public string ReadMessage() {
            try {
                return GetMessage();
            } catch (Exception e) {
                Debug.WriteLine(e);
                return ERROR_CANTCONNECT;
            }
        }
        public bool HaveMessage => message_part.Length > 0 || TcpClient.Available > 0;

        private string GetRawMessage() {
            var buffer = new byte[SYS.ByteLimit];
            int bytes = TcpClient.GetStream().Read(buffer, 0, buffer.Length);
            if (bytes == 0) return null;
            return Encoding.Unicode.GetString(buffer);
        }
        private string message_part = "";
        private string GetMessage() {
            int delimiter = 0;
            var message = "";
            if (message_part.Length > 0 && message_part.First() != '[') message_part = "";
            if (message_part.Length > 0) { //если не дочитали
                delimiter = message_part.IndexOf(SYS.MessageDelimiter);
                if (delimiter == -1) {  // если последнее недочитанное, возможен разрыв
                    message = message_part;
                    message_part = "";
                } else { //если не последнее недочитанное
                    var res = message_part.Substring(0, delimiter);
                    message_part = message_part.Substring(delimiter + 1);
                    return res;
                }
            }
            if (TcpClient.Available > 0) {//если есть что читать из буфера
                var raw = GetRawMessage().Trim();
                if (message.Length != 0) { //если уже достали кусок от прошлого
                    if (raw != null) {//если что-то было вообще
                        delimiter = raw.IndexOf(SYS.MessageDelimiter);
                        message += raw.Substring(0, delimiter);
                        message_part = raw.Substring(delimiter + 1);
                    }
                    return message;
                } else { //если недочитанных огрызков нет
                    delimiter = raw.IndexOf(SYS.MessageDelimiter);
                    message = raw.Substring(0, delimiter);
                    message_part = raw.Substring(delimiter + 1);
                    return message;
                }
            }
            return message;
        }



        System.Timers.Timer AutoUpdate;
        bool Listening = false;
        private void StartListening() {
            if (Listening) return;
            Debug.WriteLine("Listening start");
            AutoUpdate = new System.Timers.Timer(20); // Timer, каждые 200 мс чтение
            AutoUpdate.Elapsed += (object o, ElapsedEventArgs args) => Update();
            AutoUpdate.Start();
            Listening = true;
        }
        private void StopListening() {
            if (!Listening) return;
            Debug.WriteLine("Listening stop");
            AutoUpdate.Stop();
            Listening = false;
        }


        private void Update() {
            var a = Monitor.TryEnter(ReadLocker);
            if (a == false) return;
            else Monitor.Exit(ReadLocker);
            CheckMessages();
            switch (CurrentState) {
                case State.Connected:
                    if (!Connected) Connected = true;
                    AutoUpdate.Start();
                    break;

                case State.Disconnected:
                    if (Connected) Connected = false;
                    CurrentState = State.None;
                    StopListening();
                    break;

                case State.ConnectionTimeout:
                    if (Connected) Connected = false;
                    CurrentState = State.None;
                    StopListening();
                    break;

                case State.CantConnect:
                    if (Connected) Connected = false;
                    CurrentState = State.None;
                    StopListening();
                    break;
            }
        }
    }

}
