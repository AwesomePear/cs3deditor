using CSUtils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Editor {
    public class ClientIDPool {
        private static ClientIDPool o = new ClientIDPool();
        public static ClientIDPool Get => o;
        private  ClientIDPool() {}
        Queue<decimal> Vertex = new Queue<decimal>(20200);
        Queue<decimal> Edge   = new Queue<decimal>(20200);
        Queue<decimal> Face   = new Queue<decimal>(20200);
        Queue<decimal> Object = new Queue<decimal>(20200);

        public enum Type : byte {
            Vertex, Edge, Face, Object, ALL
        }
        public decimal GetNext(Type t) {
            decimal res = 0;
            switch (t) {
                case Type.Vertex:
                    res = Vertex.Dequeue();
                    if (Vertex.Count < 200) Require(t);
                    break;
                case Type.Edge:
                    res = Edge.Dequeue();
                    if (Edge.Count < 200) Require(t);
                    break;
                case Type.Face:
                    res = Face.Dequeue();
                    if (Face.Count < 200) Require(t);
                    break;
                case Type.Object:
                    res = Object.Dequeue();
                    if (Object.Count < 200) Require(t);
                    break;
            }
            return res;

        }

        private void Require(Type t) {
            var s = "i";
            switch (t) {
                case Type.Vertex: s += 'v';break;
                case Type.Edge:   s += 'e';break;
                case Type.Face:   s += 'f';break;
                case Type.Object: s += 'o';break;
            }
            s += "_" + 20000 + "_";
            Client.Get.SendMessage(MSGUtils.SYS_tag, s);
            time_to_go = false;
            while (!time_to_go) 
                Thread.Sleep(5);
            
        }

        bool time_to_go = false;
        public void Load(Type t, decimal from, decimal to) {
            switch (t) {
                case Type.Vertex:
                    for (decimal i = from; i <= to; i++)
                        Vertex.Enqueue(i);
                    break;
                case Type.Edge:
                    for (decimal i = from; i <= to; i++)
                        Edge.Enqueue(i);
                    break;
                case Type.Face:
                    for (decimal i = from; i <= to; i++)
                        Face.Enqueue(i);
                    break;
                case Type.Object:
                    for (decimal i = from; i <= to; i++)
                        Object.Enqueue(i);
                    break;
                default: throw new Graphic.RenderObject.Base.WTF("тычесделол");
            }
            time_to_go = true;
        }



        public void Parse(string msg) {
            //[SYS]iv_25-125_&|
            msg = msg.Substring(6);
            //v_25-125_&|
            var pair = msg.Split('_')[1].Split('-');
            switch (msg.First()) {
                case 'v':
                    Load(Type.Vertex, decimal.Parse(pair[0]), decimal.Parse(pair[1]));
                    break;
                case 'e':
                    Load(Type.Edge, decimal.Parse(pair[0]), decimal.Parse(pair[1]));
                    break;
                case 'f':
                    Load(Type.Face, decimal.Parse(pair[0]), decimal.Parse(pair[1]));
                    break;
                case 'o':
                    Load(Type.Object, decimal.Parse(pair[0]), decimal.Parse(pair[1]));
                    break;
                case 'a':
                    /*ia_v_100-200_e_100-200_f_100-200_o_100-200_
                       0 1    2    3    4    5    6    7    8*/
                    var pairs = msg.Split('_');
                    pair = pairs[2].Split('-');
                    Load(Type.Vertex, decimal.Parse(pair[0]), decimal.Parse(pair[1]));
                    pair = pairs[4].Split('-');
                    Load(Type.Edge, decimal.Parse(pair[0]), decimal.Parse(pair[1]));
                    pair = pairs[6].Split('-');
                    Load(Type.Face, decimal.Parse(pair[0]), decimal.Parse(pair[1]));
                    pair = pairs[8].Split('-');
                    Load(Type.Object, decimal.Parse(pair[0]), decimal.Parse(pair[1]));
                    break;
            }
            Debug.WriteLine("Client received IDs: "+msg);
        }
    }
}
