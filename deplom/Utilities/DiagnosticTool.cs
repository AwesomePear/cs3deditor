using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Editor.Utilities {
    static public class DiagnosticTool {
        private static DateTime start;
        private static DateTime stop;
        public static void Start() {
            start = DateTime.Now;
        }
        public static void Stop() {
            stop = DateTime.Now;
            Debug.WriteLine("Time spent: "+(stop-start).TotalMilliseconds +" ms");
            Debug.WriteLine("Callbacks: " + callbacks.DictionaryToString());
        }
        private static Dictionary<string,int> callbacks = new Dictionary<string, int>();
        public static void CB(string func) {
            if (!callbacks.ContainsKey(func)) callbacks.Add(func, 1);
            else callbacks[func]++;
        }

    }
}
