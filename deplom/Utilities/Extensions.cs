using Editor.Graphic.RenderObject.Base;
using OpenTK;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Editor.Utilities {
    public static class Extensions {
        public static string ListToString<T>(this List<T> list) {
            var res = "";
            for (int i = 0; i < list.Count; i++) {
                res += list[i] + "  ";
            }
            return res;
        }
        public static string ArrayToString<T>(this T[] ar) {
            var res = "";
            for (int i = 0; i < ar.Length; i++) {
                res += ar[i] + "  ";
            }
            return res;
        }
        public static List<VertexState> GetStates(this List<Vertex> list) {
            var res = new List<VertexState>();
            foreach (var x in list)
                res.Add(x.GetState());
            return res;
        }
        public static List<Matrix4> GetModelMatrices(this List<BasicRenderObject> list) {
            var res = new List<Matrix4>();
            foreach (var x in list)
                res.Add(x.ModelMatrix);
            return res;
        }

        public static bool Contains(this EdgesCBList l, Vertex v1, Vertex v2) {
            foreach (var x in l) if (x.Contains(v1, v2)) return true;
            return false;
        }
        public static bool Contains(this FacesCBList l, Vertex v1, Vertex v2, Vertex v3) {
            foreach (var x in l) if (x.Contains(v1, v2, v3)) return true;
            return false;
        }

        public static string DictionaryToString<T1,T2>(this Dictionary<T1,T2> d) {
            var res = "";
            foreach(var x in d) 
                res += x.Key + ": " + x.Value + Environment.NewLine;
            
            return res;
        }
        public static void AddRange<T>(this List<T> l, params T[] range) => l.AddRange(range);
        public static void RemoveRange<T>(this List<T> l, IEnumerable<T> range) {
            foreach (T x in range)
                l.Remove(x);
        }
        public static void RemoveRange<T1,T2>(this Dictionary<T1,T2> l, IEnumerable<T2> range) {
            foreach (T2 x in range)
                l.Remove(((KeyValuePair<T1,T2>)l.Find(o => o.Equals(x))).Key);
        }
        public static void Remove<T>(this List<T> l, Predicate<T> predicate) {
            l.RemoveAt(l.FindIndex(predicate));
        }

        public static void AddUniqueRange<T>(this List<T> l, IEnumerable<T> range) {
            foreach (T x in range)
                if (!l.Contains(x)) l.Add(x);
        }

        public static Coordinates Coord(this (float x, float y, float z) coords) => new Coordinates(coords.x, coords.y, coords.z);

        public static KeyValuePair<T1, T2>? Find<T1, T2>(this Dictionary<T1, T2> l, Predicate<T2> pred) {
            foreach (var x in l)
                if (pred(x.Value)) return x;
            return null;
        }
        public static Dictionary<T1, T2> FindAll<T1, T2>(this Dictionary<T1, T2> l, Predicate<T2> pred) {
            var res = new Dictionary<T1, T2>();
            foreach (var x in l)
                if (pred(x.Value)) res.Add(x.Key, x.Value);
            return res;
        }
    }

}
