using Editor.Graphic.RenderObject.Base;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Editor.Utilities {

    public delegate void VoidToInt(int index);
    public delegate void VoidToVoid();
    public delegate void VoidToRange(int from, int to);

    public class VertexCBList : List<Vertex>{
        public VoidToInt AddCallback = null;
        public VoidToInt DeleteCallback = null;
        public VoidToInt SetCallback = null;
        public VoidToInt InsertCallback = null;
        public VoidToVoid ClearCallback = null;
        public VoidToRange AddRangeCallback = null;
        public VoidToRange InsertRangeCallback = null;

        public VertexCBList(int capacity = 80):base(capacity) {}

        public VertexCBList CloneWithClones() {
            var res = new VertexCBList();
            for(int i = 0; i<Count; i++) {
                res.Add(new Vertex(this[i].GetCurrentState(),true));
            }
            return res;
        }

        public void onVertexChanging(Vertex v) {
            SetCallback?.Invoke(IndexOf(v));
#if DEBUG
            DiagnosticTool.CB("onVertexChanging");
#endif
        }

        public new void Add(Vertex smth) {
            base.Add(smth);
            AddCallback?.Invoke(base.Count-1);
            smth.VertexChanging += onVertexChanging;
#if DEBUG
            DiagnosticTool.CB("Add");
#endif
        }
        public void AddUnique(Vertex smth) {
            if (Contains(smth)) return;
            base.Add(smth);
            AddCallback?.Invoke(base.Count - 1);
            smth.VertexChanging += onVertexChanging;
#if DEBUG
            DiagnosticTool.CB("Add");
#endif
        }
        public void Delete(Vertex smth) => Delete(IndexOf(smth));
        public void Delete(int pos) {
            DeleteCallback?.Invoke(pos);
            base[pos].VertexChanging -= onVertexChanging;
            base.RemoveAt(pos);
#if DEBUG
            DiagnosticTool.CB("Delete");
#endif
        }
        public void DeleteByID(decimal ID) {
            for (int i = 0; i<Count; i++)
                if (this[i].ID == ID) {
                    Delete(i);
                    return;
                }
        }
        public Vertex GetByID(decimal ID) {
            foreach (var x in this) if (x.ID == ID) return x;
            return null;
        }
        public new void Remove(Vertex smth) => Delete(smth);
        public new void RemoveAt(int pos) => Delete(pos);
        public new void Insert(int pos, Vertex smth) {
            base.Insert(pos, smth);
            smth.VertexChanging += onVertexChanging;
            InsertCallback?.Invoke(pos);
#if DEBUG
            DiagnosticTool.CB("Insert");
#endif
        }

        public new void Clear() {
            foreach (var x in this)
                x.VertexChanging -= onVertexChanging;
            ClearCallback?.Invoke();
            base.Clear();
#if DEBUG
            DiagnosticTool.CB("Clear");
#endif
        }
        public new void AddRange(IEnumerable<Vertex> vs) {
            var from = Count;
            foreach(var x in vs) {
                base.Add(x);
                x.VertexChanging += onVertexChanging;
            }
            var to = Count - 1;
            AddRangeCallback?.Invoke(from, to);
#if DEBUG
            DiagnosticTool.CB("AddRange");
#endif
        }
        public new void InsertRange(int into, IEnumerable<Vertex> vs) {
            var from = into;
            foreach (var x in vs) 
                x.VertexChanging += onVertexChanging;
            base.InsertRange(into, vs);
            var to = into+vs.Count() - 1;
            InsertRangeCallback?.Invoke(from, to);
#if DEBUG
            DiagnosticTool.CB("InsertRange");
#endif
        }

        public new Vertex this[int i] {
            get { return base[i]; }
            set {
                base[i].VertexChanging -= onVertexChanging;
                base[i] = value;
                base[i].VertexChanging += onVertexChanging;
                SetCallback?.Invoke(i);
#if DEBUG
                DiagnosticTool.CB("[i] set");
#endif
            }
        }
    }

    public class FacesCBList : List<Face> {
        public VoidToInt AddCallback = null;
        public VoidToInt DeleteCallback = null;
        public VoidToInt SetCallback = null;
        public VoidToInt InsertCallback = null;
        public VoidToVoid ClearCallback = null;
        public VoidToRange AddRangeCallback = null;
        public VoidToRange InsertRangeCallback = null;

        public FacesCBList(int capacity = 80) : base(capacity) { }

        public new void Add(Face smth) {
            base.Add(smth);
            AddCallback?.Invoke(base.Count - 1);
        }
        public void Delete(Face smth) {
            Delete(IndexOf(smth));
        }
        public void Delete(int pos) {
            DeleteCallback?.Invoke(pos);
            base.RemoveAt(pos);
        }
        public new void Remove(Face smth) {
            Delete(smth);
        }
        public new void RemoveAt(int pos) {
            Delete(pos);
        }
        public new void Insert(int pos, Face smth) {
            base.Insert(pos, smth);
            InsertCallback?.Invoke(pos);
        }

        public new void Clear() {
            ClearCallback?.Invoke();
            base.Clear();
        }
        public new void AddRange(IEnumerable<Face> vs) {
            var from = Count;
            base.AddRange(vs);
            var to = Count - 1;
            AddRangeCallback?.Invoke(from, to);

        }
        public new void InsertRange(int into, IEnumerable<Face> vs) {
            var from = into;
            base.InsertRange(into, vs);
            var to = into + vs.Count() - 1;
            InsertRangeCallback?.Invoke(from, to);
        }

        public new Face this[int i] {
            get { return base[i]; }
            set {
                base[i] = value;
                SetCallback?.Invoke(i);
            }
        }
    }

    public class EdgesCBList : List<Edge> {
        public VoidToInt AddCallback = null;
        public VoidToInt DeleteCallback = null;
        public VoidToInt SetCallback = null;
        public VoidToInt InsertCallback = null;
        public VoidToVoid ClearCallback = null;
        public VoidToRange AddRangeCallback = null;
        public VoidToRange InsertRangeCallback = null;

        public EdgesCBList(int capacity = 80) : base(capacity) { }

        public new void Add(Edge smth) {
            base.Add(smth);
            AddCallback?.Invoke(base.Count - 1);
        }
        public void Delete(Edge smth) {
            Delete(IndexOf(smth));
        }
        public void Delete(int pos) {
            DeleteCallback?.Invoke(pos);
            base.RemoveAt(pos);
        }
        public new void Remove(Edge smth) {
            Delete(smth);
        }
        public new void RemoveAt(int pos) {
            Delete(pos);
        }
        public new void Insert(int pos, Edge smth) {
            base.Insert(pos, smth);
            InsertCallback?.Invoke(pos);
        }
        public new void Clear() {
            ClearCallback?.Invoke();
            base.Clear();
        }
        public new void AddRange(IEnumerable<Edge> vs) {
            var from = Count;
            base.AddRange(vs);
            var to = Count - 1;
            AddRangeCallback?.Invoke(from, to);
        }
        public new void InsertRange(int into, IEnumerable<Edge> vs) {
            var from = into;
            base.InsertRange(into, vs);
            var to = into + vs.Count() - 1;
            InsertRangeCallback?.Invoke(from, to);
        }

        public new Edge this[int i] {
            get { return base[i]; }
            set {
                base[i] = value;
                SetCallback?.Invoke(i);
            }
        }
    }

}
