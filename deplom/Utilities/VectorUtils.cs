using Editor.Graphic.RenderObject.Base;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Editor.Utilities {
    public static class VectorUtils {
        public static Vector3 Cross(this Vector3 a, Vector3 b) {
            return new Vector3(
                a.Y * b.Z - a.Z * b.Y,
                a.Z * b.X - a.X * b.Z,
                a.X * b.Y - a.Y * b.X);
        }

        public static Coordinates Coord(this Vector3 a) => new Coordinates(a.X, a.Y, a.Z);
        public static Point toPoint(this Vector2 v) => new Point(v.X, v.Y);

        public static Vector3 Normalize_by_hands(this Vector3 v) {
            var l = 1/v.Length;
            return new Vector3(v.X * l, v.Y * l, v.Z * l);
        }

        public static Vector3 v3(this (float a, float b, float c) t) => new Vector3(t.a, t.b, t.c);
        public static Vector4 v4(this (float a, float b, float c, float d) t) => new Vector4(t.a, t.b, t.c, t.d);

        public static Vector3 CenterPoint(this List<Vertex> l) {
            Vector3 sum = new Vector3(0, 0, 0);
            foreach (var x in l) sum += x.Position.ToVector3();
            sum /= l.Count;
            return sum;
        }
        public static Vector3 CenterPoint(this List<BasicRenderObject> l) {
            Vector3 sum = new Vector3(0, 0, 0);
            foreach (var o in l)
                sum += o.Vertices.CenterPoint();
            sum /= l.Count;
            return sum;
        }
        public static Vector3 CenterPoint(this (Vertex a, Vertex b, Vertex c) l) {
            return (l.a.Position.ToVector3() + l.b.Position.ToVector3() + l.c.Position.ToVector3()) / 3f;
        }

        public static Vector2 MultiplyVertical(this Matrix2 M, Vector2 v) {
            return new Vector2(M.M11*v.X + M.M12*v.Y, M.M21 * v.X + M.M22 * v.Y);
        }

        public static Vector3 SetLength(this Vector3 v,float len) {
            return v.Normalized() * len;
        }

    }
}
