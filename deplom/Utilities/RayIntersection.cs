using Editor.Graphic.RenderObject.Base;
using Editor.Graphic.Window;
using Editor.Graphics;
using Editor.Graphics.Cameras;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Editor.Utilities {
    public static class Intersection {
        public static float withSphere(Ray ray, Vector3 sphere_pos, float r) {
            Vector3 k = ray.Origin - sphere_pos;//расстояние луч-центр сферы
            float b = Vector3.Dot(k, ray.Direction);
            float c = Vector3.Dot(k, k) - r * r;
            float d = b * b - c;
            if (d >= 0) {
                float sqrtfd = (float)Math.Sqrt(d);
                float t1 = -b + sqrtfd;
                float t2 = -b - sqrtfd;
                float min_t = Math.Min(t1, t2);
                float max_t = Math.Max(t1, t2);
                float t = (min_t >= 0) ? min_t : max_t;
                return t;
            }
            return -1;
        }
        public static Vector3? withPlane(Ray ray, Vector3 normal, float distance) {
            Vector4 plane = new Vector4(normal, distance);
            if (Vector4.Dot(plane, new Vector4(ray.Direction, 0f)) == 0) return null; // пересечения нет
            float t = -(Vector4.Dot(plane, new Vector4(ray.Origin, 1f)) / Vector4.Dot(plane, new Vector4(ray.Direction, 0f)));//коэф. P(t) = ray.Origin+t*ray.Dir
            return ray.Origin + t * ray.Direction; // точка пересечения с плоскостью
        }

        public static Vector3? withTriangle(Ray ray, Face triangle) {
            /*находим точку пересечения с плоскостью*/
            //вершины треугольника
            var P0 = triangle.Vertices.First.Position.ToVector3();
            var P1 = triangle.Vertices.Second.Position.ToVector3();
            var P2 = triangle.Vertices.Third.Position.ToVector3();
            Vector3 Normal = Vector3.Cross(P1-P0,P2-P0); // вектор нормали плоскости; перпендикулярен двум векторам, лежащим в ней
            float D = Vector3.Dot(-Normal, P0); // расстояние от плоскости до начала координат (длина нормали из Р0)
            Vector4 plane = new Vector4(Normal, D);
            if (Vector4.Dot(plane, new Vector4(ray.Direction, 0f)) == 0) return null; // пересечения нет
            float t = -(Vector4.Dot(plane, new Vector4(ray.Origin, 1f)) / Vector4.Dot(plane, new Vector4(ray.Direction, 0f)));//коэф. P(t) = ray.Origin+t*ray.Dir
            Vector3 intersection = ray.Origin + t*ray.Direction; // точка пересечения с плоскостью

            /*проверяем, попадает ли точка в полигон*/
            Vector3 R = intersection - P0;
            Vector3 Q1 = P1 - P0;  Vector3 Q2 = P2 - P0;

            float sqr_Q1 = Vector3.Dot(Q1, Q1);
            float sqr_Q2 = Vector3.Dot(Q2, Q2);

            float Q1dotQ2 = Vector3.Dot(Q1, Q2);
            float RdotQ1 = Vector3.Dot(R, Q1);
            float RdotQ2 = Vector3.Dot(R, Q2);

            var M = new Matrix2(sqr_Q1, Q1dotQ2, Q1dotQ2, sqr_Q2);
            M.Invert();
            //barycentric  coordinates, w0+w1+w2= 1
            var ResMatrix = M.MultiplyVertical(new Vector2(RdotQ1, RdotQ2));
            var w1 = ResMatrix.X;
            var w2 = ResMatrix.Y;
            var w0 = 1 - w1 - w2;
            if (w0 >= 0 && w1 >= 0 && w2 >= 0) return intersection;
            else return null;
        }
    }

    public class Ray {
        public readonly Vector3 Origin;
        public readonly Vector3 Direction;
        public readonly Vector3 EndPoint;

        public Ray(Vector3 origin,Vector3 end) {
            Origin = origin;
            EndPoint = end;
            Direction = (end-origin).Normalized();
        }

        public static float Width;
        public static float Height;

       // public Ray WithModelMatrix(Matrix4 m) => new Ray(Origin, EndPoint.Multiply(m.Inverted()));

        public static Ray FromMouse() => FromMouse(MainWindow.CurrentMousePos.toPoint(), Scene.Get.Camera, Matrix4.Identity);
        public static Ray FromMouse(Matrix4 ModelMatrix) => FromMouse(MainWindow.CurrentMousePos.toPoint(), Scene.Get.Camera, ModelMatrix);
        
        private static Ray FromMouse(Point mouse, Camera camera, Matrix4 ModelMatrix) {
            Vector3 RayEndPoint, RayOrigin;
            var x = (2f * mouse.X) / Width - 1f;
            var y = 1f - (2f * mouse.Y) / Height;
            var inNDC = new Vector3((float)x, (float)y, 1.0f);

            (RayOrigin, RayEndPoint) = MouseToWorldRay(camera.GetProjectionMatrix(), camera.GetViewMatrix(), ModelMatrix, inNDC);
            return new Ray(RayOrigin, RayEndPoint);
        }
        /*w = 1f => point
         w = 0 => direction
             */
        private static (Vector3, Vector3) MouseToWorldRay(
            Matrix4 projection,
            Matrix4 view,
            Matrix4 model,
            Vector3 inNDC) {
            Vector3 pos1 = UnProject(ref projection, view, model, new Vector4(inNDC.X, inNDC.Y, 0f, 1f)); // near
            Vector3 pos2 = UnProject(ref projection, view, model, new Vector4(inNDC.X, inNDC.Y, 1f, 1f));  // far
            return (pos1, pos2);
        }

        private static Vector3 UnProject(
            ref Matrix4 projection,
            Matrix4 view,
            Matrix4 model,
            Vector4 inNDC_and_w) {
            Vector4 vec = inNDC_and_w;

            Matrix4 viewInv = Matrix4.Invert(view);
            Matrix4 projInv = Matrix4.Invert(projection);
            Matrix4 modInv = Matrix4.Invert(model);

            Vector4.Transform(ref vec, ref projInv, out vec);
            Vector4.Transform(ref vec, ref viewInv, out vec);
            Vector4.Transform(ref vec, ref modInv, out vec);

            if (vec.W > float.Epsilon || vec.W < -float.Epsilon) {
                vec.X /= vec.W;
                vec.Y /= vec.W;
                vec.Z /= vec.W;
            }
            return new Vector3(vec.X, vec.Y, vec.Z);
        }



    }
}
