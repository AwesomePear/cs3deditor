using System;
using System.Windows;
using OpenTK.Graphics.OpenGL4;
using OpenTK.Graphics;
using OpenTK;
using Editor.Graphics.Cameras;
using System.Diagnostics;
using System.Collections.Generic;
using Editor.Graphic.RenderObject.Base;
using System.Windows.Input;
using Editor.Utilities;
using System.Windows.Controls;
using Editor.Graphic;
using System.Timers;
using Editor.Graphic.Window;
using CSUtils;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Threading;
using System.Linq;

namespace Editor.Graphics {
    public partial class MainWindow : Window {

        public static void FromGUIThread(FrameworkElement element, Action action) {
            element.Dispatcher.Invoke(DispatcherPriority.Normal, action);
        }
        public static void FromGUIThread(Action action) {
            Get.Dispatcher.Invoke(DispatcherPriority.Normal, action);
        }
        private static MainWindow o;
        public static MainWindow Get => o;

        public string ChatText {
            get { return (string)GetValue(ChatTextProperty); }
            set { SetValue(ChatTextProperty, value); }
        }
        public static readonly DependencyProperty ChatTextProperty =
            DependencyProperty.Register("ChatText", typeof(string), typeof(MainWindow), new PropertyMetadata(""));


        #region SelectionMode
        public enum SelectionModes : byte {
            Object, Edit
        }
        System.Windows.Media.Brush ObjectModeColor;
        System.Windows.Media.Brush EditModeColor;
        System.Windows.Media.Brush InactiveModeColor;
        System.Windows.Media.BrushConverter Converter = new System.Windows.Media.BrushConverter();
        private SelectionModes selectionMode = SelectionModes.Object;
        public SelectionModes SelectionMode {
            get => selectionMode;
            set {
                switch (value) {
                    case SelectionModes.Edit:
                        SelectionModeButton.Content = "Edit";
                        SelectionModeButton.Background = EditModeColor;
                        EditSelectionModePanel.Visibility = Visibility.Visible;
                        break;
                    case SelectionModes.Object:
                        SelectionModeButton.Content = "Object";
                        SelectionModeButton.Background = ObjectModeColor;
                        EditSelectionModePanel.Visibility = Visibility.Hidden;
                        break;
                }
                selectionMode = value;
                Scene.SelectionMode = value;
            }
        }
        SelectionModes GetNextSelection() {
            switch (selectionMode) {
                case SelectionModes.Edit:
                    return SelectionModes.Object;
                case SelectionModes.Object:
                    return SelectionModes.Edit;
            }
            return SelectionModes.Object;
        }
        #endregion

        public string ConnectionStatusColor {
            get { return (string)GetValue(ConnectionStatusColorProperty); }
            set { SetValue(ConnectionStatusColorProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ConnectionStatusColor.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ConnectionStatusColorProperty =
            DependencyProperty.Register("ConnectionStatusColor", typeof(string), typeof(MainWindow), new PropertyMetadata(Client.RED));

        //propdp

        



        private Scene Scene;
        Timer AutoRender;

        
        int FPS = 60;
        double Interval;
        Client Client;

        private float Aspect => (float)GLControl.Width / (float)GLControl.Height;

        public MainWindow() {
            o = this;
            Client = new Client(this);
            InitializeComponent();
            

        }


        private void Костыли() {
            Ray.Width = GLControl.Width;
            Ray.Height = GLControl.Height;
        }
        public float GLWidth => GLControl.Width;
        public float GLHeight => GLControl.Height;

        private void Load(object sender, EventArgs e) {

            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            Костыли();
            ObjectModeColor = (System.Windows.Media.Brush)Converter.ConvertFromString("#9fb1bf");
            EditModeColor = (System.Windows.Media.Brush)Converter.ConvertFromString("#ebc187");
            InactiveModeColor = (System.Windows.Media.Brush)Converter.ConvertFromString("#827D82");

            Scene = Scene.Get;
            UserAction.Context = Scene;
            Scene.Camera = new Camera((1.5f, 1f, 1.5f).v3(), Aspect);
            Scene.Camera.Pitch = -30;
            Scene.Camera.Yaw = -135;

            LoadGL();
            SceneInfo.DataContext = Scene.State;
            ChatMessages.DataContext = this;
            ConnectionStatus.DataContext = this;
            SelectedStatePanel.DataContext = Scene.SelectedVertexBinding;

            //Client.Connect();
        }

        
        //todo интерфейс
        //--показать нормали



        private void Paint(object sender, System.Windows.Forms.PaintEventArgs e) {
            ReadInputs();
            try {
                Scene.Render();
            }catch (Exception ex) {
                MessageBox.Show(ex + Environment.NewLine + ex.StackTrace);
            }
            /*
            
            //todo всё что нужно
            клиент
            МАТРИЦА МОДЕЛИ
            -- изменения матрицы (модели) трансформации: перемещение всего объекта, его поворот и масштабирование
            -- учет инвертированной матрицы модели в алгоритмах пересечения
            -- возможность применения матрицы модели к объекту

            ОБЪЕКТЫ
            -- объединение объектов
            -- копирование объектов
            -- режимы выбора объекта и редактирования 


            ОСТАЛЬНОЕ
            -- кисти
            -- интерфейс

            -- разбиение полигона: выбираем один или несколько, нажимаем разбить и выбираем на сколько полигонов

            -- невозможность выбора вершин, если они не видны
            -- frustum culling[?] и всякие оптимизации

            сервер
            -- чтение и сохранение в формат obj (?)
            -- интерфейс
             хранить на сервере 
             текстуры, форматы,
             и создание проекта

            - откаты по истории
            --- предсказывание/интерполяция 
            - подсчет количества точек, ребер и всего прочего *
            --- Рендер готового видео/картинки, на выход - числовой файл, камеру задает клиент
            - Хранение текстур
            - авторизация - по паролю *
            --- фича - поделить сцену, для каких-то обхектов вкл "не отображать"
            --- список юзеров, которым доступно редактирование

            

            */

            //check оптимизация
            //[MethodImpl(MethodImplOptions.AggressiveInlining)] - атрибут типа inline
            //Debug.WriteLine(GL.GetError());
            GLControl.SwapBuffers();
        }



        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            var LB = sender as ListBox;
            var selected = (LB.SelectedItem.ToString()).ToLower().Substring(37);
            switch (selected) {
                case "faces": {
                        Scene.DrawFaces = true;
                        Scene.WireframeMode = false;
                        break;
                    }
                case "edges": {
                        Scene.DrawEdges = true;
                        Scene.DrawFaces = false;
                        Scene.WireframeMode = false;
                        break;
                    }
                case "points": {
                        Scene.DrawEdges = false;
                        Scene.DrawFaces = false;
                        Scene.WireframeMode = false;
                        break;
                    }
                case "wireframe": {
                        Scene.DrawEdges = true;
                        Scene.DrawFaces = true;
                        Scene.WireframeMode = true;
                        break;
                    }
            }


        }

        private void Delete_Click(object sender, RoutedEventArgs e) {
            Scene.DeleteObject();
        }

        private void SphereCreate_Click(object sender, RoutedEventArgs e) {
            Scene.CreateObject(BasicRenderObject.Primitive.Sphere(new Vector3(0,0,0), 1, 12, 12));
        }

        private void SelectionModeButton_Click(object sender, RoutedEventArgs e) {
            SelectionMode = GetNextSelection();

        }

        private void CubeCreate_Click(object sender, RoutedEventArgs e) {
            Scene.CreateObject(BasicRenderObject.Primitive.Cube(1f));
        }

        private void SelectFaceMode_Click(object sender, RoutedEventArgs e) {
            Scene.EditSelectionMode = Scene.EditSelectionModes.Face;
            SelectFaceMode.Background = EditModeColor;
            SelectVertexMode.Background = InactiveModeColor;
        }
        private void SelectVertexMode_Click(object sender, RoutedEventArgs e) {
            Scene.EditSelectionMode = Scene.EditSelectionModes.Vertex;
            SelectVertexMode.Background = EditModeColor;
            SelectFaceMode.Background = InactiveModeColor;
        }

        private void Info_Click(object sender, RoutedEventArgs e) {
            new InfoWindow("Info","Управление",System.IO.File.ReadAllText(@"..\..\Data\Info.txt")).Show();
        }
        private void About_Click(object sender, RoutedEventArgs e) {
            new InfoWindow("About", "О программе", System.IO.File.ReadAllText(@"..\..\Data\About.txt")).Show();
        }
        private void Settings_Click(object sender, RoutedEventArgs e) {
            new SettingsWindow(Client).ShowDialog();
        }
        private void Connect_Click(object sender, RoutedEventArgs e) {
            Client.Connect();
        }
        private void Disconnect_Click(object sender, RoutedEventArgs e) {
            Client.Disconnect();
        }
        private void MainMode_Click(object sender, RoutedEventArgs e) {
            Scene.DrawMode = Scene.DrawModes.Main;
        }
        private void ColorMode_Click(object sender, RoutedEventArgs e) {
            Scene.DrawMode = Scene.DrawModes.Color;
        }
        private void TextureMode_Click(object sender, RoutedEventArgs e) {
            Scene.DrawMode = Scene.DrawModes.Texture;
        }
        private void Save_Click(object sender, RoutedEventArgs e) {
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = "Scene"; // Default file name
            dlg.DefaultExt = ".byak"; // Default file extension
            dlg.Filter = "Byak scene format (.byak)|*.byak"; // Filter files by extension

            bool? save = dlg.ShowDialog();

            if (save == true) 
                FileWork.SaveTo(dlg.FileName);
            
        }
        private void Open_Click(object sender, RoutedEventArgs e) {

            const string text = "Do you want to save changes?";
            const string caution = "Caution";
            const MessageBoxButton button = MessageBoxButton.YesNoCancel;
            const MessageBoxImage icon = MessageBoxImage.Warning;

            MessageBoxResult result = MessageBox.Show(text, caution, button, icon);

            switch (result) {
                case MessageBoxResult.Yes:
                    Save_Click(sender, e);
                    break;
                case MessageBoxResult.No:
                    Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
                    dlg.FileName = "Scene.byak"; // Default file name
                    dlg.DefaultExt = ".byak"; // Default file extension
                    dlg.Filter = "Byak scene format (.byak)|*.byak"; // Filter files by extension

                    bool? open = dlg.ShowDialog();

                    if (open == true) {
                        Scene.Clear();
                        Parse.File(dlg.FileName);
                    }
                    break;
                case MessageBoxResult.Cancel:return;
            }

        }

        private void VertexComponentChanged(object sender, TextChangedEventArgs e) {
            Scene.SelectedVertexBinding.Accept((TextBox)sender);
        }

        private void Window_Closing(object sender, CancelEventArgs e) {
            if (Client.Connected) Client.Disconnect();
        }

        private void CreateCone_Click(object sender, RoutedEventArgs e) {
            Scene.CreateObject(BasicRenderObject.Primitive.Cone(new Vector3(0, 0, 0), 0.5f, 12, 1));
        }

        private void Join_Click(object sender, RoutedEventArgs e) {
            Scene.Join();
        }

        private void Duplicate_Click(object sender, RoutedEventArgs e) {
            Scene.CreateObject(Scene.SelectedObject.Copy());
        }
    }
}












/*
              ____           ____         _____________    ____   ____    _____________       ____    _____    _____________    ____    ____
            /   /          /   /        /    ___     /   /   /  /   /   /            /      /   /   /    /   /    ___     /   /   /   /   /
           /   /          /   /        /    /  /    /   /   /  /   /   /    ________/      /   /   /    /   /    /  /    /   /   /   /   /
          /   /          /   /        /    /  /    /   /   /  /   /   /    /____          /   /___/    /   /    /  /    /   /   /   /   /
         /   /          /   /        /    /  /    /   /   /  /   /   /    _____/         /_______     /   /    /  /    /   /   /   /   /
        /   /          /   /        /    /  /    /   /   /.-^   /   /    /                      /    /   /    /  /    /   /   /   /   /
       /   /          /   /_____   /    /  /    /   /   *   _.-^   /    /________       _______/    /   /    /  /    /   /   |___/   /
      /   /          /         /  /    /__/    /   /    _.-^      /             /      /           /   /    /__/    /    |          /
     /___/          /_________/  /____________/   /__.-^         /_____________/      /___________/   /____________/      \________/

*/
