using System;
using System.Windows;
using OpenTK.Graphics.OpenGL4;
using OpenTK.Graphics;
using OpenTK;
using Editor.Graphics.Cameras;
using System.Diagnostics;
using System.Collections.Generic;
using Editor.Graphic.RenderObject.Base;
using System.Windows.Input;
using Editor.Utilities;
using System.Windows.Controls;
using Editor.Graphic;
using System.Timers;
using Editor.Graphic.Window;
using CSUtils;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace Editor.Graphics {
    public partial class InfoWindow : Window {


        public string InfoText {
            get { return (string)GetValue(InfoTextProperty); }
            set { SetValue(InfoTextProperty, value); }
        }
        public static readonly DependencyProperty InfoTextProperty =
            DependencyProperty.Register("InfoText", typeof(string), typeof(InfoWindow), new PropertyMetadata("Some info"));


        public string TitleText {
            get { return (string)GetValue(TitleTextProperty); }
            set { SetValue(TitleTextProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TitleText.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TitleTextProperty =
            DependencyProperty.Register("TitleText", typeof(string), typeof(InfoWindow), new PropertyMetadata("Information"));



        public string WindowTitle {
            get { return (string)GetValue(WindowTitleProperty); }
            set { SetValue(WindowTitleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for WindowTitle.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty WindowTitleProperty =
            DependencyProperty.Register("WindowTitle", typeof(string), typeof(InfoWindow), new PropertyMetadata("Info"));






        public InfoWindow(string wtitle = "Info", string title = "Information", string text = "Some info") {
            InitializeComponent();
            Grid.DataContext = this;
            InfoText = text;
            WindowTitle = wtitle;
            TitleText = title;
        }


    }
}