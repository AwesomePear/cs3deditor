using Editor.Graphic.RenderObject.Base;
using Editor.Graphics.Cameras;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK.Graphics.OpenGL4;
using OpenTK.Graphics;
using OpenTK;
using Editor.Graphics;
using Editor.Utilities;
using System.Diagnostics;
using System.Windows;
using CSUtils;
using static Editor.Graphics.MainWindow;

namespace Editor.Graphic.Window {
    public class Scene : IScene {
        private static Scene o = new Scene();
        public static bool Loaded = false;
        public static Scene Get => o;
        public Dictionary<decimal, IObject> Objects { get; } = new Dictionary<decimal, IObject>();

        public List<BasicRenderObject> _Objects = new List<BasicRenderObject>(); //те, что можно выделить и изменить, спарить с объектом для сервера
        public List<BasicRenderObject> TechObjects = new List<BasicRenderObject>(); //сетка,  оси
        public BasicRenderObject SelectedObject;
        public List<BasicRenderObject> SelectedObjects;
        public BasicRenderObject SelectionCenter;

        public SelectionVisualObject SelectionVisual;

        public Camera Camera;

        public bool DrawEdges = true;
        public bool DrawFaces = true;

        private ShaderProgram SimpleShader;
        private ShaderProgram MeshEditorShader;
        private ShaderProgram ObjectModeShader;

        private ShaderProgram ColorModeShader;
        private ShaderProgram TextureModeShader;

        public readonly VertexPropdp SelectedVertexBinding = new VertexPropdp();

        public bool WireframeMode;
        public SceneState State {get;}

        public SelectionModes SelectionMode = SelectionModes.Object;
        public bool IsObjectMode() => SelectionMode == SelectionModes.Object;
        public bool IsEditMode() => SelectionMode == SelectionModes.Edit;

        public enum EditSelectionModes: byte {
            Face, Vertex, Edge
        }

        public EditSelectionModes EditSelectionMode = EditSelectionModes.Vertex;

        public bool IsFaceMode() => EditSelectionMode == EditSelectionModes.Face;
        public bool IsVertexMode() => EditSelectionMode == EditSelectionModes.Vertex;

        public void Clear() {
            Objects.Clear();
            _Objects.Clear();
            SelectionVisual.Clear();
            SelectedObject = null;
        }

        #region DrawMode
        public enum DrawModes : byte {
            Main, Color, Texture
        }
        public DrawModes DrawMode = DrawModes.Main;


        #endregion




        private Scene() {
            
#if DEBUG
            DiagnosticTool.Start();
#endif

            try {
                SimpleShader = new ShaderProgram();
                SimpleShader.AddShader(ShaderType.VertexShader, @"..\..\Graphic\Shader\Simple.vs");
                SimpleShader.AddShader(ShaderType.FragmentShader, @"..\..\Graphic\Shader\Simple.fs");
                SimpleShader.Link();

                MeshEditorShader = new ShaderProgram();
                MeshEditorShader.AddShader(ShaderType.VertexShader, @"..\..\Graphic\Shader\StandardEditor.vs");
                MeshEditorShader.AddShader(ShaderType.FragmentShader, @"..\..\Graphic\Shader\Simple.fs");
                MeshEditorShader.Link();

                ObjectModeShader = new ShaderProgram();
                ObjectModeShader.AddShader(ShaderType.VertexShader, @"..\..\Graphic\Shader\ObjectMode.vs");
                ObjectModeShader.AddShader(ShaderType.FragmentShader, @"..\..\Graphic\Shader\ObjectMode.fs");
                ObjectModeShader.Link();

                ColorModeShader = new ShaderProgram();
                ColorModeShader.AddShader(ShaderType.VertexShader, @"..\..\Graphic\Shader\WithColor.vs");
                ColorModeShader.AddShader(ShaderType.FragmentShader, @"..\..\Graphic\Shader\WithColor.fs");
                ColorModeShader.Link();

                TextureModeShader = new ShaderProgram();
                TextureModeShader.AddShader(ShaderType.VertexShader, @"..\..\Graphic\Shader\WithTextures.vs");
                TextureModeShader.AddShader(ShaderType.FragmentShader, @"..\..\Graphic\Shader\WithTextures.fs");
                TextureModeShader.Link();


                SelectedObject = BasicRenderObject.Standard();

                CreateTechObjects();
                CreateObjects();

                State = new SceneState(this);
            }catch (Exception e) {
                Debug.WriteLine(e);
                MessageBox.Show(e.StackTrace);
            }
#if DEBUG
            DiagnosticTool.Stop();
#endif
            Loaded = true;
        }
        void CreateObjects() {
            var o =
            //Objects.Add(BasicRenderObject.Primitive.Cone((0, 0, 0), 1, 48, 2));
            //BasicRenderObject.Primitive.Sphere((0,0,0),1,24,24);
            BasicRenderObject.Primitive.Cube(1f);
            _Objects.Add(o);
            Objects.Add(o.ID,o);
            SelectedObject = _Objects[0];
            SelectedObjects = new List<BasicRenderObject>() { SelectedObject };
            //Objects.Add(BasicRenderObject.Primitive.Sphere((-2f,0,2f),0.7f,72,72));
            //o.Vertices[0].TextureCoordinates = (0f, 0f);
        }

        void CreateTechObjects() {
            var Axis = BasicRenderObject.Standard();
            Axis.Vertices.Add(new Vertex(new Coordinates(0, 0, 0), new Color(0, 0, 0, 1f)));
            Axis.Vertices.Add(new Vertex(new Coordinates(1, 0, 0), new Color(1f, 0, 0, 1f)));
            Axis.Vertices.Add(new Vertex(new Coordinates(0, 1, 0), new Color(0, 1f, 0, 1f)));
            Axis.Vertices.Add(new Vertex(new Coordinates(0, 0, 1), new Color(0, 0, 1f, 1f)));
            Axis.AddEdge(0, 1);
            Axis.AddEdge(0, 2);
            Axis.AddEdge(0, 3);

            TechObjects.Add(Axis);
            TechObjects.Add(BasicRenderObject.Primitive.Grid(10, 1));

            SelectionVisual = SelectionVisualObject.Standard();
            SelectionCenter = BasicRenderObject.Standard();
            SelectionCenter.Vertices.Add(new Vertex(new Coordinates(0, 0, 0), new Color(1f,1f,1f,1f)));


        }


        Texture Texture = new Texture("../../Resources/settings_icon_1.png");
        public void Render() {
            State.ReCount();//redo когда-нибудь переделать
            if (Camera == null) throw new FieldNotSetException("Камеру забыли");
            
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            Matrix4 View = Camera.GetViewMatrix();
            Matrix4 Projection = Camera.GetProjectionMatrix();


            GL.UseProgram(SimpleShader.Id);
            GL.UniformMatrix4(16, false, ref Projection);
            GL.UniformMatrix4(18, false, ref View);

            TechObjects.ForEach(x => x.Render(PrimitiveType.Lines));

            if (DrawFaces) {
                if (DrawMode == DrawModes.Color) {
                    GL.UseProgram(ColorModeShader.Id);
                    GL.UniformMatrix4(16, false, ref Projection);
                    GL.UniformMatrix4(18, false, ref View);
                    _Objects.ForEach(x => x.Render(PrimitiveType.Triangles));

                } else if (DrawMode == DrawModes.Texture) {
                    GL.UseProgram(TextureModeShader.Id);
                    GL.UniformMatrix4(16, false, ref Projection);
                    GL.UniformMatrix4(18, false, ref View);
                    Texture.Use();
                    _Objects.ForEach(x => x.Render(PrimitiveType.Triangles));
                }
            }


            GL.LineWidth(2f);
            GL.PointSize(5f);
            GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);


            GL.UseProgram(SimpleShader.Id);
            GL.UniformMatrix4(16, false, ref Projection);
            GL.UniformMatrix4(18, false, ref View);


            /*if (PickObjectPureRay != null) { 
                PickObjectPureRay.Render(PrimitiveType.Lines);
                PickObjectPureRay.Render(PrimitiveType.Points);
            }

            if (PickObjectModelMatrixRay != null) {
                PickObjectModelMatrixRay.Render(PrimitiveType.Lines);
                PickObjectModelMatrixRay.Render(PrimitiveType.Points);
            }*/


            if (IsEditMode()) {

                SelectionCenter.Vertices[0].Position = SelectionVisual.Vertices.CenterPoint().Coord();
                SelectionCenter.Render();


                GL.UseProgram(MeshEditorShader.Id);
                GL.UniformMatrix4(16, false, ref Projection);
                GL.UniformMatrix4(18, false, ref View);

                Color4 EdFaceClr = Configs.MeshEditorFaces.Color4();
                Color4 SelFaceClr = Configs.FaceSelection.Color4();
                if (WireframeMode) {
                    EdFaceClr.A *= Configs.WireframeModifier;
                    SelFaceClr.A *= Configs.WireframeModifier;
                }

                GL.Uniform4(19, EdFaceClr);
                GL.Uniform4(20, SelFaceClr);

                SelectionVisual.Render(PrimitiveType.Triangles);


                GL.Uniform4(19, Configs.MeshEditorEdges.Color4());
                GL.Uniform4(20, Configs.EdgesSelection.Color4());
                if (IsVertexMode())
                    //_Objects.ForEach(x => x.Render(PrimitiveType.Points));
                    SelectedObject?.Render(PrimitiveType.Points);
                if (DrawEdges) {
                    _Objects.ForEach(x => x.Render(PrimitiveType.Lines));
                }
                if (DrawFaces) {
                    GL.Uniform4(19, EdFaceClr);
                    GL.Uniform4(20, EdFaceClr);
                    _Objects.ForEach(x => x.Render(PrimitiveType.Triangles));
                }
            }
            else if (IsObjectMode()) {

                GL.UseProgram(ObjectModeShader.Id);
                GL.UniformMatrix4(16, false, ref Projection);
                GL.UniformMatrix4(18, false, ref View);

                Color4 SelectedFace = Configs.ObjectModeSelection.Color4();
                Color4 NonSelectedFace = Configs.MeshEditorFaces.Color4();
                Color4 SelectedEdge = Configs.ObjectModeSelectionVertexEdge.Color4();
                Color4 NonSelectedEdge = Configs.MeshEditorEdges.Color4();
                if (WireframeMode) {
                    SelectedFace.A *= Configs.WireframeModifier;
                    NonSelectedFace.A *= Configs.WireframeModifier;
                }

                
                GL.Uniform4(19, NonSelectedEdge);
                GL.Uniform4(20, SelectedEdge);
                foreach (var x in _Objects) {
                    if (SelectedObjects.Contains(x)) GL.Uniform1(21, 1);
                    else GL.Uniform1(21, 0);
                    x.Render(PrimitiveType.Lines);
                }
                GL.Uniform4(19, NonSelectedFace);
                GL.Uniform4(20, SelectedFace);
                foreach (var x in _Objects) {
                    if (SelectedObjects.Contains(x)) GL.Uniform1(21, 1);
                    else GL.Uniform1(21, 0);
                    x.Render(PrimitiveType.Triangles);
                }
            }
            GL.UseProgram(SimpleShader.Id);
            GL.UniformMatrix4(16, false, ref Projection);
            GL.UniformMatrix4(18, false, ref View);

            TechObjects.ForEach(x => x.Render(PrimitiveType.Lines));

        }

        public (IObject,IVertex) GetVertexByID(decimal id) {
            foreach(var o in Objects) {
                var search_result = o.Value.Vertices.Find(x => x.ID == id);
                if (search_result != null) {
                    Debug.WriteLine("Search for v" + id + " resulted it in o" + o.Value.ID);
                    return (o.Value, search_result.Value.Value);
                }
            }
            return (null,null);
        }

        public (IObject, IEdge) GetEdgeByID(decimal id) {
            foreach (var o in _Objects) {
                var search_result = o.Edges.Find(x => x.ID == id);
                if (search_result != null) return (o, search_result);
            }
            return (null, null);
        }

        public (IObject, IFace) GetFaceByID(decimal id) {
            foreach (var o in _Objects) {
                var search_result = o.Faces.Find(x => x.ID == id);
                if (search_result != null) return (o, search_result);
            }
            return (null, null);
        }

        public IObject GetObjectByID(decimal id) {
            var succ = Objects.TryGetValue(id, out IObject res);
            return res;
        }

        public void DeleteVertex(IObject o, IVertex v) {
            o.DeleteVertex(v.ID);
        }

        public void CreateVertex(IObject o, VertexInfo v) {
            o.CreateVertex(v);
        }

        public void CreateObject(decimal ID) {
            FromGUIThread(MainWindow.Get, () => {
                var o = BasicRenderObject.Standard(id: ID);
                _Objects.Add(o);
                Objects.Add(ID, o);
                //Client.Get.SendMessage(MSGUtils.SYS_tag, Pack.ObjectAction(new CreateObject(0, ID)));
            });
        }

        public void DeleteObject(decimal ID) {
            FromGUIThread(MainWindow.Get, () => {
                _Objects.Remove(x => x.ID == ID);
                Objects.Remove(ID);
                //Client.Get.SendMessage(MSGUtils.SYS_tag, Pack.ObjectAction(new DeleteObject(0, ID)));
            });
        }

        public void CreateObject(BasicRenderObject o) {
            FromGUIThread(MainWindow.Get, () => {
                _Objects.Add(o);
                Objects.Add(o.ID, o);
                if (Client.Get.Connected) Client.Get.SendMessage(MSGUtils.SYS_tag, Pack.FullObject(o));
            });
        }
        public void DeleteObject() {
            FromGUIThread(MainWindow.Get, () => {
                if (Client.Get.Connected)
                    foreach(var x in SelectedObjects)
                        Client.Get.SendMessage(MSGUtils.SYS_tag, Pack.ObjectAction(new DeleteObject(0,x.ID)));
                _Objects.RemoveRange(SelectedObjects);
                Objects.RemoveRange(SelectedObjects);
                SelectedObject = null;
            });
        }

        public void JoinObjects(decimal main, params decimal[] other) {
            Objects[main].AcceptModelMatrix();
            foreach(var x in other) {
                var o = Objects[x] as BasicRenderObject;
                o.AcceptModelMatrix();
                Objects[main].Join(o);
                if (SelectedObjects.Contains(o)) SelectedObjects.Remove(o);
                _Objects.Remove(o);
                Objects.Remove(x);
            }

        }

        public void Join() {
            FromGUIThread(MainWindow.Get, () => {
                var other = new List<decimal>();
                SelectedObject.AcceptModelMatrix();
                foreach (var x in SelectedObjects)
                    if (x != SelectedObject) {
                        other.Add(x.ID);
                        x.AcceptModelMatrix();
                        SelectedObject.Join(x);
                        _Objects.Remove(x);
                        Objects.Remove(x.ID);
                    }
                if(Client.Get.Connected)Client.Get.SendMessage(MSGUtils.SYS_tag, Pack.ObjectAction(new JoinObject(0, SelectedObject.ID, other.ToArray())));
                SelectedObjects = new List<BasicRenderObject> { SelectedObject };
            });
        }


        //BasicRenderObject PickObjectPureRay = null;
        //BasicRenderObject PickObjectModelMatrixRay = null;
        public BasicRenderObject PickObject() {
            var _ray = Ray.FromMouse();
            //PickObjectPureRay = BasicRenderObject.Primitive.Ray(Camera.Position, _ray.Direction +_ray.Origin);
            var best_distance = float.MaxValue; BasicRenderObject best_candidate = null;
            foreach (var x in _Objects) {
                var ray = Ray.FromMouse(x.ModelMatrix);
                //PickObjectModelMatrixRay = BasicRenderObject.Primitive.Ray(Camera.Position,ray.Direction + ray.Origin);
                foreach (var f in x.Faces) {
                    var intersection = Intersection.withTriangle(ray, f);
                    if (intersection == null) continue;
                    var distance = Vector3.Distance(f.CenterPoint, Camera.Position);
                    if (best_distance > distance) {
                        best_distance = distance;
                        best_candidate = x;
                    }
                }
            }
            return best_candidate;
        }

        public Face PickFace() {
            var ray = Ray.FromMouse(SelectedObject.ModelMatrix);
            //ray.Direction = SelectedObject.ModelMatrix * ray.Direction;
            var best_distance = float.MaxValue; Face best_candidate = null;
            for (int i = 0; i < SelectedObject.Faces.Count; i++) {
                var f = SelectedObject.Faces[i];
                var intersection = Intersection.withTriangle(ray, f);
                if (intersection == null) continue;
                var distance = Vector3.Distance(f.CenterPoint, Camera.Position);
                if (best_distance > distance) {
                    best_distance = distance;
                    best_candidate = f;
                }
            }
            return best_candidate;
        }
        public (Face Face, Vector3 Intersection) PickFaceAndIntersection() {
            var ray = Ray.FromMouse(SelectedObject.ModelMatrix);
            var best_distance = float.MaxValue; Face best_candidate = null;
            Vector3 best_intersection = new Vector3();
            for (int i = 0; i < SelectedObject.Faces.Count; i++) {
                var f = SelectedObject.Faces[i];
                var intersection = Intersection.withTriangle(ray, f);
                if (intersection == null) continue;
                var distance = Vector3.DistanceSquared(f.CenterPoint, Camera.Position);
                if (best_distance > distance) {
                    best_intersection = (Vector3)intersection;
                    best_distance = distance;
                    best_candidate = f;
                }
            }
            return (best_candidate, best_intersection);
        }
        public Vertex PickVertex() {
            var ray = Ray.FromMouse(SelectedObject.ModelMatrix);
            float bestd = -1f; Vertex bestv = null;
            for (int i = 0; i < SelectedObject.Vertices.Count; i++) {
                var v = SelectedObject.Vertices[i];
                var dist = Intersection.withSphere(ray, v.Position.ToVector3(), Configs.PickRadius);
                if (dist <= 0) continue;
                if (bestd > dist || bestd == -1) {
                    bestv = v;
                    bestd = dist;
                }
            }
            return bestv;
        }
        public override string ToString() {
            var s = "Scene " + Environment.NewLine;
            foreach (var x in Objects) {
                s += "Object " + x.Value.ID + Environment.NewLine;
                foreach (var v in x.Value.Vertices)
                    s += "    Vertex[" + v.Value.ID + "," + v.Value.X + "," + v.Value.Y + "," + v.Value.Z + ", ...]" + Environment.NewLine;
                foreach (var v in x.Value.Edges)
                    s += "    Edge[" + v.Value.ID + "," + v.Value.First.ID + "," + v.Value.Second.ID + "]" + Environment.NewLine;
                foreach (var v in x.Value.Faces)
                    s += "    Face[" + v.Value.ID + "," + v.Value.First.ID + "," + v.Value.Second.ID + "," + v.Value.Third.ID + "]" + Environment.NewLine;

            }
            return s;
        }
    }
}
