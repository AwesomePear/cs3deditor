using Editor.Graphic.RenderObject.Base;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Editor.Graphic.Window {
    public class VertexPropdp : DependencyObject {
        Vertex Vertex = null;


        public float X {
            get { return (float)GetValue(XProperty); }
            set {
                if (Vertex == null) return;
                Vertex.Position.X = value;
                SetValue(XProperty, value);
            }
        }
        public static readonly DependencyProperty XProperty =
            DependencyProperty.Register("X", typeof(float), typeof(VertexPropdp), new PropertyMetadata(0.0f));
        public float Y {
            get { return (float)GetValue(YProperty); }
            set {
                if (Vertex == null) return;
                Vertex.Position.Y = value;
                SetValue(YProperty, value);
            }
        }
        public static readonly DependencyProperty YProperty =
            DependencyProperty.Register("Y", typeof(float), typeof(VertexPropdp), new PropertyMetadata(0.0f));

        public float Z {
            get { return (float)GetValue(ZProperty); }
            set {
                if (Vertex == null) return;
                Vertex.Position.Z = value;
                SetValue(ZProperty, value);
            }
        }
        public static readonly DependencyProperty ZProperty =
            DependencyProperty.Register("Z", typeof(float), typeof(VertexPropdp), new PropertyMetadata(0.0f));

        public float S { //texture X analog
            get { return (float)GetValue(SProperty); }
            set {
                if (Vertex == null) return;
                Vertex.TextureCoordinates.X = value;
                SetValue(SProperty, value);
            }
        }
        public static readonly DependencyProperty SProperty =
            DependencyProperty.Register("S", typeof(float), typeof(VertexPropdp), new PropertyMetadata(0.0f));

        public float T { // texture Y analog
            get { return (float)GetValue(TProperty); }
            set {
                if (Vertex == null) return;
                Vertex.TextureCoordinates.Y = value;
                SetValue(TProperty, value);
            }
        }
        public static readonly DependencyProperty TProperty =
            DependencyProperty.Register("T", typeof(float), typeof(VertexPropdp), new PropertyMetadata(0.0f));

        public float R {
            get { return (float)GetValue(RProperty); }
            set {
                if (Vertex == null) return;
                Vertex.Color.R = value;
                SetValue(RProperty, value);
            }
        }
        public static readonly DependencyProperty RProperty =
            DependencyProperty.Register("R", typeof(float), typeof(VertexPropdp), new PropertyMetadata(0.0f));

        public float G {
            get { return (float)GetValue(GProperty); }
            set {
                if (Vertex == null) return;
                Vertex.Color.G = value;
                SetValue(GProperty, value);
            }
        }
        public static readonly DependencyProperty GProperty =
            DependencyProperty.Register("G", typeof(float), typeof(VertexPropdp), new PropertyMetadata(0.0f));
        public float B {
            get { return (float)GetValue(BProperty); }
            set {
                if (Vertex == null) return;
                Vertex.Color.B = value;
                SetValue(BProperty, value);
            }
        }
        public static readonly DependencyProperty BProperty =
            DependencyProperty.Register("B", typeof(float), typeof(VertexPropdp), new PropertyMetadata(0.0f));

        public float A {
            get { return (float)GetValue(AProperty); }
            set {
                if (Vertex == null) return;
                Vertex.Color.A = value;
                SetValue(AProperty, value);
            }
        }
        public static readonly DependencyProperty AProperty =
            DependencyProperty.Register("A", typeof(float), typeof(VertexPropdp), new PropertyMetadata(0.0f));


        public void Load(Vertex vertex) {
            Vertex = vertex;
            if (vertex == null) 
                (R, G, B, A, X, Y, Z, S, T) = (0, 0, 0, 0, 0, 0, 0, 0, 0);
            else {
                (R, G, B, A) = Vertex.Color;
                (X, Y, Z) = Vertex.Position;
                (S, T) = Vertex.TextureCoordinates;
            }
        }
        public void Accept(System.Windows.Controls.TextBox sender) {
            if ((object)Vertex == null || sender.Text.Length == 0) return;
            try {
                var what = sender.Name.ToLower().Last();
                var value = float.Parse(sender.Text);
                switch (what) {
                    case 'x':
                        Vertex.Position.X = value;
                        break;
                    case 'y':
                        Vertex.Position.Y = value;
                        break;
                    case 'z':
                        Vertex.Position.Z = value;
                        break;
                    case 'r':
                        Vertex.Color.R = value;
                        break;
                    case 'g':
                        Vertex.Color.G = value;
                        break;
                    case 'b':
                        Vertex.Color.B = value;
                        break;
                    case 'a':
                        Vertex.Color.A = value;
                        break;
                    case 's':
                        Vertex.TextureCoordinates.X = value;
                        break;
                    case 't':
                        Vertex.TextureCoordinates.Y = value;
                        break;
                }
                Update();
            } catch { }
        }
        public void Update() => Load(Vertex);
    }
}
