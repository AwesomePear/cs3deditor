using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK.Graphics.OpenGL4;
using OpenTK.Graphics;
using OpenTK;
using System.Timers;
using Editor.Graphic;
using Editor.Utilities;
using System.Threading;
using CSUtils;

namespace Editor.Graphics {
    public partial class MainWindow  {
        private void LoadGL() {
            GLControl.MakeCurrent();
            GLControl.VSync = true;
            GLControl.Resize += GLControl_Resize;
            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactor.SrcAlpha, BlendingFactor.OneMinusSrcAlpha);
            GL.ClearColor(Configs.Background.Color4());
            GL.Enable(EnableCap.DepthTest);
            GL.Enable(EnableCap.CullFace); //отсечение отвернутых полигонов


            Interval = 1000 / FPS;
            AutoRender = new System.Timers.Timer(Interval); // Timer, каждые Interval мс перерисовка
            AutoRender.AutoReset = true;
            AutoRender.Elapsed += (object o, ElapsedEventArgs args) => GLControl.Invalidate();
            AutoRender.Enabled = true;
            UserAction.Context = Scene;
        }



        private void GLControl_Resize(object sender, EventArgs e) {
            GL.Viewport(0, 0, GLControl.Width, GLControl.Height);
            Scene.Camera.AspectRatio = Aspect;
            Ray.Width = GLControl.Width;
            Ray.Height = GLControl.Height;
        }
    }
}
