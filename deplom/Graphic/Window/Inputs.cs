using CSUtils;
using Editor.Graphic.RenderObject.Base;
using Editor.Graphic.RenderObject.Editing;
using Editor.Utilities;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Editor.Graphics {
    public partial class MainWindow {

        public static Vector2 CurrentMousePos;
        private Vector2 MouseLastPosition;
        private float sensitivity = 0.2f;
        private float cameraSpeed = 0.005f;

        #region Мышь
        private void GLControl_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e) {

            CurrentMousePos = new Vector2(e.X, e.Y);
            if (e.Button == System.Windows.Forms.MouseButtons.Middle) {
                var deltaX = CurrentMousePos.X - MouseLastPosition.X;
                var deltaY = CurrentMousePos.Y - MouseLastPosition.Y;

                Scene.Camera.Yaw += deltaX * sensitivity;
                Scene.Camera.Pitch -= deltaY * sensitivity; // reversed since y-coordinates range from bottom to top
            } //вращение колесиком
            if (VertexTransform.Active) VertexTransform.Work();
            else if (ObjectTransform.Active) ObjectTransform.Work();

            MouseLastPosition = new Vector2(CurrentMousePos.X, CurrentMousePos.Y);
        }



        private void onMouseUp(object sender, System.Windows.Forms.MouseEventArgs e) {
            switch (e.Button) {
                case System.Windows.Forms.MouseButtons.Left: {
                        if (Scene.IsEditMode()) {
                            if (Scene.SelectedObject == null) return;
                            if (VertexTransform.Active) {
                                VertexTransform.Accept();
                                break;
                            }
                            if (Scene.SelectionVisual.Vertices.Count >= 1 && !Keyboard.IsKeyDown(Key.LeftShift)) {
                                Scene.SelectionVisual.Clear();
                            }
                            if (Scene.IsFaceMode()) {
                                Face f = Scene.PickFace();
                                if (f != null) {
                                    Scene.SelectionVisual.Vertices.AddUnique(f.Vertices.First);
                                    Scene.SelectionVisual.Vertices.AddUnique(f.Vertices.Second);
                                    Scene.SelectionVisual.Vertices.AddUnique(f.Vertices.Third);
                                    //if (!Scene.SelectionVisual.Vertices.Contains(f.Vertices.First)) Scene.SelectionVisual.Vertices.Add(f.Vertices.First);
                                    //if (!Scene.SelectionVisual.Vertices.Contains(f.Vertices.Second)) Scene.SelectionVisual.Vertices.Add(f.Vertices.Second);
                                    //if (!Scene.SelectionVisual.Vertices.Contains(f.Vertices.Third)) Scene.SelectionVisual.Vertices.Add(f.Vertices.Third);
                                    /*if (!Scene.SelectionVisual.Faces.Contains(f)) {
                                        Scene.SelectionVisual.Faces.Add(f);
                                    } else Scene.SelectionVisual.Faces.Remove(f);*/
                                }
                            } else if (Scene.IsVertexMode()) {
                                Vertex v = Scene.PickVertex();
                                if ((object)v != null) {
                                    if (!Scene.SelectionVisual.Vertices.Contains(v)) {
                                        Scene.SelectionVisual.Vertices.Add(v);
                                        Scene.SelectedVertexBinding.Load(v);
                                    } else
                                        Scene.SelectionVisual.Vertices.Remove(v);

                                }
                            }
                        } else if (Scene.IsObjectMode()) {
                            if (Scene._Objects.Count == 0) return;
                            if (ObjectTransform.Active) {
                                ObjectTransform.Accept();
                                break;
                            }
                            if (Scene.SelectedObjects.Count > 0 && !Keyboard.IsKeyDown(Key.LeftShift)) {
                                Scene.SelectedObject = null;
                                Scene.SelectedObjects.Clear();
                            }
                            var o = Scene.PickObject();
                            if (o != null) {
                                if (Scene.SelectedObject == o && Scene.SelectedObjects.Count>0 && Keyboard.IsKeyDown(Key.LeftShift)) {//если клик по тому же снова
                                    Scene.SelectedObjects.Remove(o);
                                    Scene.SelectedObject = Scene.SelectedObjects.Last();
                                } else {
                                    Scene.SelectedObject = o;
                                    Scene.SelectionVisual.Clear();
                                    Scene.SelectionVisual.ModelMatrix = o.ModelMatrix;
                                    Scene.SelectionCenter.ModelMatrix = o.ModelMatrix;
                                }
                                if (!Scene.SelectedObjects.Contains(o)) Scene.SelectedObjects.Add(o);
                            }
                        }
                        break;
                    }
                case System.Windows.Forms.MouseButtons.Right: {
                        //check не забыть, тут биение полигона на тройку
                        /*Face face;
                        Vector3 inter;
                        (face, inter) = PickFaceAndIntersection();
                        if (face != null) {
                            var vertex = new Vertex(inter.Coord(), Color.GenRandom);
                            Scene.SelectedObject.Vertices.Add(vertex);
                            Scene.SelectedObject.AddFace(face.Vertices.First, face.Vertices.Second, vertex);
                            Scene.SelectedObject.AddFace(face.Vertices.First, vertex, face.Vertices.Third);
                            Scene.SelectedObject.AddFace(face.Vertices.Second, face.Vertices.Third, vertex);
                            face.ClearLinks();
                            Scene.SelectedObject.Faces.Remove(face);
                        }*/
                        break;
                    }
                default: return;
            }
        }

        private void GLControl_MouseWheel(object sender, System.Windows.Forms.MouseEventArgs e) {
            Scene.Camera.Position += Scene.Camera.Front * e.Delta * sensitivity * 0.01f;
        }
        #endregion

        #region Клавиатура
        public void ReadInputs() {

            if (!GLControl.Focused) return;

            if (Keyboard.IsKeyDown(Key.W/*Up   */)) Scene.Camera.Position += Scene.Camera.Front * cameraSpeed * (float)Interval; // Forward
            if (Keyboard.IsKeyDown(Key.S/*Down */)) Scene.Camera.Position -= Scene.Camera.Front * cameraSpeed * (float)Interval;// Backwards
            if (Keyboard.IsKeyDown(Key.A/*Left */)) Scene.Camera.Position -= Scene.Camera.Right * cameraSpeed * (float)Interval;// Left
            if (Keyboard.IsKeyDown(Key.D/*Right*/)) Scene.Camera.Position += Scene.Camera.Right * cameraSpeed * (float)Interval;// Right
            if (Keyboard.IsKeyDown(Key.Space)) Scene.Camera.Position += Scene.Camera.Up * cameraSpeed * (float)Interval;// Up
            if (Keyboard.IsKeyDown(Key.LeftCtrl)) Scene.Camera.Position -= Scene.Camera.Up * cameraSpeed * (float)Interval;// Down
            if (Keyboard.IsKeyDown(Key.G)) {
                if (Scene.IsEditMode()) {
                    if (!VertexTransform.Active && Scene.SelectionVisual.Vertices.Count>0)
                        VertexTransform.Start(VertexTransform.Type.Translation, Scene.SelectionVisual.Vertices);
                } else if (Scene.IsObjectMode()) {
                    if (!ObjectTransform.Active)
                        ObjectTransform.Start(ObjectTransform.Type.Translation, Scene.SelectedObjects);
                }
            } 
            if (Keyboard.IsKeyDown(Key.M)) {
                if (Scene.IsEditMode()) {
                    if (!VertexTransform.Active && Scene.SelectionVisual.Vertices.Count > 0)
                        VertexTransform.Start(VertexTransform.Type.Scaling, Scene.SelectionVisual.Vertices);
                } else if (Scene.IsObjectMode()) {
                    if (!ObjectTransform.Active)
                        ObjectTransform.Start(ObjectTransform.Type.Scaling, Scene.SelectedObjects);
                }
            }
            if (Keyboard.IsKeyDown(Key.R)) {
                if (Scene.IsEditMode()) {
                    if (!VertexTransform.Active && Scene.SelectionVisual.Vertices.Count > 0)
                        VertexTransform.Start(VertexTransform.Type.Rotation, Scene.SelectionVisual.Vertices);
                } else if (Scene.IsObjectMode()) {
                    if (!ObjectTransform.Active)
                        ObjectTransform.Start(ObjectTransform.Type.Rotation, Scene.SelectedObjects);
                }
            }

            if (Keyboard.IsKeyDown(Key.O)) {
                if (Scene.IsEditMode()) {
                    if (Scene.SelectionVisual.Vertices.Count > 0)
                        Scene.SelectionVisual.Clear();
                    else
                        Scene.SelectionVisual.SelectAll();
                } else if (Scene.IsObjectMode()) {
                    if (Scene.SelectedObjects.Count > 0) {
                        Scene.SelectedObjects.Clear();
                        Scene.SelectedObject = null;
                    } else {
                        Scene.SelectedObjects.AddRange(Scene._Objects);
                        Scene.SelectedObject = Scene.SelectedObjects.Last();
                    }
                }
            }
            if (Keyboard.IsKeyDown(Key.Escape)) {
                if (VertexTransform.Active) {
                    VertexTransform.Break();
                } else if (ObjectTransform.Active) {
                    ObjectTransform.Break();
                }



            }
        }

        private void ChatNewMessage_KeyDown(object sender, KeyEventArgs e) {
            if (Keyboard.IsKeyDown(Key.Enter)) {
                if (Keyboard.IsKeyDown(Key.LeftShift))
                    ChatNewMessage.Text += Environment.NewLine;
                else Client.SendMSG();
            }
        }
        #endregion
    }
}
