using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Editor.Graphic.Window {
    public class SceneState :DependencyObject {
        Scene Scene;
        public decimal VerticesCount {
            get => (decimal)GetValue(VerticesCountProperty);
            set => SetValue(VerticesCountProperty, value);
        }
        public static readonly DependencyProperty VerticesCountProperty =
            DependencyProperty.Register("VerticesCount", typeof(decimal), typeof(SceneState), new PropertyMetadata(0M));
        public decimal EdgesCount {
            get => (decimal)GetValue(EdgesCountProperty);
            set => SetValue(EdgesCountProperty, value);
        }
        public static readonly DependencyProperty EdgesCountProperty =
            DependencyProperty.Register("EdgesCount", typeof(decimal), typeof(SceneState), new PropertyMetadata(0M));
        public decimal FacesCount {
            get => (decimal)GetValue(FacesCountProperty);
            set => SetValue(FacesCountProperty, value);
        }
        public static readonly DependencyProperty FacesCountProperty =
            DependencyProperty.Register("FacesCount", typeof(decimal), typeof(SceneState), new PropertyMetadata(0M));
        public decimal ObjectsCount {
            get => (decimal)GetValue(ObjectsCountProperty);
            set => SetValue(ObjectsCountProperty, value);
        }
        public static readonly DependencyProperty ObjectsCountProperty =
            DependencyProperty.Register("ObjectsCount", typeof(decimal), typeof(SceneState), new PropertyMetadata(0M));
        
        public void ReCount() {
            decimal v = 0M, e = 0M, f = 0M;
            foreach(var x in Scene._Objects) {
                v += x.Vertices.Count;
                e += x.Edges.Count;
                f += x.Faces.Count;
            }
            VerticesCount = v;
            EdgesCount = e;
            FacesCount = f;
            ObjectsCount = Scene._Objects.Count;
        }

        public SceneState(Scene s) {
            Scene = s;
            ReCount();
        }

    }
}
