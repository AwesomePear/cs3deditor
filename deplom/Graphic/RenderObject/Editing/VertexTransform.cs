using CSUtils;
using Editor.Graphic.RenderObject.Base;
using Editor.Utilities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Editor.Graphic.RenderObject.Editing {
    public static partial class VertexTransform {
        public enum Type {
            Translation, Scaling, Rotation
        }
        static Type CurrentType;

        public static bool Active { get; private set; }
        static List<Vertex> Selected;
        static List<VertexState> StartStates;
        static Ray First;
        static Ray Second;
        static readonly int works_to_send = 5;
        static int works_now = 0;
        static List<VertexState> TemporaryStates;
        static List<VertexAction> GetActions() {
            var res = new List<VertexAction>();
            for (int i = 0; i < Selected.Count; i++)
                res.Add(new ChangeVertex(0, (Selected[i].GetState() - TemporaryStates[i]).ToInfo()));
            return res;
        }
        static List<VertexAction> GetRestores() {
            var res = new List<VertexAction>();
            for (int i = 0; i < Selected.Count; i++)
                res.Add(new ChangeVertex(0, (TemporaryStates[i] - StartStates[i]).ToInfo()));
            return res;
        }

        public static void Start(Type type, List<Vertex> SelectedVertices) {
            Selected = SelectedVertices;
            StartStates = new List<VertexState>();
            TemporaryStates = StartStates;
            for (int i = 0; i < Selected.Count; i++) StartStates.Add(Selected[i].GetState());
            Active = true;
            First = Ray.FromMouse();
            CurrentType = type;
            switch (CurrentType) {
                case Type.Translation: Translation.Start(); break;
                case Type.Scaling: Scale.Start(); break;
                case Type.Rotation: Rotate.Start(); break;
            }
        }

        public static void Work() {
            bool send = false;
            if (++works_now == works_to_send) {
                send = true;
                works_now = 0;
                Window.Scene.Get.SelectedVertexBinding.Update();
            }
            switch (CurrentType) {
                case Type.Translation: Translation.Work(); break;
                case Type.Scaling: Scale.Work(); break;
                case Type.Rotation: Rotate.Work(); break;
            }
            if (send) {
                if (Client.Get.Connected) 
                    Client.Get.SendVertexActions(GetActions());
                TemporaryStates = Selected.GetStates(); 

            }
        }
        public static void Accept() {
            if (Client.Get.Connected) {
                Client.Get.SendVertexActions(GetActions(),true);
            }
            Window.Scene.Get.SelectedVertexBinding.Update();
            Active = false;
        }

        public static void Break() {
            for (int i = 0; i < StartStates.Count; i++) Selected[i].RestoreState(StartStates[i]);
            if (Client.Get.Connected) {
                Client.Get.SendVertexActions(GetRestores(),true);
            }
            Window.Scene.Get.SelectedVertexBinding.Update();
            Active = false;
        }

    }
}
