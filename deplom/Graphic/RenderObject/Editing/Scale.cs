using Editor.Graphic.RenderObject.Base;
using Editor.Utilities;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Editor.Graphic.RenderObject.Editing {

    public static partial class VertexTransform {

        static class Scale {
            static List<Vector3> ScalingVectors;
            static Vector3 CenterPoint;
            static float FirstDistance;

            public static void Start() {
                CenterPoint = Selected.CenterPoint();
                FirstDistance = Vector3.Distance(CenterPoint, First.Direction);
                ScalingVectors = new List<Vector3>();
                foreach (var x in Selected) ScalingVectors.Add(x.Position.ToVector3() - CenterPoint);
            }

            public static void Work() {
                Second = Ray.FromMouse();
                var Distance = Vector3.Distance(CenterPoint, Second.Direction);
                var Coeff = (FirstDistance - Distance) * 10 / FirstDistance;
                for (int i = 0; i < Selected.Count; i++)
                    Selected[i].Position.Set(StartStates[i].Position + ScalingVectors[i] * Coeff * (1 + Configs.MouseSensitivity));
            }

        }
    }
}
