using Editor.Graphic.RenderObject.Base;
using Editor.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Editor.Graphic.RenderObject.Editing {
    public static partial class VertexTransform {
        static class Translation {

            public static void Start() { }

            public static void Work() {
                Second = Ray.FromMouse();
                var vec = Second.Direction - First.Direction;
                foreach (var v in Selected) v.Position.Move(vec * (1 + Configs.MouseSensitivity));
                First = Second;
            }


        }
    }
}
