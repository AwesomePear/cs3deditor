using Editor.Graphic.RenderObject.Base;
using Editor.Graphic.Window;
using Editor.Utilities;
using OpenTK;
using OpenTK.Graphics.OpenGL4;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Editor.Graphic.RenderObject.Editing {
    public static partial class VertexTransform {
        static class Rotate {
            static List<float> Radiuses;
            static Vector3 CenterPoint;
            static float DistanceFromCamera;
            static Vector3 FirstPoint;
            static Vector3 VectorToFirst;
            public static void Start() {
                CenterPoint = Selected.CenterPoint();
                Radiuses = new List<float>();
                foreach(var x in Selected)
                    Radiuses.Add(Vector3.Distance(x.Position.ToVector3(), CenterPoint));
                DistanceFromCamera = Vector3.Distance(CenterPoint, Scene.Get.Camera.Position);
                FirstPoint = First.Direction * DistanceFromCamera;
                VectorToFirst = FirstPoint - CenterPoint;

            }

            public static void Work() {
                Second = Ray.FromMouse();
                var SecondPoint = Second.Direction * DistanceFromCamera;
                var VectorToSecond = SecondPoint - CenterPoint;
                var angle = Vector3.CalculateAngle(VectorToFirst,VectorToSecond);
                Debug.WriteLine(angle);
                var rotation = Matrix4.CreateFromAxisAngle(Scene.Get.Camera.Front,angle);
                for (int i = 0; i<Selected.Count; i++) {
                    Selected[i].Position.Set((rotation * new Vector4(StartStates[i].Position,1)).Xyz);
                }

            }


        }
    }
}
