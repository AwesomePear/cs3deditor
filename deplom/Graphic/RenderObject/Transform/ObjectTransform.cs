using CSUtils;
using Editor.Graphic.RenderObject.Base;
using Editor.Graphic.Window;
using Editor.Utilities;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Editor.Graphic.RenderObject.Editing {
    public static partial class ObjectTransform {
        public enum Type {
            Translation, Scaling, Rotation
        }
        static Type CurrentType;

        public static bool Active { get; private set; }
        static List<BasicRenderObject> Selected;
        static List<Matrix4> StartStates;
        static Ray First;
        static Ray Second;
        static readonly int works_to_send = 5;
        static int works_now = 0;
        static List<Matrix4> TemporaryStates;
        static List<ChangeModelMatrix> GetActions() {
            var res = new List<ChangeModelMatrix>();
            for (int i = 0; i < Selected.Count; i++)
                res.Add(new ChangeModelMatrix(0, Selected[i].ID, Selected[i].ModelMatrix - TemporaryStates[i]));
            return res;
        }
        static List<ChangeModelMatrix> GetRestores() {
            var res = new List<ChangeModelMatrix>();
            for (int i = 0; i < Selected.Count; i++)
                res.Add(new ChangeModelMatrix(0, Selected[i].ID, TemporaryStates[i] - StartStates[i]));
            return res;
        }

        public static void Start(Type type, List<BasicRenderObject> SelectedObjects) {
            Selected = SelectedObjects;
            StartStates = new List<Matrix4>();
            TemporaryStates = StartStates;
            for (int i = 0; i < Selected.Count; i++) StartStates.Add(Selected[i].ModelMatrix);
            Active = true;
            First = Ray.FromMouse();
            CurrentType = type;
            switch (CurrentType) {
                case Type.Translation: Translation.Start(); break;
                case Type.Scaling: Scale.Start(); break;
                case Type.Rotation: Rotate.Start(); break;
            }
        }

        public static void Work() {
            bool send = false;
            if (++works_now == works_to_send) {
                send = true;
                works_now = 0;
            }
            switch (CurrentType) {
                case Type.Translation: Translation.Work(); break;
                case Type.Scaling: Scale.Work(); break;
                case Type.Rotation: Rotate.Work(); break;
            }
            if (send) {
                if (Client.Get.Connected) 
                    Client.Get.SendModelMatrixActions(GetActions());
                TemporaryStates = Selected.GetModelMatrices(); 

            }
        }
        public static void Accept() {
            if (Client.Get.Connected)
                Client.Get.SendModelMatrixActions(GetActions());
            Scene.Get.SelectionVisual.ModelMatrix  = Scene.Get.SelectionCenter.ModelMatrix = Scene.Get.SelectedObject.ModelMatrix;
            Active = false;
        }

        public static void Break() {
            for (int i = 0; i < StartStates.Count; i++) Selected[i].ModelMatrix = StartStates[i];
            if (Client.Get.Connected) {
                Client.Get.SendModelMatrixActions(GetRestores());
            }
            Active = false;
        }

    }
}
