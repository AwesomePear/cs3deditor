using Editor.Graphic.RenderObject.Base;
using Editor.Graphic.Window;
using Editor.Utilities;
using OpenTK;
using OpenTK.Graphics.OpenGL4;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Editor.Graphic.RenderObject.Editing {
    public static partial class ObjectTransform {
        static class Rotate {
            static List<float> Radiuses;
            static Vector3 CenterPoint;
            static float DistanceFromCamera;
            static Vector3 FirstPoint;
            static Vector3 VectorToFirst;
            public static void Start() {
                CenterPoint = Selected.CenterPoint();
                FirstPoint = First.Direction * DistanceFromCamera;
                VectorToFirst = FirstPoint - CenterPoint;

            }

            public static void Work() {
                Second = Ray.FromMouse();
                var SecondPoint = Second.Direction * DistanceFromCamera;
                var VectorToSecond = SecondPoint - CenterPoint;
                var angle = Vector3.CalculateAngle(VectorToFirst,VectorToSecond);
                Debug.WriteLine(angle);
                var rotation = Matrix4.CreateFromAxisAngle(Scene.Get.Camera.Front,angle) * Configs.MouseSensitivity;
                foreach(var x in Selected) {
                    x.ModelMatrix = x.ModelMatrix * rotation;
                }

            }


        }
    }
}
