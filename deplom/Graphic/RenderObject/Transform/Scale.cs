using Editor.Graphic.RenderObject.Base;
using Editor.Utilities;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Editor.Graphic.RenderObject.Editing {

    public static partial class ObjectTransform {

        static class Scale {
            static Vector3 CenterPoint;
            static float FirstDistance;

            public static void Start() {
                CenterPoint = Selected.CenterPoint();
                FirstDistance = Vector3.Distance(CenterPoint, First.Direction);
            }

            public static void Work() {
                Second = Ray.FromMouse();
                var Distance = Vector3.Distance(CenterPoint, Second.Direction);
                var Coeff = (FirstDistance - Distance) * 10 / FirstDistance;
                var Mat = Matrix4.CreateScale(Coeff * (1 + Configs.MouseSensitivity));
                foreach (var x in Selected)
                   x.ModelMatrix = x.ModelMatrix * Mat;
            }

        }
    }
}
