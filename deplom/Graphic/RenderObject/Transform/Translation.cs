using Editor.Graphic.RenderObject.Base;
using Editor.Utilities;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Editor.Graphic.RenderObject.Editing {
    public static partial class ObjectTransform {
        static class Translation {

            public static void Start() { }

            public static void Work() {
                Second = Ray.FromMouse();
                var vec = Second.Direction - First.Direction;
                var mat = Matrix4.CreateTranslation(vec * (1 + Configs.MouseSensitivity));
                foreach (var v in Selected) v.ModelMatrix = v.ModelMatrix * mat;
                First = Second;
            }


        }
    }
}
