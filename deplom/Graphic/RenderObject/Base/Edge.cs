using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSUtils;

namespace Editor.Graphic.RenderObject.Base {
    public class Edge :IEdge {
        public decimal ID { get; }
        private static decimal id_counter = 0M;
        public override bool Equals(object bro) {
            if (bro == null) return false;
            return ((Edge)bro).Contains(First,Second);
        }
        public static bool operator ==(Edge e1, Edge e2) => e1.Equals(e2);
        public static bool operator !=(Edge e1, Edge e2) => !(e1 == e2);

        public (Vertex First, Vertex Second) Vertices { get;}

        public IVertex First => Vertices.First;

        public IVertex Second => Vertices.Second;


        private bool Contains(IVertex v) => Contains(v.ID);
        private bool Contains(decimal v_id) => Vertices.First.ID == v_id || Vertices.Second.ID == v_id;

        public bool Contains(params IVertex[] v) {
            foreach (var x in v)
                if (!Contains(x.ID)) return false;
            return true;
        }
        public bool Contains(params decimal[] v) {
            foreach (var x in v)
                if (!Contains(x)) return false;
            return true;
        }

        public override int GetHashCode() {
            var hashCode = 2137505268;
            hashCode = hashCode * -1521134295 + ID.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<IVertex>.Default.GetHashCode(First);
            hashCode = hashCode * -1521134295 + EqualityComparer<IVertex>.Default.GetHashCode(Second);
            return hashCode;
        }

        public Edge(Vertex First, Vertex Second, decimal? id = null) {
            if (id.HasValue) {
                ID = (decimal)id;
                if (ID > id_counter) id_counter = ID + 1;
            } else {
                if (Client.Get.Connected)
                    ID = Client.Get.IDPool.GetNext(ClientIDPool.Type.Edge);
                else
                    ID = id_counter++;
            }
            Vertices = (First, Second);
            First.makes_up_edges.Add(this);
            Second.makes_up_edges.Add(this);
        }

    }


}
