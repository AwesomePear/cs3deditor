using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK.Graphics.OpenGL4;
using OpenTK.Graphics;
using System.Diagnostics;
using OpenTK;
using CSUtils;

namespace Editor.Graphic.RenderObject.Base {
    public class WTF : Exception {
        public WTF(string msg = "?!") :base("[WTF] "+msg){}
    }


    public delegate void CoordinateChangedEventHandler(Coordinates sender/*, (Coordinate, float) difference*/);
    public class Coordinates {
        public static implicit operator Coordinates((float x, float y, float z) c) => new Coordinates(c.x, c.y, c.z);
        public static implicit operator Coordinates(Vector3 t) => new Coordinates(t.X, t.Y, t.Z);
        public static implicit operator (float X, float Y, float Z) (Coordinates c) => (c.X, c.Y,c.Z);
        public static implicit operator Vector3(Coordinates c) => new Vector3(c.X, c.Y,c.Z);
        public void Deconstruct(out float X, out float Y, out float Z) {
            X = this.X;
            Y = this.Y;
            Z = this.Z;
        }

        private float x;
        private float y;
        private float z;
        public Coordinates(float x, float y, float z) {
            this.x = x;
            this.y = y;
            this.z = z;
            Changes = null;
        }

        public void Move(Vector3 vec) {
            x += vec.X;
            y += vec.Y;
            z += vec.Z;
            Changes?.Invoke(this/*, (Coordinate.All, 0)*/);
            //check отчасти оптимизировано, но возможно, можно лучше
        }
        public void Move(float x, float y, float z) {
            this.x += x;
            this.y += y;
            this.z += z;
            Changes?.Invoke(this/*, (Coordinate.All, 0)*/);
            //check отчасти оптимизировано, но возможно, можно лучше
        }
        public void Set(Vector3 vec) {
            x = vec.X;
            y = vec.Y;
            z = vec.Z;
            Changes?.Invoke(this/*, (Coordinate.All, 0)*/);
        }
        public void Set(float x, float y, float z) {
            this.x = x;
            this.y = y;
            this.z = z;
            Changes?.Invoke(this/*, (Coordinate.All, 0)*/);
        }

        public Coordinates Copy() {
            return new Coordinates(X, Y, Z);
        }
        static public Coordinates Zero { get => new Coordinates(0, 0, 0); }
        

        public float[] GetData() => new float[] { x, y, z };

        public event CoordinateChangedEventHandler Changes;
        public float X { get => x;
            set {
                Changes?.Invoke(this/*, (Coordinate.X, value - x)*/);
                x = value;
            }
        }
        public float Y { get => y;
            set {
                Changes?.Invoke(this/*, (Coordinate.Y, value - y)*/);
                y = value;
            }
        }
        public float Z { get => z;
            set {
                Changes?.Invoke(this/*, (Coordinate.Z, value - z)*/);
                z = value;
            }
        }
        /*
        public List<(Coordinate, float)> DifferenceTo(Coordinates bro) {
            List<(Coordinate, float)> res = new List<(Coordinate, float)>();
            var t = this - bro;
            if (t.X != 0f) res.Add((Coordinate.X, t.X));
            if (t.Y != 0f) res.Add((Coordinate.Y, t.Y));
            if (t.Z != 0f) res.Add((Coordinate.Z, t.Z));
            return res;
        }*/
        public void Add(Coordinates bro) {
            x += bro.X;
            y += bro.Y;
            z += bro.Z;
            Changes?.Invoke(this);
        }
        static public Coordinates operator +(Coordinates a, Coordinates b) {
            var res = a.Copy();
            res.Add(b);
            return res;
        }

        public void Subtract(Coordinates bro) {
            x -= bro.X;
            y -= bro.Y;
            z -= bro.Z;
            Changes?.Invoke(this);
        }

        //todo вынести все подобные приведения в методы расширения и назвать схожим образом
        public Vector3 ToVector3() => new Vector3(x,y,z);

        static public Coordinates operator -(Coordinates a, Coordinates b) {
            var res = a.Copy();
            res.Subtract(b);
            return res;
        }

        public override string ToString() {
            return X + ", " + Y + ", " + Z;
        }
    }

    public delegate void TextureCoordinateChangedEventHandler(TextureCoordinates sender/*, (Coordinate, float) difference*/);
    public class TextureCoordinates:ITextureCoordinates {
        public static implicit operator TextureCoordinates((float x, float y) c) => new TextureCoordinates(c.x, c.y);
        public static implicit operator TextureCoordinates(Vector2 t) => new TextureCoordinates(t.X, t.Y);
        public static implicit operator (float S, float T) (TextureCoordinates c) => (c.X, c.Y);
        public static implicit operator Vector2(TextureCoordinates c) => new Vector2(c.X, c.Y);
        public void Deconstruct(out float S, out float T) {
            S = X;
            T = Y;
        }

        private float x;
        private float y;
        public TextureCoordinates(float x, float y) {
            this.x = x;
            this.y = y;
            Changes = null;
        }
        public ITextureCoordinates Copy() => new TextureCoordinates(X, Y);
        public ITextureCoordinates Inverted() => new TextureCoordinates(-X, -Y);
        static public TextureCoordinates Zero { get => new TextureCoordinates(0, 0); }

        public float[] GetData() => new float[] { x, y };

        public event TextureCoordinateChangedEventHandler Changes;
        public float X {
            get => x;
            set {
                Changes?.Invoke(this/*, (Coordinate.X, value - x)*/);
                x = value;
            }
        }
        public float Y {
            get => y;
            set {
                Changes?.Invoke(this/*, (Coordinate.Y, value - y)*/);
                y = value;
            }
        }

        /*public List<(Coordinate, float)> DifferenceTo(TextureCoordinates bro) {
            List<(Coordinate, float)> res = new List<(Coordinate, float)>();
            var t = this - bro;
            if (t.X != 0f) res.Add((Coordinate.X, t.X));
            if (t.Y != 0f) res.Add((Coordinate.Y, t.Y));
            return res;
        }*/

        public void Set(float x, float y) {
            this.x = x;
            this.y = y;
            Changes?.Invoke(this/*, (Coordinate.All, 0)*/);
        }
        public void Subtract(TextureCoordinates bro) {
            x -= bro.x;
            y -= bro.y;
            Changes?.Invoke(this);
        }
        public void Subtract(float x, float y) {
            this.x -= x;
            this.y -= y;
            Changes?.Invoke(this);
        }
        static public TextureCoordinates operator -(TextureCoordinates a, TextureCoordinates b) {
            var res = (TextureCoordinates)a.Copy();
            res.Subtract(b);
            return res;
        }
        public void Add(TextureCoordinates bro) {
            x += bro.x;
            y += bro.y;
            Changes?.Invoke(this);
        }
        public void Add(float x, float y) {
            this.x += x;
            this.y += y;
            Changes?.Invoke(this);
        }
        static public TextureCoordinates operator +(TextureCoordinates a, TextureCoordinates b) {
            var res = (TextureCoordinates)a.Copy();
            res.Add(b);
            return res;
        }

        public override string ToString() {
            return X + ", " + Y;
        }
    }

    /*public enum ColorComponent {
        R,G,B,A
    }*/
    public delegate void ColorComponentChangesEventHandler(Color sender/*, ColorComponent comp, float difference*/);
    public class Color :IColor {
        public static implicit operator Color((float r, float g, float b, float a) c) => new Color(c.r, c.g, c.b, c.a);
        public static implicit operator Color(Color4 c) => new Color(c.R, c.G, c.B, c.A);
        public static implicit operator Color(Vector4 c) => new Color(c.X, c.Y, c.Z, c.W);
        public static implicit operator (float R, float G, float B, float A) (Color c) => (c.R, c.G, c.B, c.A);
        public static implicit operator Vector4 (Color c) => new Vector4(c.R, c.G, c.B, c.A);
        public static implicit operator Color4 (Color c) => new Color4(c.R, c.G, c.B, c.A);
        public void Deconstruct(out float R, out float G, out float B, out float A) {
            R = this.R;
            G = this.G;
            B = this.B;
            A = this.A;
        }

        // Explicit conversion from DBInt to int. Throws an exception if the
        // given DBInt represents an unknown value.

        /*public static explicit operator int(DBInt x) {
            if (!x.defined) throw new InvalidOperationException();
            return x.value;
        }*/


        private Color4 color;
        public Color() {
            color = new Color4();
        }
        public Color(byte r, byte g, byte b, byte a) {
            color = new Color4(r, g, b, a);
        }
        public Color(float r, float g, float b, float a){
            color = new Color4(r, g, b, a);
        }
        public IColor Copy() => new Color(R, G, B, A);
        public IColor Inverted() => new Color(-R, -G, -B, -A);

        public static Color Zero => new Color(0f, 0f, 0f, 1f);

        public event ColorComponentChangesEventHandler Changes;
        public float A {
            get => color.A;
            set {
                Changes?.Invoke(this/*, ColorComponent.A, value - color.A*/);
                color.A = value;
            }
        }
        public float R {
            get => color.R;
            set {
                Changes?.Invoke(this/*, ColorComponent.R, value - color.R*/);
                color.R = value;
            }
        }
        public float G {
            get => color.G;
            set {
                Changes?.Invoke(this/*, ColorComponent.G, value - color.G*/);
                color.G = value;
            }
        }
        public float B {
            get => color.B;
            set {
                Changes?.Invoke(this/*, ColorComponent.B, value - color.B*/);
                color.B = value; 
            }
        }
        public int ToArgb => color.ToArgb();

        /*public List<(ColorComponent, float)> DifferenceTo(Color bro) {
            List<(ColorComponent, float)> res = new List<(ColorComponent, float)>();
            var t = this - bro;
            if (t.A != 0f) res.Add((ColorComponent.A, t.A));
            if (t.R != 0f) res.Add((ColorComponent.R, t.R));
            if (t.G != 0f) res.Add((ColorComponent.G, t.G));
            if (t.B != 0f) res.Add((ColorComponent.B, t.B));
            return res;
        }*/
        public void Set(float R, float G, float B, float A) {
            this.R = R;
            this.G = G;
            this.B = B;
            this.A = A;
            Changes?.Invoke(this/*, (Coordinate.All, 0)*/);
        }
        public void Subtract(Color bro) {
            R -= bro.R;
            G -= bro.G;
            B -= bro.B;
            A -= bro.A;
            Changes?.Invoke(this);
        }
        public void Subtract(float R, float G, float B, float A) {
            this.R -= R;
            this.G -= G;
            this.B -= B;
            this.A -= A;
            Changes?.Invoke(this);
        }
        static public Color operator -(Color a, Color b) {
            var res = (Color)a.Copy();
            res.Subtract(b);
            return res;
        }
        public void Add(Color bro) {
            R += bro.R;
            G += bro.G;
            B += bro.B;
            A += bro.A;
            Changes?.Invoke(this);
        }
        public void Add(float R, float G, float B, float A) {
            this.R += R;
            this.G += G;
            this.B += B;
            this.A += A;
            Changes?.Invoke(this);
        }
        static public Color operator +(Color a, Color b) {
            var res = (Color)a.Copy();
            res.Add(b);
            return res;
        }

        public float[] GetData() => new float[] { R, G, B, A };

        static Random Random = new Random();
        public static Color GenRandom => new Color((float)Random.NextDouble(), (float)Random.NextDouble(), (float)Random.NextDouble(), 1f);

        public Color4 Color4() => color;

        public override string ToString() {
            return "Color(" + R + ", " + G + ", " + B + ", " + A + ");";
        }
    }

}
