using CSUtils;
using Editor.Utilities;
using OpenTK;
using OpenTK.Graphics;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Editor.Graphic.RenderObject.Base {
    public enum Coordinate {
        X, Y, Z, All
    }
    public delegate void CoordinatesChangedEventHandler(object sender/*, List<(Coordinate, float)> changes*/);
    public delegate void ColorComponentsEventHandler(object sender/*, List<(ColorComponent, float)> changes*/);
    public delegate void SelectionStatusEventHandler(object sender, bool new_status);
    public delegate void VertexChanging(Vertex sender);

    public class VertexState {
        public readonly decimal ID;
        public readonly Vector3 Position;
        public readonly Color4 Color;
        public readonly Vector2 TextureCoordinates;
        public VertexState(decimal id, Coordinates pos = null, Color clr = null, TextureCoordinates tex = null) {
            ID = id;
            if (pos != null) Position = new Vector3(pos.X, pos.Y, pos.Z);
            if (clr != null) Color = clr.Color4();
            if (tex != null) TextureCoordinates = new Vector2(tex.X, tex.Y);
        }
        public static VertexState operator-(VertexState a, VertexState b) {
            if (a.ID != b.ID) throw new ArgumentException("ID не равны");
            return new VertexState(a.ID, (a.Position - b.Position).Coord(),
                new Color(a.Color.R - b.Color.R, a.Color.G - b.Color.G, a.Color.B - b.Color.B, a.Color.A - b.Color.A),
                new TextureCoordinates(a.TextureCoordinates.X - b.TextureCoordinates.X, a.TextureCoordinates.Y - b.TextureCoordinates.Y));

        }
        public VertexInfo ToInfo() => new VertexInfo(ID, Position.X, Position.Y, Position.Z, 
            Color == null ? null : new Color(Color.R, Color.G, Color.B, Color.A), 
            TextureCoordinates ==null ? null : new TextureCoordinates(TextureCoordinates.X, TextureCoordinates.Y));
    }

    public class Vertex : IVertex {

        #region State
        public VertexState GetState() => new VertexState(ID, Position, color, TextureCoordinates);
        public void RestoreState(VertexState State, bool IgnoreID = false) {
            if (!IgnoreID && ID != State.ID) throw new ArgumentException("ID состояния не совпадает с ID вершины");
            if (State.Position != null && Position != null) {
                Position.X = State.Position.X;
                Position.Y = State.Position.Y;
                Position.Z = State.Position.Z;
            }
            if (State.Color != null && color != null) {
                color.A = State.Color.A;
                color.R = State.Color.R;
                color.G = State.Color.G;
                color.B = State.Color.B;
            }
            if (State.TextureCoordinates != null && TextureCoordinates != null) {
                TextureCoordinates.X = State.TextureCoordinates.X;
                TextureCoordinates.Y = State.TextureCoordinates.Y;
            }
        }
        #endregion
        #region other
        public List<Face> makes_up_faces;
        public List<Edge> makes_up_edges;

        public event VertexChanging VertexChanging;

        public decimal ID { get; }
        private static decimal id_counter = 0M;
        #endregion
        #region operations
        public override bool Equals(object bro) {
            if (bro == null) return false;
            return ID == ((Vertex)(bro)).ID;
        }
        public static bool operator ==(Vertex v1, Vertex v2) {
            if ((object)v1 == null) return false;
            return v1.Equals(v2);
        }
        public static bool operator !=(Vertex v1, Vertex v2) => !(v1 == v2);

        public bool IsSet(VBO.Kind kind) {
            switch (kind) {
                case VBO.Kind.TextureCoordinates: return textureCoordinates != null;
                case VBO.Kind.Coordinates: return position != null;
                case VBO.Kind.Color: return color != null;
                case VBO.Kind.SelectionStatus: return selectionStatus;
                default: return false;
            }
        }

        public Vertex Copy(bool coords = false, bool texcoords = false, bool clr = false) {
            var res = new Vertex();
            if (texcoords) res.TextureCoordinates = TextureCoordinates;
            if (coords) res.Position = Position;
            if (clr) res.color = color;
            return res;
        }

        #endregion
        #region Selection
        bool selectionStatus = false;
        public bool SelectionStatus {
            get => selectionStatus;
            set {
                selectionStatus = value;
                StatusChanges?.Invoke(this, value);
                VertexChanging?.Invoke(this);
            }
        }
        public event SelectionStatusEventHandler StatusChanges;
        #endregion
        #region Coordinates
        private Coordinates position;
        public Coordinates Position { get => position;
            set {
                position.Changes -= CoordinateChangesHandler;
                PositionChanges?.Invoke(this/*, value.DifferenceTo(position)*/);
                position = value;
                position.Changes += CoordinateChangesHandler;
                VertexChanging?.Invoke(this);
            }
        }
        public float[] GetCoordinates() => new float[] { position.X, position.Y, position.Z/*, 1fcheck w координату*/ };
        public event CoordinatesChangedEventHandler PositionChanges;
        private void CoordinateChangesHandler(Coordinates sender/*, (Coordinate c, float delta) a*/) {
            PositionChanges?.Invoke(this/*, new List<(Coordinate, float)> { a }*/);
            VertexChanging?.Invoke(this);
        }

        #endregion
        #region Texture
        public event CoordinatesChangedEventHandler TextureChanges;

        private TextureCoordinates textureCoordinates;
        public TextureCoordinates TextureCoordinates { get => textureCoordinates;
            set {
                textureCoordinates.Changes -= TextureCoordinateChangesHandler;
                TextureChanges?.Invoke(this/*, value.DifferenceTo(textureCoordinates)*/);
                textureCoordinates = value;
                textureCoordinates.Changes += TextureCoordinateChangesHandler;
                VertexChanging?.Invoke(this);
            }
        }
        public float[] GetTextureCoordinates() => TextureCoordinates==null ? Array.Empty<float>() : new float[] { textureCoordinates.X, textureCoordinates.Y };
        private void TextureCoordinateChangesHandler(TextureCoordinates sender/*, (Coordinate c, float delta) a*/) {
            TextureChanges?.Invoke(this/*, new List<(Coordinate, float)> { a }*/);
            VertexChanging?.Invoke(this);
        }
        #endregion
        #region Color
        public event ColorComponentsEventHandler ColorChanges;

        private Color color;
        public Color Color {
            get => color;
            set {
                if (color != null) color.Changes -= ColorChangesHandler;
                ColorChanges?.Invoke(this/*, color!=null ? value.DifferenceTo(color): new List<(ColorComponent, float)>()*/);
                color = value;
                if (color != null) color.Changes += ColorChangesHandler;
                VertexChanging?.Invoke(this);
            }
        }

        public float[] GetColor() => Color == null ? Array.Empty<float>() : color.GetData();
        private void ColorChangesHandler(Color sender/*, ColorComponent c, float delta*/) {
            ColorChanges?.Invoke(this/*, new List<(ColorComponent, float)> { (c, delta) }*/);
            VertexChanging?.Invoke(this);
        }
        #endregion
        #region constructors
        public Vertex(Coordinates pos = null, Color clr = null, TextureCoordinates texcoords = null,
                        int expect_edges = 16, int expect_faces = 16, decimal? id = null) {
            if (id.HasValue) {
                ID = (decimal)id;
                if (ID > id_counter) id_counter = ID + 1;
            } else {
                if (Client.Get.Connected)
                    ID = Client.Get.IDPool.GetNext(ClientIDPool.Type.Vertex);
                else
                    ID = id_counter++;
            }
            position = pos == null ? Coordinates.Zero : pos;
            textureCoordinates = texcoords == null ? TextureCoordinates.Zero : texcoords;
            color = clr == null ? Color.Zero : clr;

            
            position.Changes += CoordinateChangesHandler;
            textureCoordinates.Changes += TextureCoordinateChangesHandler;
            color.Changes += ColorChangesHandler;
            makes_up_edges = new List<Edge>(expect_edges);
            makes_up_faces = new List<Face>(expect_faces);


        }
        public Vertex(VertexInfo i, bool copy = false) {
            if (copy) ID = id_counter++;
            else {
                ID = i.ID;
                id_counter = ID > id_counter ? id_counter = ID + 1 : id_counter;
            }

            position = new Coordinates(i.X, i.Y, i.Z);

            if (i.Color != null) color = new Color(i.Color.R, i.Color.G, i.Color.B, i.Color.A);
            else color = Color.Zero;

            if (i.TextureCoordinates != null)  textureCoordinates = new TextureCoordinates(i.TextureCoordinates.X, i.TextureCoordinates.Y);
            else textureCoordinates = TextureCoordinates.Zero;

            position.Changes += CoordinateChangesHandler;
            textureCoordinates.Changes += TextureCoordinateChangesHandler;
            color.Changes += ColorChangesHandler;

            makes_up_edges = new List<Edge>();
            makes_up_faces = new List<Face>();
        }
        #endregion

        public override string ToString() {
            return "V[" + ID+"]"+Position;
        }




        #region Interface
        public float X { get => position.X; set => position.X = value; }
        public float Y { get => position.Y; set => position.Y = value; }
        public float Z { get => position.Z; set => position.Z = value; }

        IColor IVertex.Color {
            get => Color;
            set {
                if (value is Color)
                    Color = (Color)value;
                else Color = new Color(value.R, value.G, value.B, value.A);
            }
        }

        ITextureCoordinates IVertex.TextureCoordinates {
            get => TextureCoordinates;
            set {
                if (value is TextureCoordinates)
                    TextureCoordinates = (TextureCoordinates)value;
                else TextureCoordinates = new TextureCoordinates(value.X, value.Y);
            }
        }

        public VertexInfo GetCurrentState() => new VertexInfo(ID, position.X, position.Y, position.Z, Color, TextureCoordinates);
        public void AddVertexInfo(VertexInfo info) {
            position.Move(info.X, info.Y, info.Z);
            if (info.Color!=null /*&& Color!=null*/) color.Add(info.Color.R, info.Color.G, info.Color.B, info.Color.A);
            if (info.TextureCoordinates != null /*&& TextureCoordinates!=null*/) textureCoordinates.Add(info.TextureCoordinates.X, info.TextureCoordinates.Y);
        }
        public void SetVertexInfo(VertexInfo info) {
            position.Set(info.X, info.Y, info.Z);
            if (info.Color != null /*&& Color != null*/) color.Set(info.Color.R, info.Color.G, info.Color.B, info.Color.A);
            if (info.TextureCoordinates != null /*&& TextureCoordinates != null*/) textureCoordinates.Set(info.TextureCoordinates.X, info.TextureCoordinates.Y);
        }

        public override int GetHashCode() {
            return 1213502048 + ID.GetHashCode();
        }
        #endregion
    }
}
