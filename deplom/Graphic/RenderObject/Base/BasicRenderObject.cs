using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using CSUtils;
using Editor.Graphic.Window;
using Editor.Graphics;
using Editor.Utilities;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL4;

namespace Editor.Graphic.RenderObject.Base {
    class FieldNotSetException : Exception {
        public FieldNotSetException(string a):base(a) {}
    }

    public class BasicRenderObject : IObject {
        private decimal id;
        public decimal ID { get => id; private set => id = value; }
        private static decimal id_counter = 0M;

        protected uint VAO;
        protected EBO EdgeEBO;
        protected EBO FaceEBO;

        protected Dictionary<VBO.Kind, VBO> VBOs;

        ~BasicRenderObject() {
            /*GL.BindVertexArray(0);
            var id = (int)this.id;
            GL.DeleteVertexArrays(1, ref id);*/
            //check косяк, вызывается сразу
        }

        static object locker = new object();

        #region Faces and their callbacks
        public FacesCBList Faces { get; }
        protected virtual void onFaceAddCallback(int index) {
            lock (locker) {
                var face = Faces[index];
                int F = -1; int S = -1; int T = -1; var counts = 3;
                for (int i = 0; i < Vertices.Count && counts > 0; i++) {
                    var v = Vertices[i];
                    if ((Vertex)face.First == v) {
                        F = i;
                        counts--;
                    } else if ((Vertex)face.Second == v) {
                        S = i;
                        counts--;
                    } else if ((Vertex)face.Third == v) {
                        T = i;
                        counts--;
                    }
                }
                if (counts == 0) FaceEBO.AddData(F, S, T);
                ((IObject)this).Faces.Add(face.ID, face);//TODO элемент с тем же ключом уже был добавлен фууууфуфуфуфу
                if (Scene.Loaded) Scene.Get.State.FacesCount++;
            }
        }
        protected virtual void onFaceDeleteCallback(int index) {
            ((IObject)this).Faces.Remove(Faces[index].ID);
            FaceEBO.RemoveRange(index * 3, 3);
            if (Scene.Loaded) Scene.Get.State.FacesCount--;
        }
        protected virtual void onFaceSetCallback(int index) {
            /*var face = Faces[index];
            var l = new List<int>();
            for (int i = 0; i < Vertices.Count; i++) {
                var v = Vertices[i];
                if (face.Contains(v)) l.Add(i);
            }
            FaceEBO.SetData(index * 3, l);
            check надо ли */
        }
        protected virtual void onFaceInsertCallback(int index) {
            //check надо ли
        }
        protected virtual void onFacesAddRangeCallback(int from, int to) {
            var indices = new List<int>(to - from + 3);
            for (int f = from; f <= to; f++) {
                var face = Faces[f];
                int F = -1; int S = -1; int T = -1; var counts = 3;
                for (int i = 0; i < Vertices.Count && counts > 0; i++) {
                    var v = Vertices[i];

                    if ((Vertex)face.First == v) {
                        F = i;
                        counts--;
                    } else if ((Vertex)face.Second == v) {
                        S = i;
                        counts--;
                    } else if ((Vertex)face.Third == v) {
                        T = i;
                        counts--;
                    }
                }
                indices.AddRange(F, S, T);
                ((IObject)this).Faces.Add(face.ID, face);
            }
            FaceEBO.AddData(indices);
            if (Scene.Loaded) Scene.Get.State.FacesCount += to - from + 1;
        }
        protected virtual void onFacesInsertRangeCallback(int from, int to) {
            //check надо ли
        }
        protected virtual void onFacesClearCallback() {
            FaceEBO.Clear();
            ((IObject)this).Faces.Clear();
            if (Scene.Loaded) Scene.Get.State.FacesCount -= Faces.Count;
        }
        #endregion
        #region Edges and their callbacks
        public EdgesCBList Edges { get; }
        protected virtual void onEdgeAddCallback(int index) {
            lock (locker) {
                var edge = Edges[index];
                for (int i = 0, counts = 2; i < Vertices.Count && counts > 0; i++) {
                    var v = Vertices[i];
                    if (edge.Contains(v)) {
                        EdgeEBO.AddData(i);
                        counts--;
                    }
                }
                ((IObject)this).Edges.Add(edge.ID, edge);
                if (Scene.Loaded) Scene.Get.State.EdgesCount++;
            }
        }
        protected virtual void onEdgeDeleteCallback(int index) {
            EdgeEBO.RemoveRange(index * 2, 2);
            ((IObject)this).Edges.Remove(Edges[index].ID);
            if (Scene.Loaded) Scene.Get.State.EdgesCount--;
        }
        protected virtual void onEdgeSetCallback(int index) {
            //check надо ли
        }
        protected virtual void onEdgeInsertCallback(int index) {
            //check надо ли
        }
        protected virtual void onEdgesAddRangeCallback(int from, int to) {
            var indices = new List<int>(to - from + 3);
            for (int f = from; f <= to; f++) {
                var edge = Edges[f];
                for (int i = 0, counts = 2; i < Vertices.Count && counts > 0; i++) {
                    var v = Vertices[i];
                    if (edge.Contains(v)) {
                        indices.Add(i);
                        counts--;
                    }
                }
                ((IObject)this).Edges.Add(edge.ID, edge);
            }
            EdgeEBO.AddData(indices);
            if (Scene.Loaded) Scene.Get.State.EdgesCount += to - from + 1;
        }
        protected virtual void onEdgesInsertRangeCallback(int from, int to) {
            //check надо ли
        }
        protected virtual void onEdgesClearCallback() {
            EdgeEBO.Clear();
            ((IObject)this).Edges.Clear();
            if (Scene.Loaded) Scene.Get.State.EdgesCount -= Edges.Count;
        }

        #endregion
        #region Vertices and their callbacks
        public VertexCBList Vertices { get; private set; }
        protected virtual void onVertexAddCallback(int index) {
            var vertex = Vertices[index];
            if (Scene.Loaded) MainWindow.FromGUIThread(MainWindow.Get, () => Debug.WriteLine("onVertexAddCallback(" + vertex + ")"));
            /*if (IsSet(VBO.Kind.Coordinates))          */ VBOs[VBO.Kind.Coordinates].AddData(vertex.GetCoordinates());
            /*if (IsSet(VBO.Kind.TextureCoordinates))   */ VBOs[VBO.Kind.TextureCoordinates].AddData(vertex.GetTextureCoordinates());
            /*if (IsSet(VBO.Kind.Color))                */ VBOs[VBO.Kind.Color].AddData(vertex.GetColor());
            /*if (IsSet(VBO.Kind.SelectionStatus))      */ VBOs[VBO.Kind.SelectionStatus].AddData(new float[] { vertex.SelectionStatus ? 1f : 0f });
            ((IObject)this).Vertices.Add(vertex.ID, vertex);
            if (Scene.Loaded) Scene.Get.State.VerticesCount++;
        }
        protected virtual void onVertexDeleteCallback(int index) {
            var vertex = Vertices[index];
            /*if (IsSet(VBO.Kind.Coordinates))          */ VBOs[VBO.Kind.Coordinates].DeleteDataPack(index);
            /*if (IsSet(VBO.Kind.TextureCoordinates))   */ VBOs[VBO.Kind.TextureCoordinates].DeleteDataPack(index);
            /*if (IsSet(VBO.Kind.Color))                */ VBOs[VBO.Kind.Color].DeleteDataPack(index);
            /*if (IsSet(VBO.Kind.SelectionStatus))      */ VBOs[VBO.Kind.SelectionStatus].DeleteDataPack(index);
            ((IObject)this).Vertices.Remove(vertex.ID);
            if (Scene.Loaded) Scene.Get.State.VerticesCount--;
        }
        protected virtual void onVertexSetCallback(int index) {
            var vertex = Vertices[index];
            /*if (IsSet(VBO.Kind.Coordinates))          */ VBOs[VBO.Kind.Coordinates].SetDataPack(index, vertex.GetCoordinates());
            /*if (IsSet(VBO.Kind.TextureCoordinates))   */ VBOs[VBO.Kind.TextureCoordinates].SetDataPack(index, vertex.GetTextureCoordinates());
            /*if (IsSet(VBO.Kind.Color))                */ VBOs[VBO.Kind.Color].SetDataPack(index, vertex.GetColor());
            /*if (IsSet(VBO.Kind.SelectionStatus))      */ VBOs[VBO.Kind.SelectionStatus].SetDataPack(index, new float[] { vertex.SelectionStatus ? 1f : 0f });
        }
        protected virtual void onVertexInsertCallback(int index) {
            var vertex = Vertices[index];
            /*if (IsSet(VBO.Kind.Coordinates))          */ VBOs[VBO.Kind.Coordinates].InsertDataPack(index, vertex.GetCoordinates());
            /*if (IsSet(VBO.Kind.TextureCoordinates))   */ VBOs[VBO.Kind.TextureCoordinates].InsertDataPack(index, vertex.GetTextureCoordinates());
            /*if (IsSet(VBO.Kind.Color))                */ VBOs[VBO.Kind.Color].InsertDataPack(index, vertex.GetColor());
            /*if (IsSet(VBO.Kind.SelectionStatus))      */ VBOs[VBO.Kind.SelectionStatus].InsertDataPack(index, new float[] { vertex.SelectionStatus ? 1f : 0f });
            ((IObject)this).Vertices.Add(vertex.ID, vertex);
            if (Scene.Loaded) Scene.Get.State.VerticesCount++;
        }
        protected virtual void onVerticesClearCallback() {
            /*if (IsSet(VBO.Kind.Coordinates))          */ VBOs[VBO.Kind.Coordinates].Clear();
            /*if (IsSet(VBO.Kind.TextureCoordinates))   */ VBOs[VBO.Kind.TextureCoordinates].Clear();
            /*if (IsSet(VBO.Kind.Color))                */ VBOs[VBO.Kind.Color].Clear();
            /*if (IsSet(VBO.Kind.SelectionStatus))      */ VBOs[VBO.Kind.SelectionStatus].Clear();
            ((IObject)this).Vertices.Clear();
            if (Scene.Loaded) Scene.Get.State.VerticesCount -= Vertices.Count;
        }
        //ошибочно было add
        protected virtual void onVerticesInsertRangeCallback(int from, int to) {
            for (int i = from; i <= to; i++) {
                var vertex = Vertices[i];
                /*if (IsSet(VBO.Kind.Coordinates))       */ VBOs[VBO.Kind.Coordinates].InsertDataPack(i, vertex.GetCoordinates());
                /*if (IsSet(VBO.Kind.TextureCoordinates))*/ VBOs[VBO.Kind.TextureCoordinates].InsertDataPack(i, vertex.GetTextureCoordinates());
                /*if (IsSet(VBO.Kind.Color))             */ VBOs[VBO.Kind.Color].InsertDataPack(i, vertex.GetColor());
                /*if (IsSet(VBO.Kind.SelectionStatus))   */ VBOs[VBO.Kind.SelectionStatus].InsertDataPack(i, new float[] { vertex.SelectionStatus ? 1f : 0f });
                ((IObject)this).Vertices.Add(vertex.ID, vertex);
                if (Scene.Loaded) Scene.Get.State.VerticesCount += to - from + 1;
            }
        }
        //ошибочно было Insert
        protected virtual void onVerticesAddRangeCallback(int from, int to) {
            int count = to - from + 1;
            var crds = new List<float>(VBOs[VBO.Kind.Coordinates].SegmentSize * count);
            var clrs = new List<float>(VBOs[VBO.Kind.Color].SegmentSize * count);
            var txs = new List<float>(VBOs[VBO.Kind.TextureCoordinates].SegmentSize * count);
            var sss = new List<float>(VBOs[VBO.Kind.SelectionStatus].SegmentSize * count);
            for (int i = from; i <= to; i++) {
                var vertex = Vertices[i];
                crds.AddRange(vertex.GetCoordinates());
                clrs.AddRange(vertex.GetColor());
                txs.AddRange(vertex.GetTextureCoordinates());
                sss.AddRange(new float[] { vertex.SelectionStatus ? 1f : 0f });
                ((IObject)this).Vertices.Add(vertex.ID, vertex);
            }
            VBOs[VBO.Kind.Coordinates].AddData(crds);
            VBOs[VBO.Kind.TextureCoordinates].AddData(txs);
            VBOs[VBO.Kind.Color].AddData(clrs);
            VBOs[VBO.Kind.SelectionStatus].AddData(sss);
            if (Scene.Loaded) Scene.Get.State.VerticesCount += to - from + 1;
        }
        #endregion

        void Build(bool force = false) {
            GL.BindVertexArray(VAO); //прибиндили VAO
            foreach (var VBO in VBOs) if (force || !VBO.Value.Built && VBO.Value.Data.Length > 0) VBO.Value.Build();
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0); //обнулили привязку VBO, кабы что не этого
            if (!EdgeEBO.Built) EdgeEBO.Build();
            if (!FaceEBO.Built) FaceEBO.Build();
        }

        protected BasicRenderObject(int expect_verts = 100, int expect_edges = 100, int expect_faces = 100) {
            VAO = (uint)GL.GenVertexArray();//сгенерировали VAO
            VBOs = new Dictionary<VBO.Kind, VBO>();
            EdgeEBO = new EBO();
            FaceEBO = new EBO();
            Vertices = new VertexCBList(expect_verts);
            Vertices.AddCallback = onVertexAddCallback;
            Vertices.DeleteCallback = onVertexDeleteCallback;
            Vertices.SetCallback = onVertexSetCallback;
            Vertices.InsertCallback = onVertexInsertCallback;
            Vertices.ClearCallback = onVerticesClearCallback;
            Vertices.AddRangeCallback = onVerticesAddRangeCallback;
            Vertices.InsertRangeCallback = onVerticesInsertRangeCallback;

            Faces = new FacesCBList(expect_faces);
            Faces.AddCallback = onFaceAddCallback;
            Faces.DeleteCallback = onFaceDeleteCallback;
            Faces.ClearCallback = onFacesClearCallback;
            Faces.AddRangeCallback = onFacesAddRangeCallback;

            Edges = new EdgesCBList(expect_edges);
            Edges.AddCallback = onEdgeAddCallback;
            Edges.DeleteCallback = onEdgeDeleteCallback;
            Edges.ClearCallback = onEdgesClearCallback;
            Edges.AddRangeCallback = onEdgesAddRangeCallback;
        }


        public bool IsSet(VBO.Kind kind) => (VBOs.ContainsKey(kind));
        public static BasicRenderObject Standard(
            /*bool coords = false, 
            bool colors = false,
            bool textures = false,*/
            int expect_verts = 100,
            int expect_edges = 100,
            int expect_faces = 100,
            decimal? id = null) {
            var res = new BasicRenderObject(expect_verts, expect_edges, expect_faces);
            if (id.HasValue) {
                res.ID = (decimal)id;
                if (res.ID > id_counter) id_counter = res.ID + 1;
            } else {
                if (Client.Get.Connected)
                    res.ID = Client.Get.IDPool.GetNext(ClientIDPool.Type.Object);
                else
                    res.ID = id_counter++;
            }
            Debug.WriteLine("idc = " + id_counter);
            /*if (coords) */
            res.VBOs.Add(VBO.Kind.Coordinates, VBO.Standard(VBO.Kind.Coordinates));
            /*if (colors) */
            res.VBOs.Add(VBO.Kind.Color, VBO.Standard(VBO.Kind.Color));
            /*if (textures)*/
            res.VBOs.Add(VBO.Kind.TextureCoordinates, VBO.Standard(VBO.Kind.TextureCoordinates));
            res.VBOs.Add(VBO.Kind.SelectionStatus, VBO.Standard(VBO.Kind.SelectionStatus));
            return res;
        }

        public void Render(PrimitiveType type = PrimitiveType.Points) {
            if (Vertices.Count == 0) return;
            lock (locker) Build();
            var Model = ModelMatrix;
            GL.UniformMatrix4(17, false, ref Model);
            switch (type) {
                case PrimitiveType.Points: { RenderPoints(); break; }
                case PrimitiveType.Lines: { RenderLines(); break; }
                case PrimitiveType.Triangles: { RenderFaces(); break; }
            }
            GL.BindVertexArray(0); //отвязали VAO, чтоб не набедокурить

        }
        #region рендерики
        private void RenderPoints() {
            GL.DrawArrays(PrimitiveType.Points, 0, Vertices.Count);
        }
        private void RenderLines() {
            if (Edges.Count == 0) return;
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, EdgeEBO.ID); //привязали EBO
            GL.DrawElements(PrimitiveType.Lines, EdgeEBO.Indexes.Length, DrawElementsType.UnsignedInt, 0);
        }
        private void RenderFaces() {
            if (Faces.Count == 0) return;
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, FaceEBO.ID); //привязали EBO
            GL.DrawElements(PrimitiveType.Triangles, FaceEBO.Indexes.Length, DrawElementsType.UnsignedInt, 0);
        }
        #endregion
        #region добавки
        public void AddEdge(Vertex v1, Vertex v2, decimal? ID = null) {
            if (v1 == v2) throw new ArgumentException("Вершина одна и та же");
            if (!Edges.Contains(v1, v2)) {
                var edge = new Edge(v1, v2, ID);
                Edges.Add(edge);
            }
        }

        public void AddEdge(decimal ID, decimal v1, decimal v2) {
            var vs = Vertices.FindAll(x => x.ID == v1 || x.ID == v2);
            Debug.WriteLine("trying to add edge ["+v1+"-"+v2+"]");
            AddEdge(vs[0], vs[1], ID);
        }
        public void AddEdge(int v1, int v2) {
            AddEdge(Vertices[v1], Vertices[v2]);
        }

        public void AddFace(Vertex v1, Vertex v2, Vertex v3, decimal? ID = null) {
            AddEdge(v1, v2); AddEdge(v1, v3); AddEdge(v3, v2);
            if (!Faces.Contains(v1, v2, v3)) {
                var f = new Face(v1, v2, v3, ID);
                Faces.Add(f);
            }
        }
        public void AddFace(decimal ID, decimal v1, decimal v2, decimal v3) {
            var vs = new Vertex[3]; var counts = 3;
            for (int i = 0; i < Vertices.Count && counts > 0; i++) {
                var v = Vertices[i];
                if (v1 == v.ID) {
                    vs[0] = v;
                    counts--;
                } else if (v2 == v.ID) {
                    vs[1] = v;
                    counts--;
                } else if (v3 == v.ID) {
                    vs[2] = v;
                    counts--;
                }
            }
            if (counts == 0)
                AddFace(vs[0], vs[1], vs[2], ID);
        }
        public void AddFace(int v1, int v2, int v3) => AddFace(Vertices[v1], Vertices[v2], Vertices[v3]);

        /*private void AddFaceIgnoreChecks(Vertex v1, Vertex v2, Vertex v3) {
            //todo оптимизировать формирование ебо, чтоб он на ходу дописывался и формебо делал только окончательную загрузку на видеокарту
            AddEdgeIgnoreChecks(v1, v2); AddEdgeIgnoreChecks(v1, v3); AddEdgeIgnoreChecks(v3, v2);
            var f = new Face(v1, v2, v3);
            Faces.Add(f);
        }
        private void AddFaceIgnoreChecks(int v1, int v2, int v3) => AddFaceIgnoreChecks(Vertices[v1], Vertices[v2], Vertices[v3]);
        */
        #endregion


        public Matrix4 ModelMatrix { get; set; } = Matrix4.Identity;

        public void Join(IObject smth) {
            Vertices.AddRange((smth as BasicRenderObject).Vertices);
            Edges.AddRange((smth as BasicRenderObject).Edges);
            Faces.AddRange((smth as BasicRenderObject).Faces);
        }

        public void AcceptModelMatrix() {
            foreach (var v in Vertices) 
                v.Position.Set((new Vector4(v.Position.ToVector3(), 1f) * ModelMatrix).Xyz);
            ModelMatrix = Matrix4.Identity;
        }


        Dictionary<decimal, IVertex> IObject.Vertices { get; } = new Dictionary<decimal, IVertex>();
        Dictionary<decimal, IEdge> IObject.Edges { get; } = new Dictionary<decimal, IEdge>();
        Dictionary<decimal, IFace> IObject.Faces { get; } = new Dictionary<decimal, IFace>();

        void IObject.CreateVertex(VertexInfo info) => MainWindow.FromGUIThread(MainWindow.Get, () => Vertices.Add(new Vertex(info)));
        void IObject.DeleteVertex(decimal ID) => MainWindow.FromGUIThread(MainWindow.Get, () => Vertices.DeleteByID(ID));
        IVertex IObject.GetVertexByID(decimal ID) => Vertices.GetByID(ID);

        public BasicRenderObject Copy() {
            var res = Standard(Vertices.Capacity, Edges.Capacity, Faces.Capacity);
            res.Vertices.AddRange(Vertices.CloneWithClones());
            for (int i = 0; i < EdgeEBO.Indexes.Length; i += 2) {
                var i1 = EdgeEBO.Indexes[i];
                var i2 = EdgeEBO.Indexes[i + 1];
                res.AddEdge(i1, i2);
            }
            for (int i = 0; i < FaceEBO.Indexes.Length; i += 3) {
                var i1 = FaceEBO.Indexes[i];
                var i2 = FaceEBO.Indexes[i + 1];
                var i3 = FaceEBO.Indexes[i + 2];
                Debug.WriteLine(i1 + " " + i2 + " " + i3);
                res.AddFace(i1, i2, i3);
            }
            res.ModelMatrix = ModelMatrix;
            return res;
        }

        #region Edge
        public void CreateEdge(decimal ID, IVertex f, IVertex s) => CreateEdge(ID, f.ID, s.ID);
        public void CreateEdge(decimal ID, decimal f_id, decimal s_id) => AddEdge(ID, f_id, s_id);
        public void DeleteEdge(decimal ID) => Edges.Remove(x => x.ID == ID);
        public void DeleteEdge(IVertex f, IVertex s) => Edges.Remove(x => x.Contains(f.ID, s.ID));
        public void DeleteEdge(decimal f, decimal s) => Edges.Remove(x => x.Contains(f, s));
        public IEdge GetEdge(decimal ID) => Edges.Find(x => x.ID == ID);
        public IEdge GetEdge(IVertex f, IVertex s) => GetEdge(f.ID, s.ID);
        public IEdge GetEdge(decimal f, decimal s) => Edges.Find(x => x.Contains(f, s));
        #endregion
        #region Face
        public void CreateFace(decimal ID, IVertex f, IVertex s, IVertex t) => CreateFace(ID, f.ID, s.ID, t.ID);
        public void CreateFace(decimal ID, decimal f_id, decimal s_id, decimal t_id) => AddFace(ID, f_id, s_id, t_id);
        public void DeleteFace(decimal ID) => Faces.Remove(x => x.ID == ID);
        public void DeleteFace(IVertex f, IVertex s, IVertex t) => Faces.Remove(x => x.Contains(f.ID, s.ID, t.ID));
        public void DeleteFace(decimal f, decimal s, decimal t) => Faces.Remove(x => x.Contains(f, s, t));
        public IFace GetFace(decimal ID) => Faces.Find(x => x.ID == ID);
        public IFace GetFace(IVertex f, IVertex s, IVertex t) => GetFace(f.ID, s.ID, t.ID);
        public IFace GetFace(decimal f, decimal s, decimal t) => Faces.Find(x => x.Contains(f, s, t));
        #endregion

        public static class Primitive {
            public static BasicRenderObject Cube(float side, decimal? id = null) {
                var res = Standard(id: id);
                var vertices = new List<Vertex>();
                vertices.Add(new Vertex(new Coordinates(-side / 2, -side / 2, -side / 2), new Color(1.0f, 0.0f, 0.0f, 1f)));//0 красный
                vertices.Add(new Vertex(new Coordinates(-side / 2, side / 2, -side / 2), new Color(0.0f, 1.0f, 0.0f, 1f)));//1 зеленый
                vertices.Add(new Vertex(new Coordinates(side / 2, -side / 2, -side / 2), new Color(0.0f, 0.0f, 1.0f, 1f)));//2 синий
                vertices.Add(new Vertex(new Coordinates(side / 2, side / 2, -side / 2), new Color(0.0f, 1.0f, 1.0f, 1f)));//3 голубой
                vertices.Add(new Vertex(new Coordinates(-side / 2, -side / 2, side / 2), new Color(1.0f, 1.0f, 1.0f, 1f)));//4 белый
                vertices.Add(new Vertex(new Coordinates(-side / 2, side / 2, side / 2), new Color(0.0f, 0.0f, 0.0f, 1f)));//5 черный
                vertices.Add(new Vertex(new Coordinates(side / 2, -side / 2, side / 2), new Color(1.0f, 1.0f, 0.0f, 1f)));//6 желтый
                vertices.Add(new Vertex(new Coordinates(side / 2, side / 2, side / 2), new Color(1.0f, 0.0f, 1.0f, 1f)));//7 розовый
                res.Vertices.AddRange(vertices);

                res.AddFace(0, 1, 2);
                res.AddFace(0, 4, 1);
                res.AddFace(0, 2, 4);
                res.AddFace(1, 3, 2);
                res.AddFace(1, 5, 3);
                res.AddFace(1, 4, 5);
                res.AddFace(2, 3, 6);
                res.AddFace(2, 6, 4);
                res.AddFace(3, 5, 7);
                res.AddFace(3, 7, 6);
                res.AddFace(4, 6, 5);
                res.AddFace(5, 6, 7);

                return res;
            }
            public static BasicRenderObject Ray(Ray ray) => Ray(ray.Origin, ray.Direction + ray.Origin);
            public static BasicRenderObject Ray(Vector3 origin, Vector3 endpoint) {
                var res = Standard();
                res.Vertices.Add(new Vertex(origin.Coord(), new Color(1f, 1f, 1f, 1f)));
                res.Vertices.Add(new Vertex(endpoint.Coord(), new Color(0f, 1f, 1f, 1f)));
                res.AddEdge(0, 1);
                return res;
            }
            public static BasicRenderObject Sphere(Vector3 Center, float radius, int levels, int dots_on_level) => Sphere(Center.Coord(), radius, levels, dots_on_level);
            public static BasicRenderObject Sphere((float x, float y, float z) center, float radius, int levels, int dots_on_level) => Sphere(new Coordinates(center.x, center.y, center.z), radius, levels, dots_on_level);
            public static BasicRenderObject Sphere(Coordinates Center, float radius, int levels, int dots_on_level) {
                var exp_v = levels * (dots_on_level + 3);
                var exp_e = levels * (dots_on_level + 3) * 6;
                var exp_f = levels * (dots_on_level + 3) * 2;
                var res = Standard(
                    expect_verts: exp_v,
                    expect_edges: exp_e,
                    expect_faces: exp_f);
                levels -= 2;
                var horizontal_dots_step = (float)(2 * Math.PI / dots_on_level);
                var vertical_dots_step = (float)(Math.PI / levels);
                var Θ = vertical_dots_step; //по вертикали
                var Φ = 0f;                   //по горизонтали
                var vertices = new List<Vertex>(exp_v);
                vertices.Add(new Vertex(new Coordinates(Center.X, Center.Y + radius, Center.Z), Color.GenRandom));
                for (int level = 0; level < levels - 1; level++) {
                    for (int i = 0; i < dots_on_level; i++) {
                        float x = (float)(Center.X + radius * Math.Sin(Θ) * Math.Cos(Φ));
                        float y = (float)(Center.Y + radius * Math.Cos(Θ));
                        float z = (float)(Center.Z + radius * Math.Sin(Θ) * Math.Sin(Φ));
                        Φ += horizontal_dots_step;
                        vertices.Add(new Vertex(new Coordinates(x, y, z), Color.GenRandom));
                    }
                    Θ += vertical_dots_step;
                }
                vertices.Add(new Vertex(new Coordinates(Center.X, Center.Y - radius, Center.Z), Color.GenRandom));
                res.Vertices.AddRange(vertices);

                for (int i = 0; i < dots_on_level - 1; i++) {
                    res.AddFace(
                        0,
                        i + 2, //это
                        i + 1);//было здесь
                }
                res.AddFace(0, 1, dots_on_level);
                for (int i = 0; i < dots_on_level - 1; i++) {
                    res.AddFace(i + 1, i + 2, i + dots_on_level + 1);
                }
                res.AddFace(dots_on_level, 1, dots_on_level * 2);


                #region connecting center
                for (int level = 1; level < levels - 2; level++) {
                    var cur = level * dots_on_level + 1;
                    for (int i = 0; i < dots_on_level - 1; i++) {
                        res.AddFace(
                            cur + i,
                            cur + i - dots_on_level + 1, //это
                            cur + i + 1);               //было здесь
                        res.AddFace(
                            cur + i,
                            cur + i + 1,
                            cur + i + dots_on_level);
                    }
                    res.AddFace(cur, cur + dots_on_level * 2 - 1, cur + dots_on_level - 1);
                    cur -= dots_on_level;
                    res.AddFace(cur, cur + dots_on_level, cur + dots_on_level * 2 - 1);
                }
                #endregion

                #region connecting bottom to last
                var last = res.Vertices.Count - 1;

                for (int i = 0; i < dots_on_level - 1; i++) {
                    res.AddFace(last - i - 1, last - i - 2, last - i - dots_on_level - 1);
                }
                res.AddFace(
                    last - 1,
                    last - dots_on_level * 2, //это
                    last - dots_on_level);//было здесь

                for (int i = 0; i < dots_on_level - 1; i++) {
                    res.AddFace(
                        last,
                        last - i - 2, //это
                        last - i - 1);//было здесь
                }
                res.AddFace(last, last - 1, last - dots_on_level);
                #endregion
                //res.FormEBO();
                return res;
            }

            public static BasicRenderObject Cone(Vector3 Center, float radius, int dots, float height) => Cone(Center.Coord(), radius, dots, height);
            public static BasicRenderObject Cone((float x, float y, float z) Center, float radius, int dots, float height) => Cone(Center.Coord(), radius, dots, height);
            public static BasicRenderObject Cone(Coordinates Center, float radius, int dots, float height) {
                var exp_v = dots < 48 ? 100 : (dots + 2) * 2;
                var exp_e = exp_v * 5;
                var exp_f = exp_v * 2;
                var res = Standard(
                    expect_verts: exp_v,
                    expect_edges: exp_e,
                    expect_faces: exp_f);
                var step = Math.PI * 2 / dots;
                var vertices = new List<Vertex>(exp_v);
                for (int i = 0; i < dots; i++) {
                    var x = radius * Math.Cos(step * i) + Center.X;
                    var z = radius * Math.Sin(step * i) + Center.Z;
                    vertices.Add(new Vertex(new Coordinates((float)x, (float)Center.Y, (float)z), Color.GenRandom));
                }
                vertices.Add(new Vertex(new Coordinates(Center.X, Center.Y, Center.Z), Color.GenRandom));
                vertices.Add(new Vertex(new Coordinates(Center.X, Center.Y + height, Center.Z), Color.GenRandom));
                res.Vertices.AddRange(vertices);

                for (int i = 0; i < dots - 1; i++) {
                    res.AddFace(i, i + 1, dots);
                    res.AddFace(i, dots + 1, i + 1);
                }
                res.AddFace(0, dots, dots - 1);
                res.AddFace(0, dots - 1, dots + 1);

                return res;
            } //конус

            public static BasicRenderObject Grid(float width, float step) {
                var res = BasicRenderObject.Standard();
                var startX = -width / 2; var startZ = -width / 2;
                var clr = Configs.WorldGrid;
                for (int i = 0; i <= width / step; i++) {
                    var v1 = new Vertex(new Coordinates(startX, 0f, startZ + step * i), clr);
                    var v2 = new Vertex(new Coordinates(startX + width, 0f, startZ + step * i), clr);
                    res.Vertices.Add(v1);
                    res.Vertices.Add(v2);
                    res.AddEdge(v1, v2);

                    v1 = new Vertex(new Coordinates(startX + step * i, 0f, startZ), clr);
                    v2 = new Vertex(new Coordinates(startX + step * i, 0f, startZ + width), clr);
                    res.Vertices.Add(v1);
                    res.Vertices.Add(v2);

                    res.AddEdge(v1, v2);
                }

                //res.FormEBO();
                return res;
            }


            public static BasicRenderObject Vector(Vector3 Start, Vector3 Final) {
                var res = BasicRenderObject.Standard();
                res.Vertices.Add(new Vertex(Start.Coord(), new Color(1f, 1f, 1f, 1f)));
                res.Vertices.Add(new Vertex(Final.Coord(), new Color(0f, 0f, 0f, 1f)));
                res.AddEdge(0, 1);
                //res.FormEBO();
                /* var vec_len = (Final - Start).Length;
                 Vector3 vec_norm = (Final - Start).Normalized();
                 //углы поворотов к осям Ox, Oy, Oz
                 float AngleOX = (float)Math.Acos(vec_norm.X / vec_norm.Length);
                 float AngleOY = (float)Math.Acos(vec_norm.Y / vec_norm.Length);
                 float AngleOZ = (float)Math.Acos(vec_norm.Z / vec_norm.Length);
                 //
                 var Height = vec_len / 10f;
                 var Radius = Height / 4f;
                 Vector3 vec = vec_norm.SetLength(vec_len - Height);
                 var cone = Primitive.Cone(Final.Zxy, Radius, 12, Height);
                 cone.Model = cone.Model * Matrix4.CreateRotationZ(AngleOZ)* Matrix4.CreateRotationY(AngleOY) * Matrix4.CreateRotationX(AngleOX); 
                 cone.AcceptModelMatrix();
                 res.Join(cone);*/

                return res;
            }


        }
    }
}
