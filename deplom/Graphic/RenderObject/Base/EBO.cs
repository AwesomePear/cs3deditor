using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL4;

namespace Editor.Graphic.RenderObject.Base {
    public class EBO {
        public uint ID { get => id; private set => id = value; }
        List<int> indexes = new List<int>();
        private uint id;

        public int[] Indexes => indexes.ToArray();

        public void SetData(IEnumerable<int> data) {
            indexes = data.ToList();
            Built = false;
        }
        public void SetData(int to, params int[] vals) {
            for (int i = 0; i < vals.Length; i++)
                indexes[to + i] = vals[i];
            Built = false;
        }
        public void SetData(int to, IEnumerable<int> vals) {
            for (int i = 0; i < vals.Count(); i++)
                indexes[to + i] = vals.ElementAt(i);
            Built = false;
        }
        public void AddData(IEnumerable<int> data) {
            indexes.AddRange(data);
            Built = false;
        }
        public void AddData(params int[] data) {
            indexes.AddRange(data);
            Built = false;
        }
        public void AddData(int data) {
            indexes.Add(data);
            Built = false;
        }
        public void RemoveRange(int from, int length) {
            indexes.RemoveRange(from, length);
            Built = false;
        }

        /// <summary>
        /// Вставляет набор значений в указанную позицию 
        /// </summary>
        public void InsertDataPack(int into, IEnumerable<int> pack) {
            if (indexes.Count < into) indexes.AddRange(pack);
            else indexes.InsertRange(into, pack);
            Built = false;
        }

        public void Clear() {
            indexes.Clear();
            Built = false;
        }

        public int Size => indexes.Count * 4;//потому что int
        public bool Built { get; private set; } = false;
        public void Build() {
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, ID); //привязали EBO
            GL.BufferData(BufferTarget.ElementArrayBuffer, Size, Indexes, BufferUsageHint.StaticDraw);
            Built = true;
        }
        public EBO() {
            ID = (uint)GL.GenBuffer();
        }


        ~EBO() {
            /*GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);
            GL.DeleteBuffers(1, ref id); */
            //check косяк, вызывается сразу
        }
    }
}
