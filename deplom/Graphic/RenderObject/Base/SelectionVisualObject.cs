using CSUtils;
using Editor.Utilities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Editor.Graphic.RenderObject.Base {
    public class SelectionVisualObject :BasicRenderObject {

        private List<Face> potential = new List<Face>();

        public static SelectionVisualObject Standard() {
            var res = new SelectionVisualObject();
            res.VBOs.Add(VBO.Kind.Coordinates, VBO.Standard(VBO.Kind.Coordinates));
            res.VBOs.Add(VBO.Kind.Color, VBO.Standard(VBO.Kind.Color));
            res.VBOs.Add(VBO.Kind.TextureCoordinates, VBO.Standard(VBO.Kind.TextureCoordinates));
            res.VBOs.Add(VBO.Kind.SelectionStatus, VBO.Standard(VBO.Kind.SelectionStatus));
            
            return res;
        }
        public void Clear() {
            Vertices.Clear();
            ((IObject)this).Vertices.Clear();
        }
        public void SelectAll() {


        }

        protected override void onVertexAddCallback(int index) {
            base.onVertexAddCallback(index);
            potential.AddUniqueRange(Vertices[index].makes_up_faces);
            Vertices[index].SelectionStatus = true;

            foreach (var x in potential)
                if (!Faces.Contains(x) && x.AllVerticesKeptIn(Vertices))
                    Faces.Add(x);
            Debug.WriteLine(Faces.ListToString());
            Debug.WriteLine(FaceEBO.Indexes.ArrayToString());
        }

        protected override void onVertexDeleteCallback(int index) {
            var v = Vertices[index];
            var faces_deselect = Faces.FindAll(x => x.Contains(v));
            if (Faces != null && Faces.Count > 0) Faces.RemoveRange(faces_deselect);
            Vertices[index].SelectionStatus = false;
            base.onVertexDeleteCallback(index);
            List<Face> to_remove = new List<Face>();
            foreach (var x in v.makes_up_faces)
                if (!x.AnyVerticeKeptIn(Vertices))
                    to_remove.Add(x);
            

            potential.RemoveRange(to_remove);
        }
        protected override void onVerticesClearCallback() {
            foreach (var x in Vertices) x.SelectionStatus = false;
            Faces.Clear();
            potential.Clear();
            base.onVerticesClearCallback();
        }

        //todo: оптимизировать(добавить функции "выделить полигон", "выделить всё"), сделать одним классом всё выделение
    }
}
