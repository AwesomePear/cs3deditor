using Editor.Utilities;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSUtils;

namespace Editor.Graphic.RenderObject.Base {
    public class Face:IFace {
        public decimal ID { get; } 
        private static decimal id_counter = 0M;
        public override bool Equals(object bro) {
            if (bro == null) return this == null;//check работает ли
            return ((Face)bro).Contains(First, Second, Third);
        }
        public (Vertex First, Vertex Second, Vertex Third) Vertices { get; }
        public Face(Vertex a, Vertex b, Vertex c, decimal? id = null) {
            if (id.HasValue) {
                ID = (decimal)id;
                if (ID > id_counter) id_counter = ID + 1;
            } else {
                if (Client.Get.Connected)
                    ID = Client.Get.IDPool.GetNext(ClientIDPool.Type.Face);
                else
                    ID = id_counter++;
            }
            Vertices = (a, b, c);
            a.makes_up_faces.Add(this);
            b.makes_up_faces.Add(this);
            c.makes_up_faces.Add(this);
            
        }
        public Vector3 CenterPoint { get => Vertices.CenterPoint(); }
        public Vector3 Normal {
            get {
                var P0 = Vertices.First.Position.ToVector3();
                var P1 = Vertices.Second.Position.ToVector3();
                var P2 = Vertices.Third.Position.ToVector3();
                return Vector3.Cross(P1 - P0, P2 - P0).Normalized();
            }
        }

        public IVertex First => Vertices.First;
        public IVertex Second => Vertices.Second;
        public IVertex Third => Vertices.Third;

        private bool Contains(IVertex v) => Contains(v.ID);
        private bool Contains(decimal v_id) => Vertices.First.ID == v_id || Vertices.Second.ID == v_id || Vertices.Third.ID == v_id;

        public bool Contains(params IVertex[] v) {
            foreach (var x in v)
                if (!Contains(x.ID)) return false;
            return true;
        }
        public bool Contains(params decimal[] v) {
            foreach (var x in v)
                if (!Contains(x)) return false;
            return true;
        }
        public bool AllVerticesKeptIn(IEnumerable<Vertex> vertices) => vertices.Contains(Vertices.First) && vertices.Contains(Vertices.Second) && vertices.Contains(Vertices.Third);
        public bool AnyVerticeKeptIn(IEnumerable<Vertex> vertices) => vertices.Contains(Vertices.First) || vertices.Contains(Vertices.Second) || vertices.Contains(Vertices.Third);
        
        /// <summary>
        /// вызывать перед удалением
        /// </summary>
        public void ClearLinks() {
            Vertices.First.makes_up_faces.Remove(this);
            Vertices.Second.makes_up_faces.Remove(this);
            Vertices.Third.makes_up_faces.Remove(this);
        }

        public override int GetHashCode() {
            var hashCode = -308793292;
            hashCode = hashCode * -1521134295 + ID.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<IVertex>.Default.GetHashCode(First);
            hashCode = hashCode * -1521134295 + EqualityComparer<IVertex>.Default.GetHashCode(Second);
            hashCode = hashCode * -1521134295 + EqualityComparer<IVertex>.Default.GetHashCode(Third);
            return hashCode;
        }


        public override string ToString() {
            return "[" + First + " " + Second + " " + Third + "]";
        }
    }
}
