using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL4;

namespace Editor.Graphic.RenderObject.Base {
    public class VBO {
        static public VBO Standard(Kind kind) {
            switch (kind) {
                case Kind.Coordinates: {
                        return new VBO(new List<float>()) {
                            Location = 0,
                            SegmentSize = 3,
                            type = VertexAttribPointerType.Float,
                            Normalized = false,
                            Stride = 12,
                            Offset = 0,
                            Hint = BufferUsageHint.DynamicDraw
                        };
                    }
                case Kind.Color: {
                        return new VBO(new List<float>()) {
                            Location = 1,
                            SegmentSize = 4,
                            type = VertexAttribPointerType.Float,
                            Normalized = false,
                            Stride = 16,
                            Offset = 0,
                            Hint = BufferUsageHint.DynamicDraw
                        };
                    }
                case Kind.TextureCoordinates: {
                        return new VBO(new List<float>()) {
                            Location = 2,
                            SegmentSize = 2,
                            type = VertexAttribPointerType.Float,
                            Normalized = false,
                            Stride = 8,
                            Offset = 0,
                            Hint = BufferUsageHint.StaticDraw
                        };
                    }
                case Kind.SelectionStatus: {
                        return new VBO(new List<float>()) {
                            Location = 3,
                            SegmentSize = 1,
                            type = VertexAttribPointerType.Float,
                            Normalized = false,
                            Stride = 4,
                            Offset = 0,
                            Hint = BufferUsageHint.DynamicDraw
                        };
                    }


                default: {
                        return new VBO(new List<float>()) {
                            Location = 15,
                            SegmentSize = 0,
                            type = VertexAttribPointerType.Float,
                            Normalized = false,
                            Stride = 0,
                            Offset = 0,
                            Hint = BufferUsageHint.DynamicDraw
                        };
                    }
            }

        }
        public enum Kind : byte {
            Coordinates, Color, TextureCoordinates, SelectionStatus, Other
        }

        private uint id;
        public uint ID { get => id; private set => id = value; }
        private List<float> data;
        public float[] Data => data.ToArray();

        public void SetData(IEnumerable<float> data) {
            this.data = data.ToList();
            Built = false;
        }

        public void SetDataPack(int into, IEnumerable<float> data) {
            if (!data.Any()) return;
            var index = into * Stride / 4; /*потому что гвоздями прибит float*/
            for (int i = 0; i < SegmentSize; i++) {
                this.data[index + i] = data.ElementAt(i);
            }
            Built = false;
        }

        public void AddData(IEnumerable<float> data) {
            if (!data.Any()) return;
            if (this.data != null) this.data.AddRange(data); else throw new FieldNotSetException("data не установлена");
            Built = false;
        }

        public void Clear() {
            data.Clear();
            Built = false;
        }
        public void DeleteDataPack(int what) {
            for (int i = 0; i < SegmentSize; i++) {
                this.data.RemoveAt(what * Stride / 4);
            }
            Built = false;
        }
        /// <summary>
        /// Вставляет набор значений в указанную позицию (измерение идет от 0, шаг = Stride)
        /// </summary>
        public void InsertDataPack(int into, IEnumerable<float> pack) {
            if (!pack.Any()) return;
            if (data == null) throw new FieldNotSetException("data не установлена");
            if (Stride == 0) throw new FieldNotSetException("Stride не установлен");
            var index = into * Stride / 4; /*потому что гвоздями прибит float*/
            if (data.Count < index) data.AddRange(pack);
            else data.InsertRange(index, pack);
            Built = false;
        }



        #region Attributes
        #region Location
        /// <summary>
        ///<para>Позиция аргумента в шейдере (т.е.позиция целиком всего одного VBO в шейдере)</para>
        ///например, если location = 3, то в шейдере мы его получим
        ///через layout(location = 3) in /type/ /name/;
        /// </summary>
        public int Location {
            get => location;
            set {
                location = value;
                Built = false;
            }
        }
        private int location = -1;
        #endregion
        #region SegmentSize
        /// <summary>
        /// <para>Размер сегмента данных</para>
        /// например, об одной вершине: три координаты
        /// </summary>
        public int SegmentSize {
            get => shader_arg_size;
            set {
                shader_arg_size = value;
                Built = false;
            }
        }
        private int shader_arg_size = 0;
        #endregion
        #region Type
        /// <summary>
        /// Тип данных, хранимых в VBO
        /// </summary>
        public VertexAttribPointerType Type => type;
        private VertexAttribPointerType type = VertexAttribPointerType.Float;
        #endregion
        #region Normalized
        /// <summary>
        /// <para>Необходимо ли нормализовать данные</para>
        /// т.е. расположить в [-1;1] для знаковых или [0;1] для беззнаковых
        /// </summary>
        public bool Normalized {
            get => (bool)need_normalize;
            set {
                need_normalize = value;
                Built = false;
            }
        }
        private bool? need_normalize = null;
        #endregion
        #region Stride
        /// <summary>
        /// <para>Расстояние между наборами данных</para>
        /// т.е. если данные "плотно упакованы"(без символов-разделителей),
        ///то шаг равен размеру набора
        /// </summary>
        public int Stride {
            get => step;
            set {
                step = value;
                Built = false;
            }
        }
        private int step = 0;
        #endregion
        #region Offset
        /// <summary>
        /// <para>Смещение начала данных в буфере</para>
        /// т.е. позиция, с которой начнется считывание
        /// </summary>
        public int Offset {
            get => offset;
            set {
                offset = value;
                Built = false;
            }
        }
        private int offset = -1;
        #endregion
        #region Hint
        /// <summary>
        /// <para>Сообщает, как часто будут меняться данные, может быть:</para>
        /// <para>GL_STATIC_DRAW: данные либо никогда не будут изменяться, либо будут изменяться очень редко;</para>
        /// <para>GL_DYNAMIC_DRAW: данные будут меняться довольно часто;</para>
        /// <para>GL_STREAM_DRAW: данные будут меняться при каждой отрисовке.</para>
        /// </summary>
        public BufferUsageHint Hint {
            get => hint;
            set {
                hint = value;
                Built = false;
            }
        }
        private BufferUsageHint hint = BufferUsageHint.StaticDraw;
        #endregion
        #endregion
        public int DataSize {
            get => data.Count * 4;//потому что только float
            /*{
                int res = 0;
                int size;
                switch (type) {
                    case VertexAttribPointerType.Float: { size = 4; break; }
                    case VertexAttribPointerType.Byte: { size = 1; break; }
                    case VertexAttribPointerType.Double: { size = 8; break; }
                    case VertexAttribPointerType.Int: { size = 4; break; }
                    case VertexAttribPointerType.Short: { size = 2; break; }
                    case VertexAttribPointerType.UnsignedInt: { size = 4; break; }
                    case VertexAttribPointerType.UnsignedShort: { size = 2; break; }
                    case VertexAttribPointerType.UnsignedByte: { size = 1; break; }
                    default: throw new ArgumentException("Неизвестный тип данных");
                }
                res += data.Count() * size;
                return res;
            }*/
        }

        public VBO(IEnumerable<float> d = null) {
            ID = (uint)GL.GenBuffer();
            if (d != null) data = d.ToList();
        }


        public bool Built { get; private set; } = false;
        public void Build(bool force = false) {
            if (data == null) throw new FieldNotSetException("Data не установлена");
            if (location == -1) throw new FieldNotSetException("Location не установлен");
            if (shader_arg_size == 0) throw new FieldNotSetException("SegmentSize не установлен");
            if (need_normalize == null) throw new FieldNotSetException("Normalized не установлен");
            if (step == 0) throw new FieldNotSetException("Stride не установлен");
            if (offset == -1) throw new FieldNotSetException("Offset не установлен");

            GL.BindBuffer(BufferTarget.ArrayBuffer, ID);//привязали VBO
            GL.BufferData(BufferTarget.ArrayBuffer, DataSize, Data, Hint);//засунули туда дату
            GL.EnableVertexAttribArray(Location);
            GL.VertexAttribPointer(Location, SegmentSize, Type, Normalized, Stride, Offset);//указали, как интерпретировать
            Built = true;
        }

        public override string ToString() {
            var s = "";
            var counter = 0;
            foreach (var x in data) {
                s += (counter == 0 ? "{" : "") + x + (counter == SegmentSize - 1 ? "}  " : " | ");
                if (counter++ == SegmentSize - 1) counter = 0;
            }
            return s;
        }

        ~VBO() {
            /*GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
            GL.DeleteBuffers(1, ref id);*/
            //check косяк, вызывается сразу
        }
    }
}
