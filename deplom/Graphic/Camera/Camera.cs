using Editor.Utilities;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Editor.Graphics.Cameras {
   //сделать вращение камеры вокруг точки
    public class Camera {
        private Vector3 _front = -Vector3.UnitZ;

        private Vector3 _up = Vector3.UnitY;

        private Vector3 _right = Vector3.UnitX;

        // Rotation around the X axis (radians)
        private float _pitch;

        // Rotation around the Y axis (radians)
        private float _yaw = -MathHelper.PiOver2; 

        // The field of view of the camera (radians)
        private float _fov = MathHelper.DegreesToRadians(78);

        public Camera(Vector3 position, float aspectRatio) {
            Position = position;
            AspectRatio = aspectRatio;
        }

        // The position of the camera
        public Vector3 Position { get; set; }
        
        public float AspectRatio { private get; set; }

        public Vector3 Front => _front;

        public Vector3 Up => _up;

        public Vector3 Right => _right;
        
        public float Pitch {
            get => MathHelper.RadiansToDegrees(_pitch);
            set {
                var angle = MathHelper.Clamp(value, -89f, 89f);
                _pitch = MathHelper.DegreesToRadians(angle);
                UpdateVectors();
            }
        }
        
        public float Yaw {
            get => MathHelper.RadiansToDegrees(_yaw);
            set {
                _yaw = MathHelper.DegreesToRadians(value);
                UpdateVectors();
            }
        }
        public float Fov {
            get => MathHelper.RadiansToDegrees(_fov);
            set {
                var angle = MathHelper.Clamp(value, 1f, 90f);
                _fov = MathHelper.DegreesToRadians(angle);
            }
        }
        
        public Matrix4 GetViewMatrix() => Matrix4.LookAt(Position, Position + _front, _up);
        
        public Matrix4 GetProjectionMatrix() => Matrix4.CreatePerspectiveFieldOfView(_fov, AspectRatio, 0.01f, 100f);
        
        private void UpdateVectors() {
            _front.X = (float)Math.Cos(_pitch) * (float)Math.Cos(_yaw);
            _front.Y = (float)Math.Sin(_pitch);
            _front.Z = (float)Math.Cos(_pitch) * (float)Math.Sin(_yaw);
            
            _front = Vector3.Normalize(_front);
            
            _right = Vector3.Normalize(Vector3.Cross(_front, Vector3.UnitY));
            _up = Vector3.Normalize(Vector3.Cross(_right, _front));
        }
    }















































































public class ShittyCamera {
        float θ = 0f;
        float φ = 0f;
        public float Φ {
            get => φ; private set {
                φ = value;
                if (φ > Math.PI * 2) φ -= (float)Math.PI * 2;
                if (φ < 0) φ += (float)Math.PI * 2;
                Debug.WriteLine("φ in degs = " + MathHelper.RadiansToDegrees(φ));
            }
        }
        public float Θ {
            get => θ; private set {
                θ = value;
                Debug.WriteLine("θ in degs = " + MathHelper.RadiansToDegrees(θ));
            }
        }


        private Vector3 _position = new Vector3(0, 0, 0);
        private Vector3 _target = new Vector3(1, 1, 1);
        private float _distance;

        private Vector3 updir = new Vector3(0,1f,0);

        #region Rotations
        /// <summary>
        /// влево - отрицательные числа, вправо - положительные
        /// </summary>
        public void RotateHorizontalDeg(float degrees) {
            Φ += MathHelper.DegreesToRadians(degrees);
            RecalcTarget();
        }
        public void RotateHorizontalRad(float radians) {
            Φ += radians;
            RecalcTarget();
        }
        public void RotateVerticalDeg(float degrees) {
            Θ += MathHelper.DegreesToRadians(degrees);
            RecalcTarget();
        }
        public void RotateVerticalRad(float radians) {
            Θ += radians;
            RecalcTarget();
        }
        #endregion
        #region recalcs
        private void RecalcTarget() {
            /*x = x0 + R · sin θ · cos φ 
              z = z0 + R · sin θ · sin φ 
              y = y0 + R · cos θ
              
              θ = arccos((y - y0)/R)
              φ = arccos( (x-x0)/(R*sinθ) )
             
             */
            float x = (float)(Position.X + _distance * Math.Sin(Θ) * Math.Cos(Φ));
            float y = (float)(Position.Y + _distance * Math.Cos(Θ));
            float z = (float)(Position.Z + _distance * Math.Sin(Θ) * Math.Sin(Φ));
            _target = new Vector3(x, y, z);

            LookAtMatrix = Matrix4.LookAt(Position, Target, updir);
            //Debug.WriteLine("Target is now " + Target);
        }
        const string Fi = "f", Theta = "t", All = "a";
        private void RecalcParams(string c = All) {
            _distance = (Position - Target).Length;
            if (c == "a" || c == "t") Θ = (float)Math.Acos((Target.Y - Position.Y) / _distance);
            if (c == "a" || c == "f") Φ = (float)Math.Acos((Target.X - Position.X) / (_distance / Math.Sin(Θ)));
        }
        #endregion
        #region vectors
        public Vector3 Direction { get => (Target - Position).Normalized(); }
        public Vector3 HorizontalPerpendicular => new Vector3(0, 1f, 0).Cross(Direction).Normalized();
        public Vector3 VerticalPerpendicular => Direction.Cross(HorizontalPerpendicular).Normalized();
        #endregion

        public Vector3 Position {
            get => _position; set {

                _position = value;
                
                RecalcParams();

                LookAtMatrix = Matrix4.LookAt(Position, Target, updir);
                //Debug.WriteLine("Position is now " + Position);
            }
        }
        public Vector3 Target {
            get => _target; set {
                _target = value;
                RecalcParams();

                LookAtMatrix = Matrix4.LookAt(Position, Target, updir);
                //Debug.WriteLine("Target is now " + Target);

            }
        }

        public float Distance {
            get => _distance; private set {
                _distance = value;
                RecalcTarget();
            }
        }
        public Matrix4 LookAtMatrix { get; private set; }

        public ShittyCamera() {
            Θ = 0f;
            Φ = 0f;
            Distance = 10;
        }
        public ShittyCamera(Vector3 position, Vector3 target) {
            Θ = 0f;
            Φ = 0f;
            _position = position;
            Target = target;
        }
        public void Update(double time, double delta) { }
    }

















}
