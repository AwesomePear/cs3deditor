using Editor.Graphic.RenderObject.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Editor.Graphic {
    public static class Configs {
        public static float PickRadius = 0.1f;
        public static Color WorldGrid = new Color(0.236f, 0.246f, 0.243f, 1f);
        public static Color MeshEditorFaces = new Color(0.5f, 0.5f, 0.5f, 1f);
        public static Color MeshEditorEdges = new Color(0.077f, 0.05f, 0.078f, 1f);
        public static Color MeshEditorPoints = MeshEditorEdges;
        public static Color Background = new Color(39, 39, 39, 255);//new Color(0.186f, 0.176f, 0.143f, 1f);
        public static Color FaceSelection = new Color(0.82352f, 0.63137f, 0, 1f);
        public static Color EdgesSelection = new Color(1f, 0.80784f, 0.17647f, 1f);
        public static Color PointsSelection = EdgesSelection;
        public static Color ObjectModeSelection = new Color(134, 163, 191, 255);
        public static Color ObjectModeSelectionVertexEdge = new Color(100, 122, 159, 255);
        public static Color ObjectModeLastSelection = new Color(118, 148, 179, 255);


        public static float WireframeModifier = 0.5f;

        public static float MouseSensitivity = 0.2f;
    }
}
