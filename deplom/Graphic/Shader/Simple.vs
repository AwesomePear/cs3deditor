#version 450 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec4 color;

out vec4 vs_color;

layout (location = 16) uniform mat4 projection;
layout (location = 17) uniform mat4 model;
layout (location = 18) uniform mat4 view;

void main(void)
{
	gl_Position = projection * view * model * vec4(position,1.0);
	vs_color = color;
}