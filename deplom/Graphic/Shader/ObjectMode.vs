#version 450 core

layout(location = 0) in vec3 position;

out vec4 vs_color;

layout(location = 16) uniform mat4 projection;
layout(location = 17) uniform mat4 model;
layout(location = 18) uniform mat4 view;
layout(location = 19) uniform vec4 color;
layout(location = 20) uniform vec4 sel_color;
layout(location = 21) uniform bool selected;

void main(void)
{
	gl_Position = projection * view * model * vec4(position,1.0);
	if (selected){
		vs_color = sel_color; 
	} else {
		vs_color = color;
	}
}