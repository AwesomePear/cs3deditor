#version 450 core
in vec2 texcoords;
out vec4 color;

uniform sampler2D texture0;

void main(void)
{
	color = texture(texture0,texcoords);
}