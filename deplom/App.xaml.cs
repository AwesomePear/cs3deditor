using System;
using System.IO;
using System.Windows;

namespace Editor.Graphics { 
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application {

        [STAThread]
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public static void Main() {
            var app = new App();
            app.Run(new MainWindow()); 
            //app.Run(new InfoWindow(File.ReadAllText(@"..\..\Data\Info.txt")));
            /*app.InitializeComponent();
            app.Run();*/
        }
    }
}
